## Smart City API Gateway

This project implements an API gateway for a smart city application. It acts as a single entry point for various microservices, providing a unified interface for clients to interact with the smart city's functionalities. The gateway is built using Go, Gin framework, and gRPC for communication with backend services.

### Features

- **Routing and Aggregation:** Routes requests to the appropriate microservices based on the URL path. Can aggregate data from multiple services for complex requests.
- **Authentication and Authorization:** Implements JWT-based authentication and role-based access control using Casbin for securing endpoints.
- **Swagger Documentation:** Provides comprehensive API documentation using Swagger, allowing for easy exploration and testing of endpoints.
- **Microservice Communication:** Uses gRPC for efficient and type-safe communication with backend microservices.
- **Logging and Error Handling:** Centralized logging for tracking requests and errors, with structured logs for easier debugging. Provides standardized error responses for consistency.

### Architecture

The API gateway sits between clients (e.g., web apps, mobile apps) and the various microservices that make up the smart city application. It handles:

- **Request Routing:** Directs incoming requests to the correct microservice based on the request path.
- **Request Transformation:** May modify requests before forwarding them to the services, such as adding headers or transforming data formats.
- **Response Aggregation:** Combines responses from multiple services into a single response for the client.
- **Security:** Enforces authentication and authorization policies to protect sensitive data and operations.
- **Monitoring and Logging:** Logs request and response details for debugging and monitoring purposes.

### Microservices

The API gateway connects to the following microservices:

- **Citizen Service:** Manages citizen information, service requests, documents, and notification preferences.
- **City Service:** Handles data related to city planning, zones, demographics, infrastructure, and proposals.
- **Emergency Service:** Provides functionalities for reporting and managing emergency incidents, dispatching resources, and sending alerts.
- **Energy Management Service:** Deals with energy consumption data, grid status, power outages, and renewable energy sources.
- **Environmental Service:** Manages information about air quality, noise levels, green spaces, waste collection, and water quality.
- **Transportation Service:** Handles transportation-related data, including incidents, parking lots, roads, routes, traffic conditions, and vehicles.

### Getting Started

**Prerequisites:**

- Go (version 1.18 or higher)
- Docker (optional, for running services in containers)

**Steps:**

1. **Clone the repository:**
   ```bash
   git clone https://github.com/your-username/smart-city-api-gateway.git
   ```
2. **Install dependencies:**

   ```bash
   go mod download
   ```

3. **Configure the API gateway:**

   Update the configuration file (config/config.yaml) with the appropriate settings for the microservices and other parameters.

4. **Run the microservices:**

   You can run the microservices locally or in Docker containers. Refer to the documentation of each microservice for instructions.

5. **Start the API gateway:**

```bash
go run main.go
```

6.  \*\* Access Swagger UI:

        Open your browser and navigate to http://localhost:8080/api/swagger/ to view the Swagger UI for the API gateway.

Contributing

Contributions to this project are welcome! Please follow these steps:

    Fork the repository.

    Create a new branch for your feature or bug fix.

    Make your changes and commit them with clear and concise messages.

    Push your branch to your forked repository.

    Create a pull request to the main repository.

api-gateway-smart-city
├── Makefile
├── README.md
├── api
│ ├── docs
│ │ ├── docs.go
│ │ ├── swagger.json
│ │ └── swagger.yaml
│ ├── handlers
│ │ ├── citizen.go
│ │ ├── city.go
│ │ ├── emergency.go
│ │ ├── energy_manegement.go
│ │ ├── environmental.go
│ │ ├── init.go
│ │ ├── tets.go
│ │ └── transportation.go
│ ├── middleware
│ │ └── middleware.go
│ ├── router.go
│ └── token
│ └── token.go
├── config
│ ├── casbin
│ │ ├── casbin.conf
│ │ └── casbin.csv
│ ├── checkErr.go
│ ├── getEnv.go
│ └── logger
│ └── logger.go
├── genproto
│ ├── citizen
│ │ ├── citizen.pb.go
│ │ └── citizen_grpc.pb.go
│ ├── city
│ │ ├── assets.pb.go
│ │ ├── assets_grpc.pb.go
│ │ ├── building_permits.pb.go
│ │ ├── building_permits_grpc.pb.go
│ │ ├── city.pb.go
│ │ ├── city_grpc.pb.go
│ │ ├── city_zones.pb.go
│ │ ├── city_zones_grpc.pb.go
│ │ ├── demographic_data.pb.go
│ │ ├── demographic_data_grpc.pb.go
│ │ ├── planning_proposals.pb.go
│ │ ├── planning_proposals_grpc.pb.go
│ │ ├── proposal_feedback.pb.go
│ │ └── proposal_feedback_grpc.pb.go
│ ├── emergency
│ │ ├── emergency.pb.go
│ │ └── emergency_grpc.pb.go
│ ├── energy_management
│ │ ├── building.pb.go
│ │ ├── building_grpc.pb.go
│ │ ├── common.pb.go
│ │ ├── common_grpc.pb.go
│ │ ├── energy_meter.pb.go
│ │ ├── energy_meter_grpc.pb.go
│ │ ├── meter_reading.pb.go
│ │ └── meter_reading_grpc.pb.go
│ ├── enviromental
│ │ ├── air_quality_readings.pb.go
│ │ ├── air_quality_readings_grpc.pb.go
│ │ ├── air_quality_stations.pb.go
│ │ ├── air_quality_stations_grpc.pb.go
│ │ ├── green_spaces.pb.go
│ │ ├── green_spaces_grpc.pb.go
│ │ ├── noise_level_readings.pb.go
│ │ ├── noise_level_readings_grpc.pb.go
│ │ ├── noise_monitoring_zones.pb.go
│ │ ├── noise_monitoring_zones_grpc.pb.go
│ │ ├── plant_registry.pb.go
│ │ ├── plant_registry_grpc.pb.go
│ │ ├── recycling_centers.pb.go
│ │ ├── recycling_centers_grpc.pb.go
│ │ ├── waste_collections.pb.go
│ │ ├── waste_collections_grpc.pb.go
│ │ ├── water_quality_reports.pb.go
│ │ ├── water_quality_reports_grpc.pb.go
│ │ ├── water_treatment_plants.pb.go
│ │ └── water_treatment_plants_grpc.pb.go
│ └── transport
│ ├── incidents.pb.go
│ ├── incidents_grpc.pb.go
│ ├── parking_lots.pb.go
│ ├── parking_lots_grpc.pb.go
│ ├── parking_spaces.pb.go
│ ├── parking_spaces_grpc.pb.go
│ ├── roads.pb.go
│ ├── roads_grpc.pb.go
│ ├── route_schedules.pb.go
│ ├── route_schedules_grpc.pb.go
│ ├── routes.pb.go
│ ├── routes_grpc.pb.go
│ ├── traffic_conditions.pb.go
│ ├── traffic_conditions_grpc.pb.go
│ ├── vehicles.pb.go
│ └── vehicles_grpc.pb.go
├── go.mod
├── go.sum
├── logs
│ └── info.log
├── main.go
├── protos.txt
├── scripts
│ └── gen-proto.sh
└── submodule-for-smart-city
