package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

type Config struct {
	HTTPPort string

	CITIZEN_SERVICE_PORT           string
	CITY_SERVICE_PORT              string
	EMERGENCY_SERVICE_PORT         string
	ENERGY_MANAGEMENT_SERVICE_PORT string
	ENVIRONMENTAL_SERVICE_PORT     string
	TRANSPORTATION_SERVICE_PORT    string

	LOG_PATH string
}

func Load() Config {
	if err := godotenv.Load(); err != nil {
		fmt.Println("No .env file found")
	}

	config := Config{}

	config.HTTPPort = cast.ToString(coalesce("HTTP_PORT", ":8080"))

	config.LOG_PATH = cast.ToString(coalesce("LOG_PATH", "logs/info.log"))
	config.CITY_SERVICE_PORT = cast.ToString(coalesce("CITY_SERVICE_PORT", ":50051"))

	return config
}

func coalesce(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
