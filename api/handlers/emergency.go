package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/smart-city/api-gateway-smart-city/genproto/emergency" // Update with your actual package
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type EmergencyHandler struct {
	Emergency emergency.EmergencyResponseServiceClient
}

func NewEmergencyHandler(conn *grpc.ClientConn) *EmergencyHandler {
	return &EmergencyHandler{
		Emergency: emergency.NewEmergencyResponseServiceClient(conn),
	}
}

// CreateIncident godoc
// @Summary     Create a new Emergency Incident
// @Description Create a new emergency incident with the provided details.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       incident body     emergency.CreateIncidentRequest true "Incident details"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/create-incident [post]
func (e *EmergencyHandler) CreateIncident(ctx *gin.Context) {
	var req emergency.CreateIncidentRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := e.Emergency.CreateIncident(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Incident already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create incident: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Incident created successfully", "incident_id": res.IncidentId})
}

// GetIncident godoc
// @Summary     Get Emergency Incident by ID
// @Description Get details of an emergency incident by its ID.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       incident_id path     string true "Incident ID"
// @Success     200     {object} emergency.EmergencyIncident
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/get-incident/{incident_id} [get]
func (e *EmergencyHandler) GetIncident(ctx *gin.Context) {
	incidentID := ctx.Param("incident_id")

	res, err := e.Emergency.GetIncident(context.Background(), &emergency.GetIncidentRequest{IncidentId: incidentID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Incident not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get incident: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)
}

// ListActiveIncidents godoc
// @Summary     List Active Emergency Incidents
// @Description Get a list of all active emergency incidents.
// @Tags        Emergency
// @Produce     json
// @Success     200     {array}  emergency.EmergencyIncident
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/list-incidents [get]
func (e *EmergencyHandler) ListActiveIncidents(ctx *gin.Context) {

	res, err := e.Emergency.ListActiveIncidents(context.Background(), &emergency.ListActiveIncidentsRequest{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch incidents: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res.Incidents)
}

// UpdateIncidentStatus godoc
// @Summary     Update Emergency Incident Status
// @Description Update the status of an existing emergency incident.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       incident_id path     string true "Incident ID"
// @Param       status      body     emergency.UpdateIncidentStatusRequest true "New status of the incident"
// @Success     200     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/update-incident/{incident_id} [put]
func (e *EmergencyHandler) UpdateIncidentStatus(ctx *gin.Context) {
	incidentID := ctx.Param("incident_id")
	var req emergency.UpdateIncidentStatusRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}

	res, err := e.Emergency.UpdateIncidentStatus(context.Background(), &emergency.UpdateIncidentStatusRequest{IncidentId: incidentID, Status: req.Status})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Incident not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update incident: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": "Incident updated successfully", "status": res.Status})
}

// DeleteIncident godoc
// @Summary     Delete Emergency Incident
// @Description Delete an emergency incident by its ID.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       incident_id path     string true "Incident ID"
// @Success     204 {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/delete-incident/{incident_id} [delete]
func (e *EmergencyHandler) DeleteIncident(ctx *gin.Context) {
	incidentID := ctx.Param("incident_id")

	_, err := e.Emergency.DeleteIncident(context.Background(), &emergency.DeleteIncidentRequest{IncidentId: incidentID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Incident not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete incident: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Incident deleted successfully"})
}

// CreateResource godoc
// @Summary     Create Emergency Resource
// @Description Create a new emergency resource.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       resource body     emergency.CreateResourceRequest true "Resource data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/create-resource [post]
func (e *EmergencyHandler) CreateResource(ctx *gin.Context) {
	var req emergency.CreateResourceRequest

	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}

	res, err := e.Emergency.CreateResource(context.Background(), &req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create resource: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Resource created successfully", "resource_id": res.ResourceId})
}

// GetResource godoc
// @Summary     Get Emergency Resource by ID
// @Description Get details of an emergency resource by its ID.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       resource_id path     string true "Resource ID"
// @Success     200     {object} emergency.EmergencyResource
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/get-resource/{resource_id} [get]
func (e *EmergencyHandler) GetResource(ctx *gin.Context) {
	resourceID := ctx.Param("resource_id")

	res, err := e.Emergency.GetResource(context.Background(), &emergency.GetResourceRequest{ResourceId: resourceID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Resource not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get resource: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// UpdateResource godoc
// @Summary     Update Emergency Resource
// @Description Update an existing emergency resource.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       resource_id path     string                             true "Resource ID"
// @Param       resource    body     emergency.UpdateResourceRequest true "Updated resource data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/emergency/update-resource/{resource_id} [put]
func (e *EmergencyHandler) UpdateResource(ctx *gin.Context) {
	resourceID := ctx.Param("resource_id")
	var req emergency.UpdateResourceRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	res, err := e.Emergency.UpdateResource(context.Background(), &emergency.UpdateResourceRequest{ResourceId: resourceID, ResourceType: req.ResourceType, Latitude: req.Latitude, Longitude: req.Longitude, Status: req.Status})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Resource not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update resource: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Resource updated successfully", "status": res.Status})
}

// DeleteResource godoc
// @Summary     Delete Emergency Resource
// @Description Delete an emergency resource by its ID.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       resource_id path     string true "Resource ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/delete-resource/{resource_id} [delete]
func (e *EmergencyHandler) DeleteResource(ctx *gin.Context) {
	resourceID := ctx.Param("resource_id")
	_, err := e.Emergency.DeleteResource(context.Background(), &emergency.DeleteResourceRequest{ResourceId: resourceID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Resource not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete resource: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusNoContent, gin.H{"message": "Resource deleted successfully"})
}

// UpdateResourceStatus godoc
// @Summary     Update Emergency Resource Status
// @Description Update the status of an emergency resource.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       resource_id path     string true "Resource ID"
// @Param       status      body     emergency.UpdateResourceStatusRequest true "New status of the resource"
// @Success     200     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/update-resource-status/{resource_id} [put]
func (e *EmergencyHandler) UpdateResourceStatus(ctx *gin.Context) {
	resourceID := ctx.Param("resource_id")
	var req emergency.UpdateResourceStatusRequest

	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}

	res, err := e.Emergency.UpdateResourceStatus(context.Background(), &emergency.UpdateResourceStatusRequest{ResourceId: resourceID, Status: req.Status})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Resource not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update resource status: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": "Resource status updated successfully", "status": res.Status})
}

// DispatchResource godoc
// @Summary     Dispatch Emergency Resource
// @Description Dispatch an available emergency resource to an incident.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       dispatch body     emergency.DispatchResourceRequest true "Dispatch details"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/dispatch-resource [post]
func (e *EmergencyHandler) DispatchResource(ctx *gin.Context) {
	var req emergency.DispatchResourceRequest

	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}

	res, err := e.Emergency.DispatchResource(context.Background(), &req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to dispatch resource: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Resource dispatched successfully", "dispatch_id": res.DispatchId})
}

// GetDispatch godoc
// @Summary     Get Dispatch Details
// @Description Retrieve dispatch details by dispatch ID.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       dispatch_id path     string true "Dispatch ID"
// @Success     200     {object} emergency.ResourceDispatch
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/get-dispatch/{dispatch_id} [get]
func (e *EmergencyHandler) GetDispatch(ctx *gin.Context) {
	dispatchID := ctx.Param("dispatch_id")

	res, err := e.Emergency.GetDispatch(context.Background(), &emergency.GetDispatchRequest{DispatchId: dispatchID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Dispatch not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get dispatch: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// UpdateDispatch godoc
// @Summary     Update Dispatch Details
// @Description Update dispatch information, such as arrival time.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       dispatch_id path     string                             true "Dispatch ID"
// @Param       dispatch    body     emergency.UpdateDispatchRequest true "Updated dispatch information"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/emergency/update-dispatch/{dispatch_id} [put]
func (e *EmergencyHandler) UpdateDispatch(ctx *gin.Context) {
	dispatchID := ctx.Param("dispatch_id")
	var req emergency.UpdateDispatchRequest

	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}

	res, err := e.Emergency.UpdateDispatch(context.Background(), &emergency.UpdateDispatchRequest{DispatchId: dispatchID, IncidentId: req.IncidentId, ResourceId: req.ResourceId, DispatchedAt: req.DispatchedAt, ArrivedAt: req.ArrivedAt})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Dispatch not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update dispatch: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": "Dispatch updated successfully", "status": res.Status})
}

// DeleteDispatch godoc
// @Summary     Delete Dispatch Record
// @Description Delete a dispatch record by its ID.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       dispatch_id path     string true "Dispatch ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/delete-dispatch/{dispatch_id} [delete]
func (e *EmergencyHandler) DeleteDispatch(ctx *gin.Context) {
	dispatchID := ctx.Param("dispatch_id")

	_, err := e.Emergency.DeleteDispatch(context.Background(), &emergency.DeleteDispatchRequest{DispatchId: dispatchID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Dispatch not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete dispatch: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusNoContent, gin.H{"message": "Dispatch deleted successfully"})
}

// ArriveResource godoc
// @Summary     Mark Resource as Arrived
// @Description Mark an emergency resource as arrived at the incident location.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       dispatch_id path     string true "Dispatch ID"
// @Success     200     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/arrive-resource/{dispatch_id} [put]
func (e *EmergencyHandler) ArriveResource(ctx *gin.Context) {
	dispatchID := ctx.Param("dispatch_id")

	res, err := e.Emergency.ArriveResource(context.Background(), &emergency.ArriveResourceRequest{DispatchId: dispatchID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Dispatch not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to mark resource as arrived: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": "Resource marked as arrived", "status": res.Status})
}

// ListAvailableResources godoc
// @Summary     List Available Emergency Resources
// @Description Get a list of available emergency resources, optionally filtered by type.
// @Tags        Emergency
// @Produce     json
// @Param       emergency_type query  string false "Filter by emergency type"
// @Success     200     {array}  emergency.EmergencyResource
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/list-resources [get]
func (e *EmergencyHandler) ListAvailableResources(ctx *gin.Context) {
	emergencyType := ctx.Query("emergency_type") // Get the optional emergency type from query parameters

	res, err := e.Emergency.ListAvailableResources(context.Background(), &emergency.ListAvailableResourcesRequest{
		EmergencyType: emergencyType,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list resources: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.Resources)
}

// CreateAlert godoc
// @Summary     Create Emergency Alert
// @Description Broadcast an emergency alert to relevant parties.
// @Tags        Emergency
// @Accept      json
// @Produce     json
// @Param       alert body     emergency.CreateAlertRequest true "Alert details"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/emergency/create-alert [post]
func (e *EmergencyHandler) CreateAlert(ctx *gin.Context) {
	var req emergency.CreateAlertRequest

	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := e.Emergency.CreateAlert(context.Background(), &req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create alert: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Alert created successfully", "alert_id": res.AlertId})
}
