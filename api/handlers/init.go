package handlers

import (
	"github.com/smart-city/api-gateway-smart-city/config/logger"
	"google.golang.org/grpc"
)

type HTTPHandler struct {
	Citizen          CitizenHandler
	City             CityHandler
	EnergyManagement EnergyManagementHandler
	Emergency        EmergencyHandler
	Environmental    EnvironmentalHandler
	Transportation   TransportationHandler

	Logger logger.Logger
}

func NewHandler(citizen *grpc.ClientConn, city *grpc.ClientConn, emergency *grpc.ClientConn, energyManagement *grpc.ClientConn, environmental *grpc.ClientConn, transportation *grpc.ClientConn, l logger.Logger,
) *HTTPHandler {
	return &HTTPHandler{
		Citizen:          *NewCitizenHandler(citizen),
		City:             *NewCityHandler(city),
		EnergyManagement: *NewEnergyHandler(energyManagement),
		Environmental:    *NewEnvironmentalHandler(environmental),
		Emergency:        *NewEmergencyHandler(emergency),
		Transportation:   *NewTransportationHandler(transportation),

		Logger: l,
	}

}
