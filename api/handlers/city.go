package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	pbcity "github.com/smart-city/api-gateway-smart-city/genproto/city" // Update with your actual package path
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CityHandler struct {
	CityBuildingPermit            pbcity.BuildingPermitServiceClient
	CityPlanning                  pbcity.CityPlanningProposalServiceClient
	CityZone                      pbcity.CityZoneServiceClient
	CityDemographicData           pbcity.DemographicDataServiceClient
	CityInfrastructureAsset       pbcity.InfrastructureAssetServiceClient
	CityPlanningProporsal         pbcity.PlanningProposalServiceClient
	CityPlanningProporsalFeedback pbcity.ProposalFeedbackServiceClient
}

func NewCityHandler(city *grpc.ClientConn) *CityHandler {
	return &CityHandler{
		CityBuildingPermit:            pbcity.NewBuildingPermitServiceClient(city),
		CityPlanning:                  pbcity.NewCityPlanningProposalServiceClient(city),
		CityZone:                      pbcity.NewCityZoneServiceClient(city),
		CityDemographicData:           pbcity.NewDemographicDataServiceClient(city),
		CityInfrastructureAsset:       pbcity.NewInfrastructureAssetServiceClient(city),
		CityPlanningProporsal:         pbcity.NewPlanningProposalServiceClient(city),
		CityPlanningProporsalFeedback: pbcity.NewProposalFeedbackServiceClient(city),
	}
}

// ------------------ Building Permit Handlers ------------------

// CreateBuildingPermit godoc
// @Summary     Create Building Permit
// @Description Creates a new building permit application.
// @Tags        City, Building Permits
// @Accept      json
// @Produce     json
// @Param       permit body     pbcity.CreateBuildingPermitRequest true "Building permit application details"
// @Success     201     {object} pbcity.CreateBuildingPermitResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/building-permits [post]
func (h *CityHandler) CreateBuildingPermit(ctx *gin.Context) {
	var req pbcity.CreateBuildingPermitRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.CityBuildingPermit.CreateBuildingPermit(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "A building permit with this ID already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create building permit: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, res)
}

// GetBuildingPermit godoc
// @Summary     Get Building Permit
// @Description Retrieves a building permit by its ID.
// @Tags        City, Building Permits
// @Accept      json
// @Produce     json
// @Param       permit_id path     string true "Building permit ID"
// @Success     200     {object} pbcity.GetBuildingPermitResponse
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/building-permits/{permit_id} [get]
func (h *CityHandler) GetBuildingPermit(ctx *gin.Context) {
	permitID := ctx.Param("permit_id")

	res, err := h.CityBuildingPermit.GetBuildingPermit(context.Background(), &pbcity.GetBuildingPermitRequest{Id: permitID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Building permit not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get building permit: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// UpdateBuildingPermit godoc
// @Summary     Update Building Permit
// @Description Updates an existing building permit.
// @Tags        City, Building Permits
// @Accept      json
// @Produce     json
// @Param       permit_id path     string true "Building permit ID"
// @Param       permit body     pbcity.UpdateBuildingPermitRequest true "Updated building permit details"
// @Success     200     {object} pbcity.UpdateBuildingPermitResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/building-permits/{permit_id} [put]
func (h *CityHandler) UpdateBuildingPermit(ctx *gin.Context) {
	permitID := ctx.Param("permit_id")
	var req pbcity.UpdateBuildingPermitRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	req.Permit.Id = permitID // Ensure the ID is set in the request

	res, err := h.CityBuildingPermit.UpdateBuildingPermit(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Building permit not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update building permit: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// DeleteBuildingPermit godoc
// @Summary     Delete Building Permit
// @Description Deletes a building permit by its ID.
// @Tags        City, Building Permits
// @Accept      json
// @Produce     json
// @Param       permit_id path     string true "Building permit ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/building-permits/{permit_id} [delete]
func (h *CityHandler) DeleteBuildingPermit(ctx *gin.Context) {
	permitID := ctx.Param("permit_id")

	_, err := h.CityBuildingPermit.DeleteBuildingPermit(context.Background(), &pbcity.DeleteBuildingPermitRequest{Id: permitID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Building permit not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete building permit: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Building permit deleted successfully"})
}

// GetAllBuildingPermits godoc
// @Summary     Get All Building Permits
// @Description Retrieves a list of all building permits.
// @Tags        City, Building Permits
// @Produce     json
// @Success     200     {array}  pbcity.BuildingPermit
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/building-permits [get]
func (h *CityHandler) GetAllBuildingPermits(ctx *gin.Context) {
	res, err := h.CityBuildingPermit.GetAllBuildingPermits(context.Background(), &pbcity.BuildingPermitsVoid{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get building permits: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res.Permits)
}

// ------------------ City Planning Proposal Handlers ------------------

// CreateCityPlanningProposal godoc
// @Summary     Create City Planning Proposal
// @Description Creates a new city planning proposal.
// @Tags        City, Planning Proposals
// @Accept      json
// @Produce     json
// @Param       proposal body     pbcity.CreateCityPlanningProposalRequest true "City planning proposal details"
// @Success     201     {object} pbcity.CreateCityPlanningProposalResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals [post]
func (h *CityHandler) CreateCityPlanningProposal(ctx *gin.Context) {
	var req pbcity.CreateCityPlanningProposalRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.CityPlanning.CreateCityPlanningProposal(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "A planning proposal with this ID already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create city planning proposal: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, res)
}

// GetCityPlanningProposal godoc
// @Summary     Get City Planning Proposal
// @Description Retrieves a city planning proposal by its ID.
// @Tags        City, Planning Proposals
// @Accept      json
// @Produce     json
// @Param       proposal_id path     string true "City planning proposal ID"
// @Success     200     {object} pbcity.GetCityPlanningProposalResponse
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals/{proposal_id} [get]
func (h *CityHandler) GetCityPlanningProposal(ctx *gin.Context) {
	proposalID := ctx.Param("proposal_id")

	res, err := h.CityPlanning.GetCityPlanningProposal(context.Background(), &pbcity.GetCityPlanningProposalRequest{Id: proposalID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "City planning proposal not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get city planning proposal: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// UpdateCityPlanningProposal godoc
// @Summary     Update City Planning Proposal
// @Description Updates an existing city planning proposal.
// @Tags        City, Planning Proposals
// @Accept      json
// @Produce     json
// @Param       proposal_id path     string true "City planning proposal ID"
// @Param       proposal body     pbcity.UpdateCityPlanningProposalRequest true "Updated city planning proposal details"
// @Success     200     {object} pbcity.UpdateCityPlanningProposalResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals/{proposal_id} [put]
func (h *CityHandler) UpdateCityPlanningProposal(ctx *gin.Context) {
	proposalID := ctx.Param("proposal_id")
	var req pbcity.UpdateCityPlanningProposalRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	req.Proposal.Id = proposalID // Ensure the ID is set in the request

	res, err := h.CityPlanning.UpdateCityPlanningProposal(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "City planning proposal not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update city planning proposal: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// DeleteCityPlanningProposal godoc
// @Summary     Delete City Planning Proposal
// @Description Deletes a city planning proposal by its ID.
// @Tags        City, Planning Proposals
// @Accept      json
// @Produce     json
// @Param       proposal_id path     string true "City planning proposal ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals/{proposal_id} [delete]
func (h *CityHandler) DeleteCityPlanningProposal(ctx *gin.Context) {
	proposalID := ctx.Param("proposal_id")

	_, err := h.CityPlanning.DeleteCityPlanningProposal(context.Background(), &pbcity.DeleteCityPlanningProposalRequest{Id: proposalID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "City planning proposal not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete city planning proposal: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "City planning proposal deleted successfully"})
}

// GetAllCityPlanningProposals godoc
// @Summary     Get All City Planning Proposals
// @Description Retrieves a list of all city planning proposals.
// @Tags        City, Planning Proposals
// @Produce     json
// @Success     200     {array}  pbcity.CityPlanningProposal
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals [get]
func (h *CityHandler) GetAllCityPlanningProposals(ctx *gin.Context) {
	res, err := h.CityPlanning.GetAllCityPlanningProposal(context.Background(), &pbcity.PlanningVoid{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get city planning proposals: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res.Proposal)
}

// ------------------ City Zone Handlers ------------------

// CreateCityZone godoc
// @Summary     Create City Zone
// @Description Creates a new city zone.
// @Tags        City, Zones
// @Accept      json
// @Produce     json
// @Param       zone body     pbcity.CreateCityZoneRequest true "City zone details"
// @Success     201     {object} pbcity.CreateCityZoneResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/zones [post]
func (h *CityHandler) CreateCityZone(ctx *gin.Context) {
	var req pbcity.CreateCityZoneRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.CityZone.CreateCityZone(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "A city zone with this ID already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create city zone: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, res)
}

// GetCityZone godoc
// @Summary     Get City Zone
// @Description Retrieves a city zone by its ID.
// @Tags        City, Zones
// @Accept      json
// @Produce     json
// @Param       zone_id path     string true "City zone ID"
// @Success     200     {object} pbcity.GetCityZoneResponse
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/zones/{zone_id} [get]
func (h *CityHandler) GetCityZone(ctx *gin.Context) {
	zoneID := ctx.Param("zone_id")

	res, err := h.CityZone.GetCityZone(context.Background(), &pbcity.GetCityZoneRequest{Id: zoneID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "City zone not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get city zone: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// UpdateCityZone godoc
// @Summary     Update City Zone
// @Description Updates an existing city zone.
// @Tags        City, Zones
// @Accept      json
// @Produce     json
// @Param       zone_id path     string true "City zone ID"
// @Param       zone body     pbcity.UpdateCityZoneRequest true "Updated city zone details"
// @Success     200     {object} pbcity.UpdateCityZoneResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/zones/{zone_id} [put]
func (h *CityHandler) UpdateCityZone(ctx *gin.Context) {
	zoneID := ctx.Param("zone_id")
	var req pbcity.UpdateCityZoneRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	req.Zone.Id = zoneID // Ensure the ID is set in the request

	res, err := h.CityZone.UpdateCityZone(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "City zone not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update city zone: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// DeleteCityZone godoc
// @Summary     Delete City Zone
// @Description Deletes a city zone by its ID.
// @Tags        City, Zones
// @Accept      json
// @Produce     json
// @Param       zone_id path     string true "City zone ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/zones/{zone_id} [delete]
func (h *CityHandler) DeleteCityZone(ctx *gin.Context) {
	zoneID := ctx.Param("zone_id")

	_, err := h.CityZone.DeleteCityZone(context.Background(), &pbcity.DeleteCityZoneRequest{Id: zoneID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "City zone not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete city zone: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "City zone deleted successfully"})
}

// GetAllCityZones godoc
// @Summary     Get All City Zones
// @Description Retrieves a list of all city zones.
// @Tags        City, Zones
// @Produce     json
// @Success     200     {array}  pbcity.CityZone
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/zones [get]
func (h *CityHandler) GetAllCityZones(ctx *gin.Context) {
	res, err := h.CityZone.GetAllCityZones(context.Background(), &pbcity.CityZoneVoid{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get city zones: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res.Zone)
}

// ------------------ Demographic Data Handlers ------------------

// CreateDemographicData godoc
// @Summary     Create Demographic Data
// @Description Creates new demographic data for a city zone.
// @Tags        City, Demographic Data
// @Accept      json
// @Produce     json
// @Param       data body     pbcity.CreateDemographicDataRequest true "Demographic data details"
// @Success     201     {object} pbcity.CreateDemographicDataResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/demographic-data [post]
func (h *CityHandler) CreateDemographicData(ctx *gin.Context) {
	var req pbcity.CreateDemographicDataRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.CityDemographicData.CreateDemographicData(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Demographic data with this ID already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create demographic data: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, res)
}

// GetDemographicData godoc
// @Summary     Get Demographic Data
// @Description Retrieves demographic data by its ID.
// @Tags        City, Demographic Data
// @Accept      json
// @Produce     json
// @Param       data_id path     string true "Demographic data ID"
// @Success     200     {object} pbcity.GetDemographicDataResponse
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/demographic-data/{data_id} [get]
func (h *CityHandler) GetDemographicData(ctx *gin.Context) {
	dataID := ctx.Param("data_id")

	res, err := h.CityDemographicData.GetDemographicData(context.Background(), &pbcity.GetDemographicDataRequest{Id: dataID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Demographic data not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get demographic data: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// UpdateDemographicData godoc
// @Summary     Update Demographic Data
// @Description Updates existing demographic data.
// @Tags        City, Demographic Data
// @Accept      json
// @Produce     json
// @Param       data_id path     string true "Demographic data ID"
// @Param       data body     pbcity.UpdateDemographicDataRequest true "Updated demographic data details"
// @Success     200     {object} pbcity.UpdateDemographicDataResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/demographic-data/{data_id} [put]
func (h *CityHandler) UpdateDemographicData(ctx *gin.Context) {
	dataID := ctx.Param("data_id")
	var req pbcity.UpdateDemographicDataRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	req.Data.Id = dataID // Ensure the ID is set in the request

	res, err := h.CityDemographicData.UpdateDemographicData(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Demographic data not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update demographic data: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// DeleteDemographicData godoc
// @Summary     Delete Demographic Data
// @Description Deletes demographic data by its ID.
// @Tags        City, Demographic Data
// @Accept      json
// @Produce     json
// @Param       data_id path     string true "Demographic data ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/demographic-data/{data_id} [delete]
func (h *CityHandler) DeleteDemographicData(ctx *gin.Context) {
	dataID := ctx.Param("data_id")

	_, err := h.CityDemographicData.DeleteDemographicData(context.Background(), &pbcity.DeleteDemographicDataRequest{Id: dataID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Demographic data not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete demographic data: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Demographic data deleted successfully"})
}

// GetAllDemographicData godoc
// @Summary     Get All Demographic Data
// @Description Retrieves a list of all demographic data entries.
// @Tags        City, Demographic Data
// @Produce     json
// @Success     200     {array}  pbcity.DemographicData
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/demographic-data [get]
func (h *CityHandler) GetAllDemographicData(ctx *gin.Context) {
	res, err := h.CityDemographicData.GetAllDemographicData(context.Background(), &pbcity.DemographicDataVoid{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get demographic data: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res.Data)
}

// ------------------ Infrastructure Asset Handlers ------------------

// CreateInfrastructureAsset godoc
// @Summary     Create Infrastructure Asset
// @Description Creates a new infrastructure asset record.
// @Tags        City, Infrastructure Assets
// @Accept      json
// @Produce     json
// @Param       asset body     pbcity.CreateInfrastructureAssetRequest true "Infrastructure asset details"
// @Success     201     {object} pbcity.CreateInfrastructureAssetResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/infrastructure-assets [post]
func (h *CityHandler) CreateInfrastructureAsset(ctx *gin.Context) {
	var req pbcity.CreateInfrastructureAssetRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.CityInfrastructureAsset.CreateInfrastructureAsset(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "An infrastructure asset with this ID already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create infrastructure asset: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, res)
}

// GetInfrastructureAsset godoc
// @Summary     Get Infrastructure Asset
// @Description Retrieves an infrastructure asset by its ID.
// @Tags        City, Infrastructure Assets
// @Accept      json
// @Produce     json
// @Param       asset_id path     string true "Infrastructure asset ID"
// @Success     200     {object} pbcity.GetInfrastructureAssetResponse
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/infrastructure-assets/{asset_id} [get]
func (h *CityHandler) GetInfrastructureAsset(ctx *gin.Context) {
	assetID := ctx.Param("asset_id")

	res, err := h.CityInfrastructureAsset.GetInfrastructureAsset(context.Background(), &pbcity.GetInfrastructureAssetRequest{Id: assetID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Infrastructure asset not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get infrastructure asset: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// UpdateInfrastructureAsset godoc
// @Summary     Update Infrastructure Asset
// @Description Updates an existing infrastructure asset record.
// @Tags        City, Infrastructure Assets
// @Accept      json
// @Produce     json
// @Param       asset_id path     string true "Infrastructure asset ID"
// @Param       asset body     pbcity.UpdateInfrastructureAssetRequest true "Updated infrastructure asset details"
// @Success     200     {object} pbcity.UpdateInfrastructureAssetResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/infrastructure-assets/{asset_id} [put]
func (h *CityHandler) UpdateInfrastructureAsset(ctx *gin.Context) {
	assetID := ctx.Param("asset_id")
	var req pbcity.UpdateInfrastructureAssetRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	req.Asset.Id = assetID // Ensure the ID is set in the request

	res, err := h.CityInfrastructureAsset.UpdateInfrastructureAsset(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Infrastructure asset not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update infrastructure asset: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// DeleteInfrastructureAsset godoc
// @Summary     Delete Infrastructure Asset
// @Description Deletes an infrastructure asset record by its ID.
// @Tags        City, Infrastructure Assets
// @Accept      json
// @Produce     json
// @Param       asset_id path     string true "Infrastructure asset ID"
// @Success     204     {object} map[string]interface{}
// @Failure     40
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/infrastructure-assets/{asset_id} [delete]
func (h *CityHandler) DeleteInfrastructureAsset(ctx *gin.Context) {
	assetID := ctx.Param("asset_id")

	_, err := h.CityInfrastructureAsset.DeleteInfrastructureAsset(context.Background(), &pbcity.DeleteInfrastructureAssetRequest{Id: assetID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Infrastructure asset not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete infrastructure asset: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Infrastructure asset deleted successfully"})
}

// GetAllInfrastructureAssets godoc
// @Summary     Get All Infrastructure Assets
// @Description Retrieves a list of all infrastructure assets.
// @Tags        City, Infrastructure Assets
// @Produce     json
// @Success     200     {array}  pbcity.InfrastructureAsset
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/infrastructure-assets [get]
func (h *CityHandler) GetAllInfrastructureAssets(ctx *gin.Context) {
	res, err := h.CityInfrastructureAsset.GetAllInfrastructureAssets(context.Background(), &pbcity.InfrastructureAssetsVoid{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get infrastructure assets: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res.Assets)
}

// ------------------ Planning Proposal Handlers ------------------

// CreatePlanningProposal godoc
// @Summary     Create Planning Proposal
// @Description Creates a new planning proposal for a city zone.
// @Tags        City, Planning Proposals
// @Accept      json
// @Produce     json
// @Param       proposal body     pbcity.CreatePlanningProposalRequest true "Planning proposal details"
// @Success     201     {object} pbcity.CreatePlanningProposalResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals/zone [post]
func (h *CityHandler) CreatePlanningProposal(ctx *gin.Context) {
	var req pbcity.CreatePlanningProposalRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.CityPlanningProporsal.CreatePlanningProposal(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "A planning proposal with this ID already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create planning proposal: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, res)
}

// GetPlanningProposal godoc
// @Summary     Get Planning Proposal
// @Description Retrieves a planning proposal by its ID.
// @Tags        City, Planning Proposals
// @Accept      json
// @Produce     json
// @Param       proposal_id path     string true "Planning proposal ID"
// @Success     200     {object} pbcity.GetPlanningProposalResponse
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals/zone/{proposal_id} [get]
func (h *CityHandler) GetPlanningProposal(ctx *gin.Context) {
	proposalID := ctx.Param("proposal_id")

	res, err := h.CityPlanningProporsal.GetPlanningProposal(context.Background(), &pbcity.GetPlanningProposalRequest{Id: proposalID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Planning proposal not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get planning proposal: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// UpdatePlanningProposal godoc
// @Summary     Update Planning Proposal
// @Description Updates an existing planning proposal.
// @Tags        City, Planning Proposals
// @Accept      json
// @Produce     json
// @Param       proposal_id path     string true "Planning proposal ID"
// @Param       proposal body     pbcity.UpdatePlanningProposalRequest true "Updated planning proposal details"
// @Success     200     {object} pbcity.UpdatePlanningProposalResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals/zone/{proposal_id} [put]
func (h *CityHandler) UpdatePlanningProposal(ctx *gin.Context) {
	proposalID := ctx.Param("proposal_id")
	var req pbcity.UpdatePlanningProposalRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	req.Proposal.Id = proposalID // Ensure the ID is set in the request

	res, err := h.CityPlanningProporsal.UpdatePlanningProposal(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Planning proposal not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update planning proposal: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// DeletePlanningProposal godoc
// @Summary     Delete Planning Proposal
// @Description Deletes a planning proposal by its ID.
// @Tags        City, Planning Proposals
// @Accept      json
// @Produce     json
// @Param       proposal_id path     string true "Planning proposal ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals/zone/{proposal_id} [delete]
func (h *CityHandler) DeletePlanningProposal(ctx *gin.Context) {
	proposalID := ctx.Param("proposal_id")

	_, err := h.CityPlanningProporsal.DeletePlanningProposal(context.Background(), &pbcity.DeletePlanningProposalRequest{Id: proposalID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Planning proposal not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete planning proposal: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Planning proposal deleted successfully"})
}

// GetAllPlanningProposals godoc
// @Summary     Get All Planning Proposals
// @Description Retrieves a list of all planning proposals.
// @Tags        City, Planning Proposals
// @Produce     json
// @Success     200     {array}  pbcity.PlanningProposal
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals/zone [get]
func (h *CityHandler) GetAllPlanningProposals(ctx *gin.Context) {
	res, err := h.CityPlanningProporsal.GetAllPlanningProposals(context.Background(), &pbcity.PlanningProposalVoid{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get planning proposals: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res.Proposals)
}

// ------------------ Proposal Feedback Handlers ------------------

// CreateProposalFeedback godoc
// @Summary     Create Proposal Feedback
// @Description Submits feedback on a planning proposal.
// @Tags        City, Planning Proposals, Feedback
// @Accept      json
// @Produce     json
// @Param       feedback body     pbcity.CreateProposalFeedbackRequest true "Proposal feedback details"
// @Success     201     {object} pbcity.CreateProposalFeedbackResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals/feedback [post]
func (h *CityHandler) CreateProposalFeedback(ctx *gin.Context) {
	var req pbcity.CreateProposalFeedbackRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.CityPlanningProporsalFeedback.CreateProposalFeedback(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Feedback with this ID already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create proposal feedback: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, res)
}

// GetProposalFeedback godoc
// @Summary     Get Proposal Feedback
// @Description Retrieves feedback on a proposal by its ID.
// @Tags        City, Planning Proposals, Feedback
// @Accept      json
// @Produce     json
// @Param       feedback_id path     string true "Proposal feedback ID"
// @Success     200     {object} pbcity.GetProposalFeedbackResponse
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals/feedback/{feedback_id} [get]
func (h *CityHandler) GetProposalFeedback(ctx *gin.Context) {
	feedbackID := ctx.Param("feedback_id")

	res, err := h.CityPlanningProporsalFeedback.GetProposalFeedback(context.Background(), &pbcity.GetProposalFeedbackRequest{Id: feedbackID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Proposal feedback not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get proposal feedback: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// UpdateProposalFeedback godoc
// @Summary     Update Proposal Feedback
// @Description Updates existing feedback on a proposal.
// @Tags        City, Planning Proposals, Feedback
// @Accept      json
// @Produce     json
// @Param       feedback_id path     string true "Proposal feedback ID"
// @Param       feedback body     pbcity.UpdateProposalFeedbackRequest true "Updated proposal feedback details"
// @Success     200     {object} pbcity.UpdateProposalFeedbackResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals/feedback/{feedback_id} [put]
func (h *CityHandler) UpdateProposalFeedback(ctx *gin.Context) {
	feedbackID := ctx.Param("feedback_id")
	var req pbcity.UpdateProposalFeedbackRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	req.Feedback.Id = feedbackID // Ensure the ID is set in the request

	res, err := h.CityPlanningProporsalFeedback.UpdateProposalFeedback(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Proposal feedback not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update proposal feedback: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// DeleteProposalFeedback godoc
// @Summary     Delete Proposal Feedback
// @Description Deletes feedback on a proposal by its ID.
// @Tags        City, Planning Proposals, Feedback
// @Accept      json
// @Produce     json
// @Param       feedback_id path     string true "Proposal feedback ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals/feedback/{feedback_id} [delete]
func (h *CityHandler) DeleteProposalFeedback(ctx *gin.Context) {
	feedbackID := ctx.Param("feedback_id")

	_, err := h.CityPlanningProporsalFeedback.DeleteProposalFeedback(context.Background(), &pbcity.DeleteProposalFeedbackRequest{Id: feedbackID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Proposal feedback not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete proposal feedback: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Proposal feedback deleted successfully"})
}

// GetAllProposalFeedback godoc
// @Summary     Get All Proposal Feedback
// @Description Retrieves a list of all feedback entries for proposals.
// @Tags        City, Planning Proposals, Feedback
// @Produce     json
// @Success     200     {array}  pbcity.ProposalFeedback
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/city/planning-proposals/feedback [get]
func (h *CityHandler) GetAllProposalFeedback(ctx *gin.Context) {
	res, err := h.CityPlanningProporsalFeedback.GetAllProposalFeedback(context.Background(), &pbcity.ProposalFeedbackRequestVoid{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get proposal feedback: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res.Feedbacks)
}
