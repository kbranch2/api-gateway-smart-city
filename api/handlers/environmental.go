package handlers

import (
	"google.golang.org/grpc"

	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/smart-city/api-gateway-smart-city/genproto/enviromental"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type EnvironmentalHandler struct {
	EnvironmentalAirQualityReading   enviromental.AirQualityReadingServiceClient
	EnvironmentalAirQualityStation   enviromental.AirQualityStationServiceClient
	EnvironmentalNoiceLevelReading   enviromental.NoiseLevelReadingServiceClient
	EnvironmentalPlantRegistry       enviromental.PlantRegistryServiceClient
	EnvironmentalRecyclingCenter     enviromental.RecyclingCenterServiceClient
	EnvironmentalWasteCollection     enviromental.WasteCollectionScheduleServiceClient
	EnvironmentalWaterQualityReport  enviromental.WaterQualityReportServiceClient
	EnvironmentalWaterTreatment      enviromental.WaterTreatmentPlantServiceClient
	EnvironmentalGreenSpace          enviromental.GreenSpaceServiceClient
	EnvironmentalNoiceMonitoringZone enviromental.NoiseMonitoringZoneServiceClient
}

func NewEnvironmentalHandler(conn *grpc.ClientConn) *EnvironmentalHandler {
	return &EnvironmentalHandler{
		EnvironmentalAirQualityReading:   enviromental.NewAirQualityReadingServiceClient(conn),
		EnvironmentalAirQualityStation:   enviromental.NewAirQualityStationServiceClient(conn),
		EnvironmentalNoiceLevelReading:   enviromental.NewNoiseLevelReadingServiceClient(conn),
		EnvironmentalPlantRegistry:       enviromental.NewPlantRegistryServiceClient(conn),
		EnvironmentalRecyclingCenter:     enviromental.NewRecyclingCenterServiceClient(conn),
		EnvironmentalWasteCollection:     enviromental.NewWasteCollectionScheduleServiceClient(conn),
		EnvironmentalWaterQualityReport:  enviromental.NewWaterQualityReportServiceClient(conn),
		EnvironmentalWaterTreatment:      enviromental.NewWaterTreatmentPlantServiceClient(conn),
		EnvironmentalGreenSpace:          enviromental.NewGreenSpaceServiceClient(conn),
		EnvironmentalNoiceMonitoringZone: enviromental.NewNoiseMonitoringZoneServiceClient(conn),
	}
}

// ------------------------ Air Quality Readings Management ------------------------

// CreateAirQualityReading godoc
// @Summary     Create Air Quality Reading
// @Description Records a new air quality reading.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       reading body     enviromental.AirQualityReading true "Air quality reading data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/air_quality_readings [post]
func (e *EnvironmentalHandler) CreateAirQualityReading(ctx *gin.Context) {
	var req enviromental.AirQualityReading
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := e.EnvironmentalAirQualityReading.Create(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Air quality reading already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create air quality reading: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Air quality reading created successfully", "reading_id": req.ReadingId})
}

// DeleteAirQualityReading godoc
// @Summary     Delete Air Quality Reading
// @Description Deletes an air quality reading by ID.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       reading_id path     string true "Reading ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/air_quality_readings/{reading_id} [delete]
func (e *EnvironmentalHandler) DeleteAirQualityReading(ctx *gin.Context) {
	readingID := ctx.Param("reading_id")

	_, err := e.EnvironmentalAirQualityReading.Delete(context.Background(), &enviromental.ById{Id: readingID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Air quality reading not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete air quality reading: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Air quality reading deleted successfully"})
}

// UpdateAirQualityReading godoc
// @Summary     Update Air Quality Reading
// @Description Updates an existing air quality reading.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       reading_id path     string                             true "Reading ID"
// @Param       reading    body     enviromental.AirQualityReading true "Updated air quality reading data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/environmental/air_quality_readings/{reading_id} [put]
func (e *EnvironmentalHandler) UpdateAirQualityReading(ctx *gin.Context) {
	readingID := ctx.Param("reading_id")
	var req enviromental.AirQualityReading
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.ReadingId = readingID
	_, err := e.EnvironmentalAirQualityReading.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Air quality reading not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update air quality reading: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Air quality reading updated successfully"})
}

// GetAirQualityReading godoc
// @Summary     Get Air Quality Reading by ID
// @Description Retrieves a specific air quality reading.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       reading_id path     string true "Reading ID"
// @Success     200     {object} enviromental.AirQualityReading
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/air_quality_readings/{reading_id} [get]
func (e *EnvironmentalHandler) GetAirQualityReading(ctx *gin.Context) {
	readingID := ctx.Param("reading_id")

	res, err := e.EnvironmentalAirQualityReading.GetById(context.Background(), &enviromental.ById{Id: readingID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Air quality reading not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get air quality reading: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllAirQualityReadings godoc
// @Summary     List Air Quality Readings
// @Description Retrieves a list of all air quality readings.
// @Tags        Environmental
// @Produce     json
// @Success     200     {array}  enviromental.AirQualityReading
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/air_quality_readings [get]
func (e *EnvironmentalHandler) GetAllAirQualityReadings(ctx *gin.Context) {

	res, err := e.EnvironmentalAirQualityReading.GetAll(context.Background(), &enviromental.AirQualityReading{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list air quality readings: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.AirQualityReadings)
}

// ------------------------ Air Quality Stations Management ------------------------

// CreateAirQualityStation godoc
// @Summary     Create Air Quality Station
// @Description Registers a new air quality monitoring station.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       station body     enviromental.AirQualityStation true "Air quality station data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/air_quality_stations [post]
func (e *EnvironmentalHandler) CreateAirQualityStation(ctx *gin.Context) {
	var req enviromental.AirQualityStation
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := e.EnvironmentalAirQualityStation.Create(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Air quality station already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create air quality station: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Air quality station created successfully", "station_id": req.StationId})
}

// DeleteAirQualityStation godoc
// @Summary     Delete Air Quality Station
// @Description Removes an air quality station from the system.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       station_id path     string true "Station ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/air_quality_stations/{station_id} [delete]
func (e *EnvironmentalHandler) DeleteAirQualityStation(ctx *gin.Context) {
	stationID := ctx.Param("station_id")

	_, err := e.EnvironmentalAirQualityStation.Delete(context.Background(), &enviromental.ById{Id: stationID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Air quality station not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete air quality station: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Air quality station deleted successfully"})
}

// UpdateAirQualityStation godoc
// @Summary     Update Air Quality Station
// @Description Updates information about an air quality station.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       station_id path     string                             true "Station ID"
// @Param       station    body     enviromental.AirQualityStation true "Updated air quality station data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/environmental/air_quality_stations/{station_id} [put]
func (e *EnvironmentalHandler) UpdateAirQualityStation(ctx *gin.Context) {
	stationID := ctx.Param("station_id")
	var req enviromental.AirQualityStation
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.StationId = stationID

	_, err := e.EnvironmentalAirQualityStation.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Air quality station not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update air quality station: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Air quality station updated successfully"})
}

// GetAirQualityStation godoc
// @Summary     Get Air Quality Station by ID
// @Description Retrieves information about a specific air quality station.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       station_id path     string true "Station ID"
// @Success     200     {object} enviromental.AirQualityStation
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/air_quality_stations/{station_id} [get]
func (e *EnvironmentalHandler) GetAirQualityStation(ctx *gin.Context) {
	stationID := ctx.Param("station_id")

	res, err := e.EnvironmentalAirQualityStation.GetById(context.Background(), &enviromental.ById{Id: stationID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Air quality station not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get air quality station: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllAirQualityStations godoc
// @Summary     List Air Quality Stations
// @Description Retrieves a list of all registered air quality stations.
// @Tags        Environmental
// @Produce     json
// @Success     200     {array}  enviromental.AirQualityStation
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/air_quality_stations [get]
func (e *EnvironmentalHandler) GetAllAirQualityStations(ctx *gin.Context) {

	res, err := e.EnvironmentalAirQualityStation.GetAll(context.Background(), &enviromental.AirQualityStation{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list air quality stations: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.AirQualityStations)
}

// ------------------------ Noise Level Readings Management ------------------------

// CreateNoiseLevelReading godoc
// @Summary     Create Noise Level Reading
// @Description Adds a new noise level reading to the database.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       reading body     enviromental.NoiseLevelReading true "Noise level reading data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/noise_level_readings [post]
func (e *EnvironmentalHandler) CreateNoiseLevelReading(ctx *gin.Context) {
	var req enviromental.NoiseLevelReading
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := e.EnvironmentalNoiceLevelReading.Create(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Noise level reading already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create noise level reading: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Noise level reading created successfully", "reading_id": req.ReadingId})
}

// DeleteNoiseLevelReading godoc
// @Summary     Delete Noise Level Reading
// @Description Removes a noise level reading by its ID.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       reading_id path     string true "Reading ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/noise_level_readings/{reading_id} [delete]
func (e *EnvironmentalHandler) DeleteNoiseLevelReading(ctx *gin.Context) {
	readingID := ctx.Param("reading_id")

	_, err := e.EnvironmentalNoiceLevelReading.Delete(context.Background(), &enviromental.ById{Id: readingID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Noise level reading not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete noise level reading: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Noise level reading deleted successfully"})
}

// UpdateNoiseLevelReading godoc
// @Summary     Update Noise Level Reading
// @Description Modifies an existing noise level reading.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       reading_id path     string                             true "Reading ID"
// @Param       reading    body     enviromental.NoiseLevelReading true "Updated noise level reading data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/environmental/noise_level_readings/{reading_id} [put]
func (e *EnvironmentalHandler) UpdateNoiseLevelReading(ctx *gin.Context) {
	readingID := ctx.Param("reading_id")
	var req enviromental.NoiseLevelReading
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.ReadingId = readingID
	_, err := e.EnvironmentalNoiceLevelReading.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Noise level reading not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update noise level reading: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Noise level reading updated successfully"})
}

// GetNoiseLevelReading godoc
// @Summary     Get Noise Level Reading by ID
// @Description Fetches a specific noise level reading by ID.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       reading_id path     string true "Reading ID"
// @Success     200     {object} enviromental.NoiseLevelReading
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/noise_level_readings/{reading_id} [get]
func (e *EnvironmentalHandler) GetNoiseLevelReading(ctx *gin.Context) {
	readingID := ctx.Param("reading_id")

	res, err := e.EnvironmentalNoiceLevelReading.GetById(context.Background(), &enviromental.ById{Id: readingID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Noise level reading not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get noise level reading: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllNoiseLevelReadings godoc
// @Summary     List Noise Level Readings
// @Description Retrieves a list of noise level readings.
// @Tags        Environmental
// @Produce     json
// @Success     200     {array}  enviromental.NoiseLevelReading
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/noise_level_readings [get]
func (e *EnvironmentalHandler) GetAllNoiseLevelReadings(ctx *gin.Context) {

	res, err := e.EnvironmentalNoiceLevelReading.GetAll(context.Background(), &enviromental.NoiseLevelReading{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list noise level readings: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.NoiseLevelReadings)
}

// ------------------------ Plant Registry Management ------------------------

// CreatePlantRegistry godoc
// @Summary     Create Plant Registry Entry
// @Description Adds a new entry to the plant registry.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       registry body     enviromental.PlantRegistry true "Plant registry data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/plant_registry [post]
func (e *EnvironmentalHandler) CreatePlantRegistry(ctx *gin.Context) {
	var req enviromental.PlantRegistry
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := e.EnvironmentalPlantRegistry.Create(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Plant registry entry already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create plant registry entry: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Plant registry entry created successfully", "registry_id": req.RegistryId})
}

// DeletePlantRegistry godoc
// @Summary     Delete Plant Registry Entry
// @Description Removes an entry from the plant registry.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       registry_id path     string true "Registry ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/plant_registry/{registry_id} [delete]
func (e *EnvironmentalHandler) DeletePlantRegistry(ctx *gin.Context) {
	registryID := ctx.Param("registry_id")

	_, err := e.EnvironmentalPlantRegistry.Delete(context.Background(), &enviromental.ById{Id: registryID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Plant registry entry not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete plant registry entry: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Plant registry entry deleted successfully"})
}

// UpdatePlantRegistry godoc
// @Summary     Update Plant Registry Entry
// @Description Updates information in an existing plant registry entry.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       registry_id path     string                             true "Registry ID"
// @Param       registry    body     enviromental.PlantRegistry true "Updated plant registry data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/environmental/plant_registry/{registry_id} [put]
func (e *EnvironmentalHandler) UpdatePlantRegistry(ctx *gin.Context) {
	registryID := ctx.Param("registry_id")
	var req enviromental.PlantRegistry
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.RegistryId = registryID

	_, err := e.EnvironmentalPlantRegistry.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Plant registry entry not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update plant registry entry: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Plant registry entry updated successfully"})
}

// GetPlantRegistry godoc
// @Summary     Get Plant Registry Entry by ID
// @Description Retrieves details of a specific plant registry entry.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       registry_id path     string true "Registry ID"
// @Success     200     {object} enviromental.PlantRegistry
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/plant_registry/{registry_id} [get]
func (e *EnvironmentalHandler) GetPlantRegistry(ctx *gin.Context) {
	registryID := ctx.Param("registry_id")

	res, err := e.EnvironmentalPlantRegistry.GetById(context.Background(), &enviromental.ById{Id: registryID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Plant registry entry not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get plant registry entry: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllPlantRegistries godoc
// @Summary     List Plant Registry Entries
// @Description Returns a list of all plant registry entries.
// @Tags        Environmental
// @Produce     json
// @Success     200     {array}  enviromental.PlantRegistry
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/plant_registry [get]
func (e *EnvironmentalHandler) GetAllPlantRegistries(ctx *gin.Context) {

	res, err := e.EnvironmentalPlantRegistry.GetAll(context.Background(), &enviromental.PlantRegistry{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list plant registry entries: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.PlantRegistries)
}

// ------------------------ Recycling Centers Management ------------------------

// CreateRecyclingCenter godoc
// @Summary     Create Recycling Center
// @Description Adds a new recycling center to the system.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       center body     enviromental.RecyclingCenter true "Recycling center data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/recycling_centers [post]
func (e *EnvironmentalHandler) CreateRecyclingCenter(ctx *gin.Context) {
	var req enviromental.RecyclingCenter
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := e.EnvironmentalRecyclingCenter.Create(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Recycling center already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create recycling center: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Recycling center created successfully", "center_id": req.CenterId})
}

// DeleteRecyclingCenter godoc
// @Summary     Delete Recycling Center
// @Description Removes a recycling center by its ID.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       center_id path     string true "Center ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/recycling_centers/{center_id} [delete]
func (e *EnvironmentalHandler) DeleteRecyclingCenter(ctx *gin.Context) {
	centerID := ctx.Param("center_id")

	_, err := e.EnvironmentalRecyclingCenter.Delete(context.Background(), &enviromental.ById{Id: centerID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Recycling center not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete recycling center: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Recycling center deleted successfully"})
}

// UpdateRecyclingCenter godoc
// @Summary     Update Recycling Center
// @Description Updates details of a recycling center.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       center_id path     string                             true "Center ID"
// @Param       center    body     enviromental.RecyclingCenter true "Updated recycling center data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/environmental/recycling_centers/{center_id} [put]
func (e *EnvironmentalHandler) UpdateRecyclingCenter(ctx *gin.Context) {
	centerID := ctx.Param("center_id")
	var req enviromental.RecyclingCenter
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.CenterId = centerID

	_, err := e.EnvironmentalRecyclingCenter.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Recycling center not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update recycling center: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Recycling center updated successfully"})
}

// GetRecyclingCenter godoc
// @Summary     Get Recycling Center by ID
// @Description Retrieves details of a specific recycling center.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       center_id path     string true "Center ID"
// @Success     200     {object} enviromental.RecyclingCenter
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router     /v1/environmental/recycling_centers/{center_id} [get]
func (e *EnvironmentalHandler) GetRecyclingCenter(ctx *gin.Context) {
	centerID := ctx.Param("center_id")

	res, err := e.EnvironmentalRecyclingCenter.GetById(context.Background(), &enviromental.ById{Id: centerID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Recycling center not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get recycling center: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllRecyclingCenters godoc
// @Summary     List Recycling Centers
// @Description Retrieves a list of all registered recycling centers.
// @Tags        Environmental
// @Produce     json
// @Success     200     {array}  enviromental.RecyclingCenter
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/recycling_centers [get]
func (e *EnvironmentalHandler) GetAllRecyclingCenters(ctx *gin.Context) {

	res, err := e.EnvironmentalRecyclingCenter.GetAll(context.Background(), &enviromental.RecyclingCenter{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list recycling centers: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.RecyclingCenters)
}

// ------------------------ Waste Collection Schedules Management ------------------------

// CreateWasteCollectionSchedule godoc
// @Summary     Create Waste Collection Schedule
// @Description Adds a new waste collection schedule.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       schedule body     enviromental.WasteCollectionSchedule true "Waste collection schedule data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/waste_collection_schedules [post]
func (e *EnvironmentalHandler) CreateWasteCollectionSchedule(ctx *gin.Context) {
	var req enviromental.WasteCollectionSchedule
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := e.EnvironmentalWasteCollection.Create(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Waste collection schedule already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create waste collection schedule: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Waste collection schedule created successfully", "schedule_id": req.ScheduleId})
}

// DeleteWasteCollectionSchedule godoc
// @Summary     Delete Waste Collection Schedule
// @Description Removes a waste collection schedule by ID.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       schedule_id path     string true "Schedule ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/waste_collection_schedules/{schedule_id} [delete]
func (e *EnvironmentalHandler) DeleteWasteCollectionSchedule(ctx *gin.Context) {
	scheduleID := ctx.Param("schedule_id")

	_, err := e.EnvironmentalWasteCollection.Delete(context.Background(), &enviromental.ById{Id: scheduleID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Waste collection schedule not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete waste collection schedule: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Waste collection schedule deleted successfully"})
}

// UpdateWasteCollectionSchedule godoc
// @Summary     Update Waste Collection Schedule
// @Description Updates information for a waste collection schedule.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       schedule_id path     string                             true "Schedule ID"
// @Param       schedule    body     enviromental.WasteCollectionSchedule true "Updated waste collection schedule data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/environmental/waste_collection_schedules/{schedule_id} [put]
func (e *EnvironmentalHandler) UpdateWasteCollectionSchedule(ctx *gin.Context) {
	scheduleID := ctx.Param("schedule_id")
	var req enviromental.WasteCollectionSchedule
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.ScheduleId = scheduleID
	_, err := e.EnvironmentalWasteCollection.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Waste collection schedule not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update waste collection schedule: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Waste collection schedule updated successfully"})
}

// GetWasteCollectionSchedule godoc
// @Summary     Get Waste Collection Schedule by ID
// @Description Retrieves a specific waste collection schedule.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       schedule_id path     string true "Schedule ID"
// @Success     200     {object} enviromental.WasteCollectionSchedule
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/waste_collection_schedules/{schedule_id} [get]
func (e *EnvironmentalHandler) GetWasteCollectionSchedule(ctx *gin.Context) {
	scheduleID := ctx.Param("schedule_id")

	res, err := e.EnvironmentalWasteCollection.GetById(context.Background(), &enviromental.ById{Id: scheduleID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Waste collection schedule not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get waste collection schedule: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllWasteCollectionSchedules godoc
// @Summary     List Waste Collection Schedules
// @Description Retrieves a list of waste collection schedules.
// @Tags        Environmental
// @Produce     json
// @Success     200     {array}  enviromental.WasteCollectionSchedule
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/waste_collection_schedules [get]
func (e *EnvironmentalHandler) GetAllWasteCollectionSchedules(ctx *gin.Context) {

	res, err := e.EnvironmentalWasteCollection.GetAll(context.Background(), &enviromental.WasteCollectionSchedule{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list waste collection schedules: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.WasteCollectionSchedules)
}

// ------------------------ Water Quality Reports Management ------------------------

// CreateWaterQualityReport godoc
// @Summary     Create Water Quality Report
// @Description Generates a new water quality report.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       report body     enviromental.WaterQualityReport true "Water quality report data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/water_quality_reports [post]
func (e *EnvironmentalHandler) CreateWaterQualityReport(ctx *gin.Context) {
	var req enviromental.WaterQualityReport
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := e.EnvironmentalWaterQualityReport.Create(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Water quality report already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create water quality report: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Water quality report created successfully", "report_id": req.ReportId})
}

// DeleteWaterQualityReport godoc
// @Summary     Delete Water Quality Report
// @Description Deletes a water quality report by ID.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       report_id path     string true "Report ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/water_quality_reports/{report_id} [delete]
func (e *EnvironmentalHandler) DeleteWaterQualityReport(ctx *gin.Context) {
	reportID := ctx.Param("report_id")

	_, err := e.EnvironmentalWaterQualityReport.Delete(context.Background(), &enviromental.ById{Id: reportID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Water quality report not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete water quality report: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Water quality report deleted successfully"})
}

// UpdateWaterQualityReport godoc
// @Summary     Update Water Quality Report
// @Description Updates information in a water quality report.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       report_id path     string                             true "Report ID"
// @Param       report    body     enviromental.WaterQualityReport true "Updated water quality report data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/environmental/water_quality_reports/{report_id} [put]
func (e *EnvironmentalHandler) UpdateWaterQualityReport(ctx *gin.Context) {
	reportID := ctx.Param("report_id")
	var req enviromental.WaterQualityReport
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.ReportId = reportID
	_, err := e.EnvironmentalWaterQualityReport.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Water quality report not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update water quality report: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Water quality report updated successfully"})
}

// GetWaterQualityReport godoc
// @Summary     Get Water Quality Report by ID
// @Description Retrieves a specific water quality report.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       report_id path     string true "Report ID"
// @Success     200     {object} enviromental.WaterQualityReport
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/water_quality_reports/{report_id} [get]
func (e *EnvironmentalHandler) GetWaterQualityReport(ctx *gin.Context) {
	reportID := ctx.Param("report_id")

	res, err := e.EnvironmentalWaterQualityReport.GetById(context.Background(), &enviromental.ById{Id: reportID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Water quality report not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get water quality report: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllWaterQualityReports godoc
// @Summary     List Water Quality Reports
// @Description Retrieves a list of water quality reports.
// @Tags        Environmental
// @Produce     json
// @Success     200     {array}  enviromental.WaterQualityReport
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/water_quality_reports [get]
func (e *EnvironmentalHandler) GetAllWaterQualityReports(ctx *gin.Context) {

	res, err := e.EnvironmentalWaterQualityReport.GetAll(context.Background(), &enviromental.WaterQualityReport{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list water quality reports: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.WaterQualityReports)
}

// ------------------------ Water Treatment Plants Management ------------------------

// CreateWaterTreatmentPlant godoc
// @Summary     Create Water Treatment Plant
// @Description Adds a new water treatment plant to the system.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       plant body     enviromental.WaterTreatmentPlant true "Water treatment plant data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/water_treatment_plants [post]
func (e *EnvironmentalHandler) CreateWaterTreatmentPlant(ctx *gin.Context) {
	var req enviromental.WaterTreatmentPlant
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := e.EnvironmentalWaterTreatment.Create(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Water treatment plant already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create water treatment plant: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Water treatment plant created successfully", "plant_id": req.PlantId})
}

// DeleteWaterTreatmentPlant godoc
// @Summary     Delete Water Treatment Plant
// @Description Removes a water treatment plant by its ID.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       plant_id path     string true "Plant ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/water_treatment_plants/{plant_id} [delete]
func (e *EnvironmentalHandler) DeleteWaterTreatmentPlant(ctx *gin.Context) {
	plantID := ctx.Param("plant_id")

	_, err := e.EnvironmentalWaterTreatment.Delete(context.Background(), &enviromental.ById{Id: plantID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Water treatment plant not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete water treatment plant: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Water treatment plant deleted successfully"})
}

// UpdateWaterTreatmentPlant godoc
// @Summary     Update Water Treatment Plant
// @Description Updates details of a water treatment plant.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       plant_id path     string                             true "Plant ID"
// @Param       plant    body     enviromental.WaterTreatmentPlant true "Updated water treatment plant data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/environmental/water_treatment_plants/{plant_id} [put]
func (e *EnvironmentalHandler) UpdateWaterTreatmentPlant(ctx *gin.Context) {
	plantID := ctx.Param("plant_id")
	var req enviromental.WaterTreatmentPlant
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.PlantId = plantID

	_, err := e.EnvironmentalWaterTreatment.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Water treatment plant not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update water treatment plant: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Water treatment plant updated successfully"})
}

// GetWaterTreatmentPlant godoc
// @Summary     Get Water Treatment Plant by ID
// @Description Retrieves details of a specific water treatment plant.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       plant_id path     string true "Plant ID"
// @Success     200     {object} enviromental.WaterTreatmentPlant
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/water_treatment_plants/{plant_id} [get]
func (e *EnvironmentalHandler) GetWaterTreatmentPlant(ctx *gin.Context) {
	plantID := ctx.Param("plant_id")

	res, err := e.EnvironmentalWaterTreatment.GetById(context.Background(), &enviromental.ById{Id: plantID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Water treatment plant not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get water treatment plant: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllWaterTreatmentPlants godoc
// @Summary     List Water Treatment Plants
// @Description Retrieves a list of all water treatment plants.
// @Tags        Environmental
// @Produce     json
// @Success     200     {array}  enviromental.WaterTreatmentPlant
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/water_treatment_plants [get]
func (e *EnvironmentalHandler) GetAllWaterTreatmentPlants(ctx *gin.Context) {

	res, err := e.EnvironmentalWaterTreatment.GetAll(context.Background(), &enviromental.WaterTreatmentPlant{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list water treatment plants: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.WaterTreatmentPlants)
}

// ------------------------ Green Space Management ------------------------

// CreateGreenSpace godoc
// @Summary     Create Green Space
// @Description Registers a new green space.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       space body     enviromental.GreenSpace true "Green space data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/green_spaces [post]
func (e *EnvironmentalHandler) CreateGreenSpace(ctx *gin.Context) {
	var req enviromental.GreenSpace
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := e.EnvironmentalGreenSpace.Create(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Green space already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create green space: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Green space created successfully", "space_id": req.SpaceId})
}

// DeleteGreenSpace godoc
// @Summary     Delete Green Space
// @Description Removes a green space from the system.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       space_id path     string true "Space ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/green_spaces/{space_id} [delete]
func (e *EnvironmentalHandler) DeleteGreenSpace(ctx *gin.Context) {
	spaceID := ctx.Param("space_id")

	_, err := e.EnvironmentalGreenSpace.Delete(context.Background(), &enviromental.ById{Id: spaceID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Green space not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete green space: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Green space deleted successfully"})
}

// UpdateGreenSpace godoc
// @Summary     Update Green Space
// @Description Updates information about a green space.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       space_id path     string                             true "Space ID"
// @Param       space    body     enviromental.GreenSpace true "Updated green space data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/environmental/green_spaces/{space_id} [put]
func (e *EnvironmentalHandler) UpdateGreenSpace(ctx *gin.Context) {
	spaceID := ctx.Param("space_id")
	var req enviromental.GreenSpace
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.SpaceId = spaceID

	_, err := e.EnvironmentalGreenSpace.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Green space not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update green space: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Green space updated successfully"})
}

// GetGreenSpace godoc
// @Summary     Get Green Space by ID
// @Description Retrieves details of a specific green space.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       space_id path     string true "Space ID"
// @Success     200     {object} enviromental.GreenSpace
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/green_spaces/{space_id} [get]
func (e *EnvironmentalHandler) GetGreenSpace(ctx *gin.Context) {
	spaceID := ctx.Param("space_id")

	res, err := e.EnvironmentalGreenSpace.GetById(context.Background(), &enviromental.ById{Id: spaceID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Green space not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get green space: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllGreenSpaces godoc
// @Summary     List Green Spaces
// @Description Retrieves a list of all registered green spaces.
// @Tags        Environmental
// @Produce     json
// @Success     200     {array}  enviromental.GreenSpace
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/green_spaces [get]
func (e *EnvironmentalHandler) GetAllGreenSpaces(ctx *gin.Context) {

	res, err := e.EnvironmentalGreenSpace.GetAll(context.Background(), &enviromental.GreenSpace{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list green spaces: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.GreenSpaces)
}

// ------------------------ Noise Monitoring Zones Management ------------------------

// CreateNoiseMonitoringZone godoc
// @Summary     Create Noise Monitoring Zone
// @Description Defines a new zone for noise monitoring.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       zone body     enviromental.NoiseMonitoringZone true "Noise monitoring zone data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/noise_monitoring_zones [post]
func (e *EnvironmentalHandler) CreateNoiseMonitoringZone(ctx *gin.Context) {
	var req enviromental.NoiseMonitoringZone
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := e.EnvironmentalNoiceMonitoringZone.Create(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Noise monitoring zone already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create noise monitoring zone: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Noise monitoring zone created successfully", "zone_id": req.ZoneId})
}

// DeleteNoiseMonitoringZone godoc
// @Summary     Delete Noise Monitoring Zone
// @Description Removes a noise monitoring zone by its ID.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       zone_id path     string true "Zone ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/noise_monitoring_zones/{zone_id} [delete]
func (e *EnvironmentalHandler) DeleteNoiseMonitoringZone(ctx *gin.Context) {
	zoneID := ctx.Param("zone_id")

	_, err := e.EnvironmentalNoiceMonitoringZone.Delete(context.Background(), &enviromental.ById{Id: zoneID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Noise monitoring zone not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete noise monitoring zone: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Noise monitoring zone deleted successfully"})
}

// UpdateNoiseMonitoringZone godoc
// @Summary     Update Noise Monitoring Zone
// @Description Updates information about a noise monitoring zone.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       zone_id path     string                             true "Zone ID"
// @Param       zone    body     enviromental.NoiseMonitoringZone true "Updated noise monitoring zone data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/environmental/noise_monitoring_zones/{zone_id} [put]
func (e *EnvironmentalHandler) UpdateNoiseMonitoringZone(ctx *gin.Context) {
	zoneID := ctx.Param("zone_id")
	var req enviromental.NoiseMonitoringZone
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.ZoneId = zoneID

	_, err := e.EnvironmentalNoiceMonitoringZone.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Noise monitoring zone not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update noise monitoring zone: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Noise monitoring zone updated successfully"})
}

// GetNoiseMonitoringZone godoc
// @Summary     Get Noise Monitoring Zone by ID
// @Description Retrieves information about a specific noise monitoring zone.
// @Tags        Environmental
// @Accept      json
// @Produce     json
// @Param       zone_id path     string true "Zone ID"
// @Success     200     {object} enviromental.NoiseMonitoringZone
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/noise_monitoring_zones/{zone_id} [get]
func (e *EnvironmentalHandler) GetNoiseMonitoringZone(ctx *gin.Context) {
	zoneID := ctx.Param("zone_id")

	res, err := e.EnvironmentalNoiceMonitoringZone.GetById(context.Background(), &enviromental.ById{Id: zoneID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Noise monitoring zone not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get noise monitoring zone: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllNoiseMonitoringZones godoc
// @Summary     List Noise Monitoring Zones
// @Description Retrieves a list of all noise monitoring zones.
// @Tags        Environmental
// @Produce     json
// @Success     200     {array}  enviromental.NoiseMonitoringZone
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/environmental/noise_monitoring_zones [get]
func (e *EnvironmentalHandler) GetAllNoiseMonitoringZones(ctx *gin.Context) {

	res, err := e.EnvironmentalNoiceMonitoringZone.GetAll(context.Background(), &enviromental.NoiseMonitoringZone{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list noise monitoring zones: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.NoiseMonitoringZones)
}
