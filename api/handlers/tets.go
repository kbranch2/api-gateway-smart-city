package handlers

import "github.com/gin-gonic/gin"

// @Summary Get category
// @Description Get a category by ID
// @Tags category
// @Accept json
// @Produce json
// @Success 200 {object} string
// @Failure 400 {object} string "Invalid category  ID"
// @Failure 500 {object} string "Server error"
// @Security BearerAuth
// @Router /test-user [get]
func (h *HTTPHandler) GetUser(c *gin.Context) {
	h.Logger.INFO.Println("Get User")
	c.JSON(200, gin.H{
		"message": "Get User",
	})
}

// @Summary Get category
// @Description Get a category by ID
// @Tags category
// @Accept json
// @Produce json
// @Success 200 {object} string
// @Failure 400 {object} string "Invalid category  ID"
// @Failure 500 {object} string "Server error"
// @Security BearerAuth
// @Router /test-admin [get]
func (h *HTTPHandler) GetUserForAdmin(c *gin.Context) {
	h.Logger.INFO.Println("Get User for admin")
	c.JSON(200, gin.H{
		"message": "Get User for admin",
	})
}
