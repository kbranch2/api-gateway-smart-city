package handlers

import (
	pbtransportation "github.com/smart-city/api-gateway-smart-city/genproto/transport"
	"google.golang.org/grpc"

	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type TransportationHandler struct {
	TransportationIncident         pbtransportation.IncidentServiceClient
	TransportationParkingLot       pbtransportation.ParkingLotServiceClient
	TransportationRoad             pbtransportation.RoadServiceClient
	TransportationRouteSchedule    pbtransportation.RouteScheduleServiceClient
	TransportationTrafficCondition pbtransportation.TrafficConditionServiceClient
	TransportationVehicle          pbtransportation.VehicleServiceClient
}

func NewTransportationHandler(conn *grpc.ClientConn) *TransportationHandler {
	return &TransportationHandler{
		TransportationIncident:         pbtransportation.NewIncidentServiceClient(conn),
		TransportationParkingLot:       pbtransportation.NewParkingLotServiceClient(conn),
		TransportationRoad:             pbtransportation.NewRoadServiceClient(conn),
		TransportationRouteSchedule:    pbtransportation.NewRouteScheduleServiceClient(conn),
		TransportationTrafficCondition: pbtransportation.NewTrafficConditionServiceClient(conn),
		TransportationVehicle:          pbtransportation.NewVehicleServiceClient(conn),
	}
}

// ------------------------ Incident Management ------------------------

// CreateIncident godoc
// @Summary     Create a new Traffic Incident
// @Description Create a new traffic incident report.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       incident body     pbtransportation.Incident true "Incident details"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/incidents [post]
func (t *TransportationHandler) CreateIncident(ctx *gin.Context) {
	var req pbtransportation.Incident
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := t.TransportationIncident.Create(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Incident already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create incident: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Incident created successfully", "incident_id": req.IncidentId})
}

// DeleteIncident godoc
// @Summary     Delete Traffic Incident
// @Description Delete a traffic incident by its ID.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       incident_id path     string true "Incident ID"
// @Success     204 {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/incidents/{incident_id} [delete]
func (t *TransportationHandler) DeleteIncident(ctx *gin.Context) {
	incidentID := ctx.Param("incident_id")

	_, err := t.TransportationIncident.Delete(context.Background(), &pbtransportation.ById{Id: incidentID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Incident not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete incident: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Incident deleted successfully"})
}

// UpdateIncident godoc
// @Summary     Update Traffic Incident
// @Description Update a traffic incident report.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       incident_id path     string true "Incident ID"
// @Param       incident body     pbtransportation.Incident true "Update Incident details"
// @Success     200     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/incidents/{incident_id} [put]
func (t *TransportationHandler) UpdateIncident(ctx *gin.Context) {
	var req pbtransportation.Incident
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}
	incidentID := ctx.Param("incident_id")
	req.IncidentId = incidentID

	_, err := t.TransportationIncident.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "Incident already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update incident: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": "Incident Updated successfully", "incident_id": req.IncidentId})
}

// GetIncident godoc
// @Summary     Get Traffic Incident by ID
// @Description Retrieve a traffic incident report by its ID.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       incident_id path     string true "Incident ID"
// @Success     200     {object} pbtransportation.Incident
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/incidents/{incident_id} [get]
func (t *TransportationHandler) GetIncident(ctx *gin.Context) {
	incidentID := ctx.Param("incident_id")

	res, err := t.TransportationIncident.GetById(context.Background(), &pbtransportation.ById{Id: incidentID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Incident not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get incident: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)
}

// GetAllIncidents godoc
// @Summary     List  Traffic Incidents
// @Description Get a list of all  traffic incident reports.
// @Tags        Transportation
// @Produce     json
// @Success     200     {array}  pbtransportation.Incident
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/incidents [get]
func (t *TransportationHandler) GetAllIncidents(ctx *gin.Context) {

	res, err := t.TransportationIncident.GetAll(context.Background(), &pbtransportation.Incident{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch incidents: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res.Incidents)
}

// ------------------------ Parking Lot Management ------------------------

// CreateParkingLot godoc
// @Summary     Create Parking Lot
// @Description Create a new parking lot entry.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       parkingLot body     pbtransportation.ParkingLot true "Parking lot data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/parking_lots [post]
func (t *TransportationHandler) CreateParkingLot(ctx *gin.Context) {
	var req pbtransportation.ParkingLot
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := t.TransportationParkingLot.Create(context.Background(), &req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create Parking lot: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Parking lot created successfully", "lot_id": req.LotId})
}

// DeleteParkingLot godoc
// @Summary     Delete Parking Lot
// @Description Delete a parking lot entry by its ID.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       lot_id path     string true "Lot ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/parking_lots/{lot_id} [delete]
func (t *TransportationHandler) DeleteParkingLot(ctx *gin.Context) {
	lotID := ctx.Param("lot_id")

	_, err := t.TransportationParkingLot.Delete(context.Background(), &pbtransportation.ById{Id: lotID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Parking Lot not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete Parking Lot: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Parking Lot deleted successfully"})
}

// UpdateParkingLot godoc
// @Summary     Update Parking Lot
// @Description Update details of an existing parking lot.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       lot_id path     string                             true "Lot ID"
// @Param       parkingLot    body     pbtransportation.ParkingLot true "Updated parking lot data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/transport/parking_lots/{lot_id} [put]
func (t *TransportationHandler) UpdateParkingLot(ctx *gin.Context) {
	lotID := ctx.Param("lot_id")
	var req pbtransportation.ParkingLot
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.LotId = lotID

	_, err := t.TransportationParkingLot.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Parking Lot not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update Parking Lot: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Parking Lot updated successfully"})
}

// GetParkingLot godoc
// @Summary     Get Parking Lot by ID
// @Description Retrieve parking lot information by its ID.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       lot_id path     string true "Lot ID"
// @Success     200     {object} pbtransportation.ParkingLot
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/parking_lots/{lot_id} [get]
func (t *TransportationHandler) GetParkingLot(ctx *gin.Context) {
	lotID := ctx.Param("lot_id")

	res, err := t.TransportationParkingLot.GetById(context.Background(), &pbtransportation.ById{Id: lotID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Parking Lot not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get Parking Lot: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllParkingLots godoc
// @Summary     List All Parking Lots
// @Description Get a list of all registered parking lots.
// @Tags        Transportation
// @Produce     json
// @Success     200     {array}  pbtransportation.ParkingLot
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/parking_lots [get]
func (t *TransportationHandler) GetAllParkingLots(ctx *gin.Context) {

	res, err := t.TransportationParkingLot.GetAll(context.Background(), &pbtransportation.ParkingLot{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list Parking Lots: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.ParkingLots)
}

// ------------------------ Road Management ------------------------

// CreateRoad godoc
// @Summary     Create Road Segment
// @Description Register a new road segment.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       road body     pbtransportation.Road true "Road segment data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/roads [post]
func (t *TransportationHandler) CreateRoad(ctx *gin.Context) {
	var req pbtransportation.Road
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := t.TransportationRoad.Create(context.Background(), &req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create Road: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Road segment created successfully", "road_id": req.RoadId})
}

// DeleteRoad godoc
// @Summary     Delete Road Segment
// @Description Delete a road segment by its ID.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       road_id path     string true "Road ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/roads/{road_id} [delete]
func (t *TransportationHandler) DeleteRoad(ctx *gin.Context) {
	roadID := ctx.Param("road_id")

	_, err := t.TransportationRoad.Delete(context.Background(), &pbtransportation.ById{Id: roadID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Road segment not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete road: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Road segment deleted successfully"})
}

// UpdateRoad godoc
// @Summary     Update Road Segment
// @Description Update information about an existing road segment.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       road_id path     string                             true "Road ID"
// @Param       road    body     pbtransportation.Road true "Updated road segment data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/transport/roads/{road_id} [put]
func (t *TransportationHandler) UpdateRoad(ctx *gin.Context) {
	roadID := ctx.Param("road_id")
	var req pbtransportation.Road
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.RoadId = roadID
	_, err := t.TransportationRoad.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Road segment not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update road: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Road segment updated successfully"})
}

// GetRoad godoc
// @Summary     Get Road Segment by ID
// @Description Get details of a specific road segment.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       road_id path     string true "Road ID"
// @Success     200     {object} pbtransportation.Road
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/roads/{road_id} [get]
func (t *TransportationHandler) GetRoad(ctx *gin.Context) {
	roadID := ctx.Param("road_id")

	res, err := t.TransportationRoad.GetById(context.Background(), &pbtransportation.ById{Id: roadID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Road segment not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get road segment: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllRoads godoc
// @Summary     List All Road Segments
// @Description Retrieve a list of all road segments.
// @Tags        Transportation
// @Produce     json
// @Success     200     {array}  pbtransportation.Road
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/roads [get]
func (t *TransportationHandler) GetAllRoads(ctx *gin.Context) {

	res, err := t.TransportationRoad.GetAll(context.Background(), &pbtransportation.Road{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list road segments: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.Roads)
}

// ------------------------ Route Schedule Management ------------------------

// CreateRouteSchedule godoc
// @Summary     Create Route Schedule
// @Description Define a new schedule for a specific route.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       routeSchedule body     pbtransportation.RouteSchedule true "Route schedule data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/route_schedules [post]
func (t *TransportationHandler) CreateRouteSchedule(ctx *gin.Context) {
	var req pbtransportation.RouteSchedule
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := t.TransportationRouteSchedule.Create(context.Background(), &req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create Route Schedule: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Route schedule created successfully", "schedule_id": req.ScheduleId})
}

// DeleteRouteSchedule godoc
// @Summary     Delete Route Schedule
// @Description Remove a route schedule by its ID.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       schedule_id path     string true "Schedule ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/route_schedules/{schedule_id} [delete]
func (t *TransportationHandler) DeleteRouteSchedule(ctx *gin.Context) {
	scheduleID := ctx.Param("schedule_id")

	_, err := t.TransportationRouteSchedule.Delete(context.Background(), &pbtransportation.ById{Id: scheduleID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Route schedule not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete route schedule: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Route schedule deleted successfully"})
}

// UpdateRouteSchedule godoc
// @Summary     Update Route Schedule
// @Description Modify an existing route schedule.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       schedule_id path     string                             true "Schedule ID"
// @Param       routeSchedule    body     pbtransportation.RouteSchedule true "Updated route schedule data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/transport/route_schedules/{schedule_id} [put]
func (t *TransportationHandler) UpdateRouteSchedule(ctx *gin.Context) {
	scheduleID := ctx.Param("schedule_id")
	var req pbtransportation.RouteSchedule
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.ScheduleId = scheduleID

	_, err := t.TransportationRouteSchedule.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Route Schedule not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update Route Schedule: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Route Schedule updated successfully"})
}

// GetRouteSchedule godoc
// @Summary     Get Route Schedule by ID
// @Description Get information about a specific route schedule.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       schedule_id path     string true "Schedule ID"
// @Success     200     {object} pbtransportation.RouteSchedule
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/route_schedules/{schedule_id} [get]
func (t *TransportationHandler) GetRouteSchedule(ctx *gin.Context) {
	scheduleID := ctx.Param("schedule_id")

	res, err := t.TransportationRouteSchedule.GetById(context.Background(), &pbtransportation.ById{Id: scheduleID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Route Schedule not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get Route Schedule: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllRouteSchedules godoc
// @Summary     List All Route Schedules
// @Description Retrieve a complete list of route schedules.
// @Tags        Transportation
// @Produce     json
// @Success     200     {array}  pbtransportation.RouteSchedule
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/route_schedules [get]
func (t *TransportationHandler) GetAllRouteSchedules(ctx *gin.Context) {

	res, err := t.TransportationRouteSchedule.GetAll(context.Background(), &pbtransportation.RouteSchedule{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list Route Schedules: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.RouteSchedules)
}

// ------------------------ Traffic Condition Management ------------------------

// CreateTrafficCondition godoc
// @Summary     Create Traffic Condition Report
// @Description Report current traffic conditions on a road segment.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       trafficCondition body     pbtransportation.TrafficCondition true "Traffic condition data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/traffic_conditions [post]
func (t *TransportationHandler) CreateTrafficCondition(ctx *gin.Context) {
	var req pbtransportation.TrafficCondition
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := t.TransportationTrafficCondition.Create(context.Background(), &req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create Traffic Condition: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Traffic condition report created successfully", "condition_id": req.ConditionId})
}

// DeleteTrafficCondition godoc
// @Summary     Delete Traffic Condition Report
// @Description Remove a traffic condition report by its ID.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       condition_id path     string true "Condition ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/traffic_conditions/{condition_id} [delete]
func (t *TransportationHandler) DeleteTrafficCondition(ctx *gin.Context) {
	conditionID := ctx.Param("condition_id")

	_, err := t.TransportationTrafficCondition.Delete(context.Background(), &pbtransportation.ById{Id: conditionID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Traffic condition report not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete traffic condition report: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Traffic condition report deleted successfully"})
}

// UpdateTrafficCondition godoc
// @Summary     Update Traffic Condition Report
// @Description Update existing traffic condition information.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       condition_id path     string                             true "Condition ID"
// @Param       trafficCondition    body     pbtransportation.TrafficCondition true "Updated traffic condition data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/transport/traffic_conditions/{condition_id} [put]
func (t *TransportationHandler) UpdateTrafficCondition(ctx *gin.Context) {
	conditionID := ctx.Param("condition_id")
	var req pbtransportation.TrafficCondition
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.ConditionId = conditionID
	_, err := t.TransportationTrafficCondition.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Traffic Condition not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update Traffic Condition: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Traffic Condition updated successfully"})
}

// GetTrafficCondition godoc
// @Summary     Get Traffic Condition Report by ID
// @Description Get a specific traffic condition report.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       condition_id path     string true "Condition ID"
// @Success     200     {object} pbtransportation.TrafficCondition
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/traffic_conditions/{condition_id} [get]
func (t *TransportationHandler) GetTrafficCondition(ctx *gin.Context) {
	conditionID := ctx.Param("condition_id")

	res, err := t.TransportationTrafficCondition.GetById(context.Background(), &pbtransportation.ById{Id: conditionID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Traffic Condition not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get Traffic Condition: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllTrafficConditions godoc
// @Summary     List Traffic Condition Reports
// @Description Get a list of traffic condition reports.
// @Tags        Transportation
// @Produce     json
// @Success     200     {array}  pbtransportation.TrafficCondition
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/traffic_conditions [get]
func (t *TransportationHandler) GetAllTrafficConditions(ctx *gin.Context) {

	res, err := t.TransportationTrafficCondition.GetAll(context.Background(), &pbtransportation.TrafficCondition{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list Traffic Conditions: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.TrafficConditions)
}

// ------------------------ Vehicle Management ------------------------

// CreateVehicle godoc
// @Summary     Register New Vehicle
// @Description Register a new vehicle in the transportation system.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       vehicle body     pbtransportation.Vehicle true "Vehicle data"
// @Success     201     {object} map[string]interface{}
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/vehicles [post]
func (t *TransportationHandler) CreateVehicle(ctx *gin.Context) {
	var req pbtransportation.Vehicle
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	_, err := t.TransportationVehicle.Create(context.Background(), &req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create Vehicle: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "Vehicle registered successfully", "vehicle_id": req.VehicleId})
}

// DeleteVehicle godoc
// @Summary     Delete Vehicle Registration
// @Description Remove a vehicle from the system by its ID.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       vehicle_id path     string true "Vehicle ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/vehicles/{vehicle_id} [delete]
func (t *TransportationHandler) DeleteVehicle(ctx *gin.Context) {
	vehicleID := ctx.Param("vehicle_id")

	_, err := t.TransportationVehicle.Delete(context.Background(), &pbtransportation.ById{Id: vehicleID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Vehicle  not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete vehicle : " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Vehicle registration deleted successfully"})
}

// UpdateVehicle godoc
// @Summary     Update Vehicle Information
// @Description Update details of a registered vehicle.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       vehicle_id path     string                             true "Vehicle ID"
// @Param       vehicle    body     pbtransportation.Vehicle true "Updated vehicle data"
// @Success     200         {object} map[string]interface{}
// @Failure     400         {object} map[string]interface{}
// @Failure     404         {object} map[string]interface{}
// @Failure     500         {object} map[string]interface{}
// @Router      /v1/transport/vehicles/{vehicle_id} [put]
func (t *TransportationHandler) UpdateVehicle(ctx *gin.Context) {
	vehicleID := ctx.Param("vehicle_id")
	var req pbtransportation.Vehicle
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}
	req.VehicleId = vehicleID
	_, err := t.TransportationVehicle.Update(context.Background(), &req)
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Vehicle not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update Vehicle: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Vehicle registration updated successfully"})
}

// GetVehicle godoc
// @Summary     Get Vehicle Information by ID
// @Description Retrieve details of a specific vehicle by its ID.
// @Tags        Transportation
// @Accept      json
// @Produce     json
// @Param       vehicle_id path     string true "Vehicle ID"
// @Success     200     {object} pbtransportation.Vehicle
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/vehicles/{vehicle_id} [get]
func (t *TransportationHandler) GetVehicle(ctx *gin.Context) {
	vehicleID := ctx.Param("vehicle_id")

	res, err := t.TransportationVehicle.GetById(context.Background(), &pbtransportation.ById{Id: vehicleID})
	if err != nil {
		st, ok := status.FromError(err)
		if ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Vehicle  not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get Vehicle: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res)

}

// GetAllVehicles godoc
// @Summary     List Registered Vehicles
// @Description Get a list of all vehicles registered in the system.
// @Tags        Transportation
// @Produce     json
// @Success     200     {array}  pbtransportation.Vehicle
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/transport/vehicles [get]
func (t *TransportationHandler) GetAllVehicles(ctx *gin.Context) {

	res, err := t.TransportationVehicle.GetAll(context.Background(), &pbtransportation.Vehicle{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to list Vehicles: " + err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, res.Vehicles)
}
