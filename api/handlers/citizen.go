package handlers

import (
	pbcitizen "github.com/smart-city/api-gateway-smart-city/genproto/citizen"
	"google.golang.org/grpc"
)

type CitizenHandler struct {
	Citizen pbcitizen.CitizenServiceClient
}

func NewCitizenHandler(conn *grpc.ClientConn) *CitizenHandler {
	return &CitizenHandler{pbcitizen.NewCitizenServiceClient(conn)}
}

// CreateCitizen godoc
// // @Summary     Create a new Citizen
// // @Description Registers a new citizen in the system.
// // @Tags        Citizen
// // @Accept      json
// // @Produce     json
// // @Param       citizen body     pbcitizen.CreateCitizenRequest true "Citizen details"
// // @Success     201     {object} pbcitizen.Citizen
// // @Failure     400     {object} map[string]interface{}
// // @Failure     500     {object} map[string]interface{}
// // @Router      /v1/citizen [post]
// func (c *CitizenHandler) CreateCitizen(ctx *gin.Context) {
// 	var req pbcitizen.CreateCitizenRequest
// 	if err := ctx.ShouldBindJSON(&req); err != nil {
// 		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
// 		return
// 	}

// 	res, err := c.Citizen.CreateCitizen(context.Background(), &req)
// 	if err != nil {
// 		st, ok := status.FromError(err)
// 		if ok && st.Code() == codes.AlreadyExists {
// 			ctx.JSON(http.StatusConflict, gin.H{"error": "Citizen already exists"})
// 			return
// 		}
// 		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create citizen: " + err.Error()})
// 		return
// 	}

// 	ctx.JSON(http.StatusCreated, res.Citizen)
// }

// // GetCitizen godoc
// // @Summary     Get Citizen by ID
// // @Description Retrieves details of a citizen by their ID.
// // @Tags        Citizen
// // @Accept      json
// // @Produce     json
// // @Param       citizen_id path     string true "Citizen ID"
// // @Success     200     {object} pbcitizen.Citizen
// // @Failure     400     {object} map[string]interface{}
// // @Failure     404     {object} map[string]interface{}
// // @Failure     500     {object} map[string]interface{}
// // @Router      /v1/citizen/{citizen_id} [get]
// func (c *CitizenHandler) GetCitizen(ctx *gin.Context) {
// 	citizenID := ctx.Param("citizen_id")

// 	res, err := c.Citizen.GetCitizen(context.Background(), &pbcitizen.GetCitizenRequest{CitizenId: citizenID})
// 	if err != nil {
// 		st, ok := status.FromError(err)
// 		if ok && st.Code() == codes.NotFound {
// 			ctx.JSON(http.StatusNotFound, gin.H{"error": "Citizen not found"})
// 			return
// 		}
// 		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get citizen: " + err.Error()})
// 		return
// 	}

// 	ctx.JSON(http.StatusOK, res.Citizen)
// }

// // UpdateCitizen godoc
// // @Summary     Update Citizen
// // @Description Updates information about an existing citizen.
// // @Tags        Citizen
// // @Accept      json
// // @Produce     json
// // @Param       citizen_id path     string                             true "Citizen ID"
// // @Param       citizen    body     pbcitizen.UpdateCitizenRequest true "Updated citizen details"
// // @Success     200         {object} pbcitizen.Citizen
// // @Failure     400         {object} map[string]interface{}
// // @Failure     404         {object} map[string]interface{}
// // @Failure     500         {object} map[string]interface{}
// // @Router      /v1/citizen/{citizen_id} [put]
// func (c *CitizenHandler) UpdateCitizen(ctx *gin.Context) {
// 	citizenID := ctx.Param("citizen_id")
// 	var req pbcitizen.UpdateCitizenRequest
// 	if err := ctx.ShouldBindJSON(&req); err != nil {
// 		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
// 		return
// 	}
// 	req.CitizenId = citizenID

// 	res, err := c.Citizen.UpdateCitizen(context.Background(), &req)
// 	if err != nil {
// 		st, ok := status.FromError(err)
// 		if ok && st.Code() == codes.NotFound {
// 			ctx.JSON(http.StatusNotFound, gin.H{"error": "Citizen not found"})
// 			return
// 		}
// 		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update citizen: " + err.Error()})
// 		return
// 	}

// 	ctx.JSON(http.StatusOK, res.Citizen)
// }

// // SubmitServiceRequest godoc
// // @Summary     Submit Service Request
// // @Description Allows a citizen to submit a service request.
// // @Tags        Citizen
// // @Accept      json
// // @Produce     json
// // @Param       request body     pbcitizen.ServiceRequestSubmission true "Service request details"
// // @Success     201     {object} pbcitizen.ServiceRequest
// // @Failure     400     {object} map[string]interface{}
// // @Failure     500     {object} map[string]interface{}
// // @Router      /v1/citizen/service-requests [post]
// func (c *CitizenHandler) SubmitServiceRequest(ctx *gin.Context) {
// 	var req pbcitizen.ServiceRequestSubmission
// 	if err := ctx.ShouldBindJSON(&req); err != nil {
// 		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
// 		return
// 	}

// 	res, err := c.Citizen.SubmitServiceRequest(context.Background(), &req)
// 	if err != nil {
// 		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to submit service request: " + err.Error()})
// 		return
// 	}

// 	ctx.JSON(http.StatusCreated, res.ServiceRequest)
// }

// // ListServiceRequests godoc
// // @Summary     List Service Requests
// // @Description Retrieves a list of service requests for a citizen.
// // @Tags        Citizen
// // @Accept      json
// // @Produce     json
// // @Param       citizen_id path     string true "Citizen ID"
// // @Success     200     {array}  pbcitizen.ServiceRequest
// // @Failure     400     {object} map[string]interface{}
// // @Failure     500     {object} map[string]interface{}
// // @Router      /v1/citizen/{citizen_id}/service-requests [get]
// func (c *CitizenHandler) ListServiceRequests(ctx *gin.Context) {
// 	citizenID := ctx.Param("citizen_id")

// 	res, err := c.Citizen.ListServiceRequests(context.Background(), &pbcitizen.ListServiceRequestsRequest{CitizenId: citizenID})
// 	if err != nil {
// 		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to fetch service requests: " + err.Error()})
// 		return
// 	}

// 	ctx.JSON(http.StatusOK, res.ServiceRequests)
// }

// // RetrieveDocument godoc
// // @Summary     Retrieve Document
// // @Description Allows a citizen to retrieve a previously uploaded document.
// // @Tags        Citizen
// // @Accept      json
// // @Produce     json
// // @Param       citizen_id path     string true "Citizen ID"
// // @Param       document_id path     string true "Document ID"
// // @Success     200     {object} pbcitizen.Document
// // @Failure     400     {object} map[string]interface{}
// // @Failure     404     {object} map[string]interface{}
// // @Failure     500     {object} map[string]interface{}
// // @Router      /v1/citizen/{citizen_id}/documents/{document_id} [get]
// func (c *CitizenHandler) RetrieveDocument(ctx *gin.Context) {
// 	citizenID := ctx.Param("citizen_id")
// 	documentID := ctx.Param("document_id")

// 	// ... (Implement document retrieval logic based on citizenID and documentID) ...

// 	// Example response for a retrieved document
// 	ctx.JSON(http.StatusOK, gin.H{
// 		"document_id":   documentID,
// 		"citizen_id":    citizenID,
// 		"document_type": "example_type",
// 		"file_path":     "/path/to/document",
// 		"uploaded_at":   "2023-12-19T10:00:00Z",
// 	})
// }

// // SetNotificationPreferences godoc
// // @Summary     Set Notification Preferences
// // @Description Allows a citizen to manage their notification preferences.
// // @Tags        Citizen
// // @Accept      json
// // @Produce     json
// // @Param       citizen_id path     string                                  true "Citizen ID"
// // @Param       preferences body     pbcitizen.SetNotificationPreferencesRequest true "Notification preferences"
// // @Success     200     {object} pbcitizen.NotificationPreference
// // @Failure     400     {object} map[string]interface{}
// // @Failure     500     {object} map[string]interface{}
// // @Router      /v1/citizen/{citizen_id}/notifications/preferences [put]
// func (c *CitizenHandler) SetNotificationPreferences(ctx *gin.Context) {
// 	citizenID := ctx.Param("citizen_id")
// 	var req pbcitizen.SetNotificationPreferencesRequest
// 	if err := ctx.ShouldBindJSON(&req); err != nil {
// 		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
// 		return
// 	}
// 	req.CitizenId = citizenID

// 	res, err := c.Citizen.SetNotificationPreferences(context.Background(), &req)
// 	if err != nil {
// 		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to set notification preferences: " + err.Error()})
// 		return
// 	}

// 	ctx.JSON(http.StatusOK, res.NotificationPreference)
// }

// // GetServiceUsage godoc
// // @Summary     Get Service Usage
// // @Description Provides statistics about a citizen's service usage.
// // @Tags        Citizen
// // @Accept      json
// // @Produce     json
// // @Param       citizen_id path     string true "Citizen ID"
// // @Success     200     {object} pbcitizen.ServiceUsageResponse
// // @Failure     400     {object} map[string]interface{}
// // @Failure     500     {object} map[string]interface{}
// // @Router      /v1/citizen/{citizen_id}/service-usage [get]
// func (c *CitizenHandler) GetServiceUsage(ctx *gin.Context) {
// 	citizenID := ctx.Param("citizen_id")

// 	res, err := c.Citizen.GetServiceUsage(context.Background(), &pbcitizen.GetServiceUsageRequest{CitizenId: citizenID})
// 	if err != nil {
// 		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get service usage: " + err.Error()})
// 		return
// 	}

// 	ctx.JSON(http.StatusOK, res)
// }
