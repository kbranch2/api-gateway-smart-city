package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/smart-city/api-gateway-smart-city/genproto/energy_management"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type EnergyManagementHandler struct {
	EnergyManagementBuilding     energy_management.BuildingServiceClient
	EnergyManagementMeter        energy_management.EnenrgyMeterServiceClient
	EnergyManageent              energy_management.EnergyManagementServiceClient
	EnergyManagementMeterReading energy_management.MeterReadingServiceClient
}

func NewEnergyHandler(conn *grpc.ClientConn) *EnergyManagementHandler {
	return &EnergyManagementHandler{
		EnergyManagementBuilding:     energy_management.NewBuildingServiceClient(conn),
		EnergyManagementMeter:        energy_management.NewEnenrgyMeterServiceClient(conn),
		EnergyManageent:              energy_management.NewEnergyManagementServiceClient(conn),
		EnergyManagementMeterReading: energy_management.NewMeterReadingServiceClient(conn),
	}
}

// ------------------ Building Handlers ------------------

// CreateBuilding godoc
// @Summary     Create Building
// @Description Creates a new building record.
// @Tags        Energy Management, Buildings
// @Accept      json
// @Produce     json
// @Param       building body     energy_management.CreateBuildingRequest true "Building details"
// @Success     201     {object} energy_management.CreateBuildingResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/buildings [post]
func (h *EnergyManagementHandler) CreateBuilding(ctx *gin.Context) {
	var req energy_management.CreateBuildingRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.EnergyManagementBuilding.CreateBuilding(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "A building with this ID already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create building: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, res)
}

// GetBuilding godoc
// @Summary     Get Building
// @Description Retrieves a building by its ID.
// @Tags        Energy Management, Buildings
// @Accept      json
// @Produce     json
// @Param       building_id path     string true "Building ID"
// @Success     200     {object} energy_management.GetBuildingResponse
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/buildings/{building_id} [get]
func (h *EnergyManagementHandler) GetBuilding(ctx *gin.Context) {
	buildingID := ctx.Param("building_id")

	res, err := h.EnergyManagementBuilding.GetBuilding(context.Background(), &energy_management.GetBuildingRequest{Id: &energy_management.BuildingById{Id: buildingID}})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Building not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get building: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// UpdateBuilding godoc
// @Summary     Update Building
// @Description Updates an existing building record.
// @Tags        Energy Management, Buildings
// @Accept      json
// @Produce     json
// @Param       building_id path     string true "Building ID"
// @Param       building body     energy_management.UpdateBuildingRequest true "Updated building details"
// @Success     200     {object} energy_management.UpdateBuildingResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/buildings/{building_id} [put]
func (h *EnergyManagementHandler) UpdateBuilding(ctx *gin.Context) {
	buildingID := ctx.Param("building_id")
	var req energy_management.UpdateBuildingRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	req.BuildingId = buildingID // Ensure the ID is set in the request

	res, err := h.EnergyManagementBuilding.UpdateBuilding(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Building not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update building: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// DeleteBuilding godoc
// @Summary     Delete Building
// @Description Deletes a building record by its ID.
// @Tags        Energy Management, Buildings
// @Accept      json
// @Produce     json
// @Param       building_id path     string true "Building ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/buildings/{building_id} [delete]
func (h *EnergyManagementHandler) DeleteBuilding(ctx *gin.Context) {
	buildingID := ctx.Param("building_id")

	_, err := h.EnergyManagementBuilding.DeleteBuilding(context.Background(), &energy_management.DeleteBuildingRequest{Id: &energy_management.BuildingById{Id: buildingID}})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Building not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete building: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Building deleted successfully"})
}

// GetAllBuildings godoc
// @Summary     Get All Buildings
// @Description Retrieves a list of all building records.
// @Tags        Energy Management, Buildings
// @Produce     json
// @Success     200     {array}  energy_management.Building
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/buildings [get]
func (h *EnergyManagementHandler) GetAllBuildings(ctx *gin.Context) {
	res, err := h.EnergyManagementBuilding.GetAllBuildings(context.Background(), &energy_management.GetAllBuildingsRequest{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get buildings: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res.Res)
}

// ------------------ Energy Meter Handlers ------------------

// CreateEnergyMeter godoc
// @Summary     Create Energy Meter
// @Description Creates a new energy meter record.
// @Tags        Energy Management, Meters
// @Accept      json
// @Produce     json
// @Param       meter body     energy_management.CreateEnergyMeterRequest true "Energy meter details"
// @Success     201     {object} energy_management.CreateEnergyMeterResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/meters [post]
func (h *EnergyManagementHandler) CreateEnergyMeter(ctx *gin.Context) {
	var req energy_management.CreateEnergyMeterRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.EnergyManagementMeter.CreateEnergyMeter(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "An energy meter with this ID already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create energy meter: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, res)
}

// GetEnergyMeter godoc
// @Summary     Get Energy Meter
// @Description Retrieves an energy meter by its ID.
// @Tags        Energy Management, Meters
// @Accept      json
// @Produce     json
// @Param       meter_id path     string true "Energy meter ID"
// @Success     200     {object} energy_management.GetEnergyMeterResponse
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/meters/{meter_id} [get]
func (h *EnergyManagementHandler) GetEnergyMeter(ctx *gin.Context) {
	meterID := ctx.Param("meter_id")

	res, err := h.EnergyManagementMeter.GetEnergyMeter(context.Background(), &energy_management.GetEnergyMeterRequest{Id: &energy_management.EnenrgyMeterById{Id: meterID}})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Energy meter not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get energy meter: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// UpdateEnergyMeter godoc
// @Summary     Update Energy Meter
// @Description Updates an existing energy meter record.
// @Tags        Energy Management, Meters
// @Accept      json
// @Produce     json
// @Param       meter_id path     string true "Energy meter ID"
// @Param       meter body     energy_management.UpdateEnergyMeterRequest true "Updated energy meter details"
// @Success     200     {object} energy_management.UpdateEnergyMeterResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/meters/{meter_id} [put]
func (h *EnergyManagementHandler) UpdateEnergyMeter(ctx *gin.Context) {
	meterID := ctx.Param("meter_id")
	var req energy_management.UpdateEnergyMeterRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	req.MeterId = meterID // Ensure the ID is set in the request

	res, err := h.EnergyManagementMeter.UpdateEnergyMeter(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Energy meter not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update energy meter: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// DeleteEnergyMeter godoc
// @Summary     Delete Energy Meter
// @Description Deletes an energy meter record by its ID.
// @Tags        Energy Management, Meters
// @Accept      json
// @Produce     json
// @Param       meter_id path     string true "Energy meter ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/meters/{meter_id} [delete]
func (h *EnergyManagementHandler) DeleteEnergyMeter(ctx *gin.Context) {
	meterID := ctx.Param("meter_id")

	_, err := h.EnergyManagementMeter.DeleteEnergyMeter(context.Background(), &energy_management.DeleteEnergyMeterRequest{Id: &energy_management.EnenrgyMeterById{Id: meterID}})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Energy meter not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete energy meter: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Energy meter deleted successfully"})
}

// GetAllEnergyMeters godoc
// @Summary     Get All Energy Meters
// @Description Retrieves a list of all energy meter records.
// @Tags        Energy Management, Meters
// @Produce     json
// @Success     200     {array}  energy_management.GetEnergyMeter
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/meters [get]
func (h *EnergyManagementHandler) GetAllEnergyMeters(ctx *gin.Context) {
	res, err := h.EnergyManagementMeter.GetAllEnergyMeters(context.Background(), &energy_management.GetAllEnergyMetersRequest{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get energy meters: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res.Res)
}

// ------------------ Meter Reading Handlers ------------------

// CreateMeterReading godoc
// @Summary     Create Meter Reading
// @Description Creates a new meter reading record.
// @Tags        Energy Management, Meter Readings
// @Accept      json
// @Produce     json
// @Param       reading body     energy_management.CreateMeterReadingRequest true "Meter reading details"
// @Success     201     {object} energy_management.CreateMeterReadingResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/readings [post]
func (h *EnergyManagementHandler) CreateMeterReading(ctx *gin.Context) {
	var req energy_management.CreateMeterReadingRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.EnergyManagementMeterReading.CreateMeterReading(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.AlreadyExists {
			ctx.JSON(http.StatusConflict, gin.H{"error": "A meter reading with this ID already exists"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to create meter reading: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusCreated, res)
}

// GetMeterReading godoc
// @Summary     Get Meter Reading
// @Description Retrieves a meter reading by its ID.
// @Tags        Energy Management, Meter Readings
// @Accept      json
// @Produce     json
// @Param       reading_id path     string true "Meter reading ID"
// @Success     200     {object} energy_management.GetMeterReadingResponse
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/readings/{reading_id} [get]
func (h *EnergyManagementHandler) GetMeterReading(ctx *gin.Context) {
	readingID := ctx.Param("reading_id")

	res, err := h.EnergyManagementMeterReading.GetMeterReading(context.Background(), &energy_management.GetMeterReadingRequest{Id: &energy_management.MeterReadingById{Id: readingID}})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Meter reading not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get meter reading: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// UpdateMeterReading godoc
// @Summary     Update Meter Reading
// @Description Updates an existing meter reading record.
// @Tags        Energy Management, Meter Readings
// @Accept      json
// @Produce     json
// @Param       reading_id path     string true "Meter reading ID"
// @Param       reading body     energy_management.UpdateMeterReadingRequest true "Updated meter reading details"
// @Success     200     {object} energy_management.UpdateMeterReadingResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/readings/{reading_id} [put]
func (h *EnergyManagementHandler) UpdateMeterReading(ctx *gin.Context) {
	readingID := ctx.Param("reading_id")
	var req energy_management.UpdateMeterReadingRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	req.ReadingId = readingID // Ensure the ID is set in the request

	res, err := h.EnergyManagementMeterReading.UpdateMeterReading(context.Background(), &req)
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Meter reading not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update meter reading: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// DeleteMeterReading godoc
// @Summary     Delete Meter Reading
// @Description Deletes a meter reading record by its ID.
// @Tags        Energy Management, Meter Readings
// @Accept      json
// @Produce     json
// @Param       reading_id path     string true "Meter reading ID"
// @Success     204     {object} map[string]interface{}
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/readings/{reading_id} [delete]
func (h *EnergyManagementHandler) DeleteMeterReading(ctx *gin.Context) {
	readingID := ctx.Param("reading_id")

	_, err := h.EnergyManagementMeterReading.DeleteMeterReading(context.Background(), &energy_management.DeleteMeterReadingRequest{Id: &energy_management.MeterReadingById{Id: readingID}})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Meter reading not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete meter reading: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusNoContent, gin.H{"message": "Meter reading deleted successfully"})
}

// GetAllMeterReadings godoc
// @Summary     Get All Meter Readings
// @Description Retrieves a list of all meter reading records.
// @Tags        Energy Management, Meter Readings
// @Produce     json
// @Success     200     {array}  energy_management.GetMeterReading
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/readings [get]
func (h *EnergyManagementHandler) GetAllMeterReadings(ctx *gin.Context) {
	res, err := h.EnergyManagementMeterReading.GetAllMeterReadings(context.Background(), &energy_management.GetAllMeterReadingsRequest{})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get meter readings: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res.Res)
}

// ------------------ Energy Management Service Handlers ------------------

// GetCityDailyConsumption godoc
// @Summary     Get City Daily Consumption
// @Description Retrieves the daily energy consumption for the entire city.
// @Tags        Energy Management, Consumption
// @Accept      json
// @Produce     json
// @Param       date query     string true "Date (YYYY-MM-DD)"
// @Success     200     {object} energy_management.GetCityDailyConsumptionResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/consumption/city/daily [get]
func (h *EnergyManagementHandler) GetCityDailyConsumption(ctx *gin.Context) {
	date := ctx.Query("date")
	if date == "" {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Missing 'date' query parameter"})
		return
	}

	res, err := h.EnergyManageent.GetCityDailyConsumption(context.Background(), &energy_management.GetCityDailyConsumptionRequest{Date: date})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get city daily consumption: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// GetBuildingHourlyConsumption godoc
// @Summary     Get Building Hourly Consumption
// @Description Retrieves the hourly energy consumption for a specific building.
// @Tags        Energy Management, Consumption
// @Accept      json
// @Produce     json
// @Param       building_id path     string true "Building ID"
// @Param       date        query     string true "Date (YYYY-MM-DD)"
// @Success     200     {object} energy_management.GetBuildingHourlyConsumptionResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/consumption/building/{building_id}/hourly [get]
func (h *EnergyManagementHandler) GetBuildingHourlyConsumption(ctx *gin.Context) {
	buildingID := ctx.Param("building_id")
	date := ctx.Query("date")
	if date == "" {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Missing 'date' query parameter"})
		return
	}

	res, err := h.EnergyManageent.GetBuildingHourlyConsumption(context.Background(), &energy_management.GetBuildingHourlyConsumptionRequest{BuildingId: buildingID, Date: date})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get building hourly consumption: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// SubmitMeterReading godoc
// @Summary     Submit Meter Reading
// @Description Submits a new meter reading.
// @Tags        Energy Management, Meter Readings
// @Accept      json
// @Produce     json
// @Param       reading body     energy_management.SubmitMeterReadingRequest true "Meter reading details"
// @Success     200     {object} energy_management.SubmitMeterReadingResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/readings/submit [post]
func (h *EnergyManagementHandler) SubmitMeterReading(ctx *gin.Context) {
	var req energy_management.SubmitMeterReadingRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.EnergyManageent.SubmitMeterReading(context.Background(), &req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to submit meter reading: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// GetGridSectorStatus godoc
// @Summary     Get Grid Sector Status
// @Description Retrieves the status of a specific grid sector.
// @Tags        Energy Management, Grid
// @Accept      json
// @Produce     json
// @Param       sector_id path     string true "Grid sector ID"
// @Success     200     {object} energy_management.GetGridSectorStatusResponse
// @Failure     404     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/grid/{sector_id}/status [get]
func (h *EnergyManagementHandler) GetGridSectorStatus(ctx *gin.Context) {
	sectorID := ctx.Param("sector_id")

	res, err := h.EnergyManageent.GetGridSectorStatus(context.Background(), &energy_management.GetGridSectorStatusRequest{SectorId: sectorID})
	if err != nil {
		if st, ok := status.FromError(err); ok && st.Code() == codes.NotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"error": "Grid sector not found"})
			return
		}
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get grid sector status: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// ReportPowerOutage godoc
// @Summary     Report Power Outage
// @Description Reports a power outage in a grid sector.
// @Tags        Energy Management, Grid, Outages
// @Accept      json
// @Produce     json
// @Param       outage body     energy_management.ReportPowerOutageRequest true "Power outage details"
// @Success     200     {object} energy_management.ReportPowerOutageResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/grid/outages [post]
func (h *EnergyManagementHandler) ReportPowerOutage(ctx *gin.Context) {
	var req energy_management.ReportPowerOutageRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.EnergyManageent.ReportPowerOutage(context.Background(), &req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to report power outage: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// GetEnergySavingTips godoc
// @Summary     Get Energy Saving Tips
// @Description Retrieves energy saving tips for a citizen.
// @Tags        Energy Management, Tips
// @Accept      json
// @Produce     json
// @Param       citizen_id path     string true "Citizen ID"
// @Success     200     {object} energy_management.GetEnergySavingTipsResponse
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/tips/{citizen_id} [get]
func (h *EnergyManagementHandler) GetEnergySavingTips(ctx *gin.Context) {
	citizenID := ctx.Param("citizen_id")

	res, err := h.EnergyManageent.GetEnergySavingTips(context.Background(), &energy_management.GetEnergySavingTipsRequest{CitizenId: citizenID})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get energy saving tips: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// EnrollInDemandResponseProgram godoc
// @Summary     Enroll in Demand Response Program
// @Description Enrolls a citizen in a demand response program.
// @Tags        Energy Management, Demand Response
// @Accept      json
// @Produce     json
// @Param       enrollment body     energy_management.EnrollInDemandResponseProgramRequest true "Enrollment details"
// @Success     200     {object} energy_management.EnrollInDemandResponseProgramResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/demand-response/enroll [post]
func (h *EnergyManagementHandler) EnrollInDemandResponseProgram(ctx *gin.Context) {
	var req energy_management.EnrollInDemandResponseProgramRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.EnergyManageent.EnrollInDemandResponseProgram(context.Background(), &req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to enroll in demand response program: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// GetUpcomingDemandResponseEvents godoc
// @Summary     Get Upcoming Demand Response Events
// @Description Retrieves upcoming demand response events for a citizen.
// @Tags        Energy Management, Demand Response
// @Accept      json
// @Produce     json
// @Param       citizen_id path     string true "Citizen ID"
// @Success     200     {object} energy_management.GetUpcomingDemandResponseEventsResponse
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/demand-response/events/{citizen_id} [get]
func (h *EnergyManagementHandler) GetUpcomingDemandResponseEvents(ctx *gin.Context) {
	citizenID := ctx.Param("citizen_id")

	res, err := h.EnergyManageent.GetUpcomingDemandResponseEvents(context.Background(), &energy_management.GetUpcomingDemandResponseEventsRequest{CitizenId: citizenID})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get upcoming demand response events: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}

// RegisterRenewableEnergySource godoc
// @Summary     Register Renewable Energy Source
// @Description Registers a new renewable energy source for a building.
// @Tags        Energy Management, Renewable Energy
// @Accept      json
// @Produce     json
// @Param       source body     energy_management.RegisterRenewableEnergySourceRequest true "Renewable energy source details"
// @Success     200     {object} energy_management.RegisterRenewableEnergySourceResponse
// @Failure     400     {object} map[string]interface{}
// @Failure     500     {object} map[string]interface{}
// @Router      /v1/energy/renewable-sources [post]
func (h *EnergyManagementHandler) RegisterRenewableEnergySource(ctx *gin.Context) {
	var req energy_management.RegisterRenewableEnergySourceRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	res, err := h.EnergyManageent.RegisterRenewableEnergySource(context.Background(), &req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to register renewable energy source: " + err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, res)
}
