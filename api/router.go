package api

import (
	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"

	swaggerFiles "github.com/swaggo/files"

	ginSwagger "github.com/swaggo/gin-swagger"

	_ "github.com/smart-city/api-gateway-smart-city/api/docs"
	"github.com/smart-city/api-gateway-smart-city/api/handlers"
	"github.com/smart-city/api-gateway-smart-city/api/middleware"

	"github.com/smart-city/api-gateway-smart-city/config/logger"
)

// @securityDefinitions.apikey BearerAuth
// @in header
// @name Authorization
func NewRouter(
	ConnCitizen *grpc.ClientConn,
	ConnCity *grpc.ClientConn,
	ConnEmergence *grpc.ClientConn,
	ConnEnergyManagement *grpc.ClientConn,
	ConnEnvironmentalService *grpc.ClientConn,
	ConnTransportationService *grpc.ClientConn,
	logger logger.Logger,
	enforcer *casbin.Enforcer,
) *gin.Engine {
	r := gin.Default()
	handler := handlers.NewHandler(ConnCitizen, ConnCity, ConnEmergence, ConnEnergyManagement, ConnEnvironmentalService, ConnTransportationService, logger)

	r.GET("/api/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	protected := r.Group("/")
	protected.Use(middleware.JWTMiddleware())
	protected.Use(middleware.CasbinMiddleware(enforcer))
	v1 := r.Group("/v1")
	emergency := v1.Group("/emergency")
	{
		emergency.POST("/create-incident", handler.Emergency.CreateIncident)
		emergency.GET("/get-incident/:incident_id", handler.Emergency.GetIncident)
		emergency.GET("/list-incidents", handler.Emergency.ListActiveIncidents)
		emergency.PUT("/update-incident/:incident_id", handler.Emergency.UpdateIncidentStatus)
		emergency.DELETE("/delete-incident/:incident_id", handler.Emergency.DeleteIncident)
		// Resource management
		emergency.POST("/create-resource", handler.Emergency.CreateResource)
		emergency.GET("/get-resource/:resource_id", handler.Emergency.GetResource)
		emergency.PUT("/update-resource/:resource_id", handler.Emergency.UpdateResource)
		emergency.DELETE("/delete-resource/:resource_id", handler.Emergency.DeleteResource)
		emergency.PUT("/update-resource-status/:resource_id", handler.Emergency.UpdateResourceStatus)
		// Dispatch management
		emergency.POST("/dispatch-resource", handler.Emergency.DispatchResource)
		emergency.GET("/get-dispatch/:dispatch_id", handler.Emergency.GetDispatch)
		emergency.PUT("/update-dispatch/:dispatch_id", handler.Emergency.UpdateDispatch)
		emergency.DELETE("/delete-dispatch/:dispatch_id", handler.Emergency.DeleteDispatch)
		emergency.PUT("/arrive-resource/:dispatch_id", handler.Emergency.ArriveResource)
		// Other
		emergency.GET("/list-resources", handler.Emergency.ListAvailableResources)
		// Alert management
		emergency.POST("/create-alert", handler.Emergency.CreateAlert)
	}

	// City service
	city := v1.Group("/city")
	{
		// Building Permits
		city.POST("/building-permits", handler.City.CreateBuildingPermit)
		city.GET("/building-permits/:permit_id", handler.City.GetBuildingPermit)
		city.PUT("/building-permits/:permit_id", handler.City.UpdateBuildingPermit)
		city.DELETE("/building-permits/:permit_id", handler.City.DeleteBuildingPermit)
		city.GET("/building-permits", handler.City.GetAllBuildingPermits)

		// City Planning Proposals
		city.POST("/planning-proposals", handler.City.CreateCityPlanningProposal)
		city.GET("/planning-proposals/:proposal_id", handler.City.GetCityPlanningProposal)
		city.PUT("/planning-proposals/:proposal_id", handler.City.UpdateCityPlanningProposal)
		city.DELETE("/planning-proposals/:proposal_id", handler.City.DeleteCityPlanningProposal)
		city.GET("/planning-proposals", handler.City.GetAllCityPlanningProposals)

		// City Zones
		city.POST("/zones", handler.City.CreateCityZone)
		city.GET("/zones/:zone_id", handler.City.GetCityZone)
		city.PUT("/zones/:zone_id", handler.City.UpdateCityZone)
		city.DELETE("/zones/:zone_id", handler.City.DeleteCityZone)
		city.GET("/zones", handler.City.GetAllCityZones)

		// Demographic Data
		city.POST("/demographic-data", handler.City.CreateDemographicData)
		city.GET("/demographic-data/:data_id", handler.City.GetDemographicData)
		city.PUT("/demographic-data/:data_id", handler.City.UpdateDemographicData)
		city.DELETE("/demographic-data/:data_id", handler.City.DeleteDemographicData)
		city.GET("/demographic-data", handler.City.GetAllDemographicData)

		// Infrastructure Assets
		city.POST("/infrastructure-assets", handler.City.CreateInfrastructureAsset)
		city.GET("/infrastructure-assets/:asset_id", handler.City.GetInfrastructureAsset)
		city.PUT("/infrastructure-assets/:asset_id", handler.City.UpdateInfrastructureAsset)
		city.DELETE("/infrastructure-assets/:asset_id", handler.City.DeleteInfrastructureAsset)
		city.GET("/infrastructure-assets", handler.City.GetAllInfrastructureAssets)

		// Planning Proposals (for zones)
		city.POST("/planning-proposals/zone", handler.City.CreatePlanningProposal)
		city.GET("/planning-proposals/zone/:proposal_id", handler.City.GetPlanningProposal)
		city.PUT("/planning-proposals/zone/:proposal_id", handler.City.UpdatePlanningProposal)
		city.DELETE("/planning-proposals/zone/:proposal_id", handler.City.DeletePlanningProposal)
		city.GET("/planning-proposals/zone", handler.City.GetAllPlanningProposals)

		// Proposal Feedback
		city.POST("/planning-proposals/feedback", handler.City.CreateProposalFeedback)
		city.GET("/planning-proposals/feedback/:feedback_id", handler.City.GetProposalFeedback)
		city.PUT("/planning-proposals/feedback/:feedback_id", handler.City.UpdateProposalFeedback)
		city.DELETE("/planning-proposals/feedback/:feedback_id", handler.City.DeleteProposalFeedback)
		city.GET("/planning-proposals/feedback", handler.City.GetAllProposalFeedback)
	}

	// Energy Management service
	energy := v1.Group("/energy")
	{
		// Buildings
		energy.POST("/buildings", handler.EnergyManagement.CreateBuilding)
		energy.GET("/buildings/:building_id", handler.EnergyManagement.GetBuilding)
		energy.PUT("/buildings/:building_id", handler.EnergyManagement.UpdateBuilding)
		energy.DELETE("/buildings/:building_id", handler.EnergyManagement.DeleteBuilding)
		energy.GET("/buildings", handler.EnergyManagement.GetAllBuildings)

		// Meters
		energy.POST("/meters", handler.EnergyManagement.CreateEnergyMeter)
		energy.GET("/meters/:meter_id", handler.EnergyManagement.GetEnergyMeter)
		energy.PUT("/meters/:meter_id", handler.EnergyManagement.UpdateEnergyMeter)
		energy.DELETE("/meters/:meter_id", handler.EnergyManagement.DeleteEnergyMeter)
		energy.GET("/meters", handler.EnergyManagement.GetAllEnergyMeters)

		// Meter Readings
		energy.POST("/readings", handler.EnergyManagement.CreateMeterReading)
		energy.GET("/readings/:reading_id", handler.EnergyManagement.GetMeterReading)
		energy.PUT("/readings/:reading_id", handler.EnergyManagement.UpdateMeterReading)
		energy.DELETE("/readings/:reading_id", handler.EnergyManagement.DeleteMeterReading)
		energy.GET("/readings", handler.EnergyManagement.GetAllMeterReadings)

		// Consumption
		energy.GET("/consumption/city/daily", handler.EnergyManagement.GetCityDailyConsumption)
		energy.GET("/consumption/building/:building_id/hourly", handler.EnergyManagement.GetBuildingHourlyConsumption)

		// Other Energy Management endpoints
		energy.POST("/readings/submit", handler.EnergyManagement.SubmitMeterReading)
		energy.GET("/grid/:sector_id/status", handler.EnergyManagement.GetGridSectorStatus)
		energy.POST("/grid/outages", handler.EnergyManagement.ReportPowerOutage)
		energy.GET("/tips/:citizen_id", handler.EnergyManagement.GetEnergySavingTips)
		energy.POST("/demand-response/enroll", handler.EnergyManagement.EnrollInDemandResponseProgram)
		energy.GET("/demand-response/events/:citizen_id", handler.EnergyManagement.GetUpcomingDemandResponseEvents)
		energy.POST("/renewable-sources", handler.EnergyManagement.RegisterRenewableEnergySource)
	}

	// Transportation service
	transport := v1.Group("/transport")
	{
		// Incidents
		transport.POST("/incidents", handler.Transportation.CreateIncident)
		transport.GET("/incidents/:incident_id", handler.Transportation.GetIncident)
		transport.PUT("/incidents/:incident_id", handler.Transportation.UpdateIncident)
		transport.DELETE("/incidents/:incident_id", handler.Transportation.DeleteIncident)
		transport.GET("/incidents", handler.Transportation.GetAllIncidents)

		// Parking Lots
		transport.POST("/parking_lots", handler.Transportation.CreateParkingLot)
		transport.GET("/parking_lots/:lot_id", handler.Transportation.GetParkingLot)
		transport.PUT("/parking_lots/:lot_id", handler.Transportation.UpdateParkingLot)
		transport.DELETE("/parking_lots/:lot_id", handler.Transportation.DeleteParkingLot)
		transport.GET("/parking_lots", handler.Transportation.GetAllParkingLots)

		// Roads
		transport.POST("/roads", handler.Transportation.CreateRoad)
		transport.GET("/roads/:road_id", handler.Transportation.GetRoad)
		transport.PUT("/roads/:road_id", handler.Transportation.UpdateRoad)
		transport.DELETE("/roads/:road_id", handler.Transportation.DeleteRoad)
		transport.GET("/roads", handler.Transportation.GetAllRoads)

		// Route Schedules
		transport.POST("/route_schedules", handler.Transportation.CreateRouteSchedule)
		transport.GET("/route_schedules/:schedule_id", handler.Transportation.GetRouteSchedule)
		transport.PUT("/route_schedules/:schedule_id", handler.Transportation.UpdateRouteSchedule)
		transport.DELETE("/route_schedules/:schedule_id", handler.Transportation.DeleteRouteSchedule)
		transport.GET("/route_schedules", handler.Transportation.GetAllRouteSchedules)

		// Traffic Conditions
		transport.POST("/traffic_conditions", handler.Transportation.CreateTrafficCondition)
		transport.GET("/traffic_conditions/:condition_id", handler.Transportation.GetTrafficCondition)
		transport.PUT("/traffic_conditions/:condition_id", handler.Transportation.UpdateTrafficCondition)
		transport.DELETE("/traffic_conditions/:condition_id", handler.Transportation.DeleteTrafficCondition)
		transport.GET("/traffic_conditions", handler.Transportation.GetAllTrafficConditions)

		// Vehicles
		transport.POST("/vehicles", handler.Transportation.CreateVehicle)
		transport.GET("/vehicles/:vehicle_id", handler.Transportation.GetVehicle)
		transport.PUT("/vehicles/:vehicle_id", handler.Transportation.UpdateVehicle)
		transport.DELETE("/vehicles/:vehicle_id", handler.Transportation.DeleteVehicle)
		transport.GET("/vehicles", handler.Transportation.GetAllVehicles)
	}

	// Environmental service
	environmental := v1.Group("/environmental")
	{
		// Air Quality Readings
		environmental.POST("/air_quality_readings", handler.Environmental.CreateAirQualityReading)
		environmental.GET("/air_quality_readings/:reading_id", handler.Environmental.GetAirQualityReading)
		environmental.PUT("/air_quality_readings/:reading_id", handler.Environmental.UpdateAirQualityReading)
		environmental.DELETE("/air_quality_readings/:reading_id", handler.Environmental.DeleteAirQualityReading)
		environmental.GET("/air_quality_readings", handler.Environmental.GetAllAirQualityReadings)

		// Air Quality Stations
		environmental.POST("/air_quality_stations", handler.Environmental.CreateAirQualityStation)
		environmental.GET("/air_quality_stations/:station_id", handler.Environmental.GetAirQualityStation)
		environmental.PUT("/air_quality_stations/:station_id", handler.Environmental.UpdateAirQualityStation)
		environmental.DELETE("/air_quality_stations/:station_id", handler.Environmental.DeleteAirQualityStation)
		environmental.GET("/air_quality_stations", handler.Environmental.GetAllAirQualityStations)

		// Green Spaces
		environmental.POST("/green_spaces", handler.Environmental.CreateGreenSpace)
		environmental.GET("/green_spaces/:space_id", handler.Environmental.GetGreenSpace)
		environmental.PUT("/green_spaces/:space_id", handler.Environmental.UpdateGreenSpace)
		environmental.DELETE("/green_spaces/:space_id", handler.Environmental.DeleteGreenSpace)
		environmental.GET("/green_spaces", handler.Environmental.GetAllGreenSpaces)

		// Noise Level Readings
		environmental.POST("/noise_level_readings", handler.Environmental.CreateNoiseLevelReading)
		environmental.GET("/noise_level_readings/:reading_id", handler.Environmental.GetNoiseLevelReading)
		environmental.PUT("/noise_level_readings/:reading_id", handler.Environmental.UpdateNoiseLevelReading)
		environmental.DELETE("/noise_level_readings/:reading_id", handler.Environmental.DeleteNoiseLevelReading)
		environmental.GET("/noise_level_readings", handler.Environmental.GetAllNoiseLevelReadings)

		// Noise Monitoring Zones
		environmental.POST("/noise_monitoring_zones", handler.Environmental.CreateNoiseMonitoringZone)
		environmental.GET("/noise_monitoring_zones/:zone_id", handler.Environmental.GetNoiseMonitoringZone)
		environmental.PUT("/noise_monitoring_zones/:zone_id", handler.Environmental.UpdateNoiseMonitoringZone)
		environmental.DELETE("/noise_monitoring_zones/:zone_id", handler.Environmental.DeleteNoiseMonitoringZone)
		environmental.GET("/noise_monitoring_zones", handler.Environmental.GetAllNoiseMonitoringZones)

		// Plant Registry
		environmental.POST("/plant_registry", handler.Environmental.CreatePlantRegistry)
		environmental.GET("/plant_registry/:registry_id", handler.Environmental.GetPlantRegistry)
		environmental.PUT("/plant_registry/:registry_id", handler.Environmental.UpdatePlantRegistry)
		environmental.DELETE("/plant_registry/:registry_id", handler.Environmental.DeletePlantRegistry)
		environmental.GET("/plant_registry", handler.Environmental.GetAllPlantRegistries)

		// Recycling Centers
		environmental.POST("/recycling_centers", handler.Environmental.CreateRecyclingCenter)
		environmental.GET("/recycling_centers/:center_id", handler.Environmental.GetRecyclingCenter)
		environmental.PUT("/recycling_centers/:center_id", handler.Environmental.UpdateRecyclingCenter)
		environmental.DELETE("/recycling_centers/:center_id", handler.Environmental.DeleteRecyclingCenter)
		environmental.GET("/recycling_centers", handler.Environmental.GetAllRecyclingCenters)

		// Waste Collection Schedules
		environmental.POST("/waste_collection_schedules", handler.Environmental.CreateWasteCollectionSchedule)
		environmental.GET("/waste_collection_schedules/:schedule_id", handler.Environmental.GetWasteCollectionSchedule)
		environmental.PUT("/waste_collection_schedules/:schedule_id", handler.Environmental.UpdateWasteCollectionSchedule)
		environmental.DELETE("/waste_collection_schedules/:schedule_id", handler.Environmental.DeleteWasteCollectionSchedule)
		environmental.GET("/waste_collection_schedules", handler.Environmental.GetAllWasteCollectionSchedules)

		// Water Quality Reports
		environmental.POST("/water_quality_reports", handler.Environmental.CreateWaterQualityReport)
		environmental.GET("/water_quality_reports/:report_id", handler.Environmental.GetWaterQualityReport)
		environmental.PUT("/water_quality_reports/:report_id", handler.Environmental.UpdateWaterQualityReport)
		environmental.DELETE("/water_quality_reports/:report_id", handler.Environmental.DeleteWaterQualityReport)
		environmental.GET("/water_quality_reports", handler.Environmental.GetAllWaterQualityReports)

		// Water Treatment Plants
		environmental.POST("/water_treatment_plants", handler.Environmental.CreateWaterTreatmentPlant)
		environmental.GET("/water_treatment_plants/:plant_id", handler.Environmental.GetWaterTreatmentPlant)
		environmental.PUT("/water_treatment_plants/:plant_id", handler.Environmental.UpdateWaterTreatmentPlant)
		environmental.DELETE("/water_treatment_plants/:plant_id", handler.Environmental.DeleteWaterTreatmentPlant)
		environmental.GET("/water_treatment_plants", handler.Environmental.GetAllWaterTreatmentPlants)
	}

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	return r
}
