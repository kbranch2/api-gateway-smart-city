// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.2
// 	protoc        v5.27.1
// source: submodule-for-smart-city/city/city.proto

package city

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type CityPlanningProposal struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id            string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Title         string `protobuf:"bytes,2,opt,name=title,proto3" json:"title,omitempty"`
	Description   string `protobuf:"bytes,3,opt,name=description,proto3" json:"description,omitempty"`
	AffectedAreas string `protobuf:"bytes,4,opt,name=affected_areas,json=affectedAreas,proto3" json:"affected_areas,omitempty"`
	SubmittedBy   string `protobuf:"bytes,5,opt,name=submitted_by,json=submittedBy,proto3" json:"submitted_by,omitempty"`
	SubmittedAt   string `protobuf:"bytes,6,opt,name=submitted_at,json=submittedAt,proto3" json:"submitted_at,omitempty"`
}

func (x *CityPlanningProposal) Reset() {
	*x = CityPlanningProposal{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CityPlanningProposal) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CityPlanningProposal) ProtoMessage() {}

func (x *CityPlanningProposal) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CityPlanningProposal.ProtoReflect.Descriptor instead.
func (*CityPlanningProposal) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_city_proto_rawDescGZIP(), []int{0}
}

func (x *CityPlanningProposal) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *CityPlanningProposal) GetTitle() string {
	if x != nil {
		return x.Title
	}
	return ""
}

func (x *CityPlanningProposal) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *CityPlanningProposal) GetAffectedAreas() string {
	if x != nil {
		return x.AffectedAreas
	}
	return ""
}

func (x *CityPlanningProposal) GetSubmittedBy() string {
	if x != nil {
		return x.SubmittedBy
	}
	return ""
}

func (x *CityPlanningProposal) GetSubmittedAt() string {
	if x != nil {
		return x.SubmittedAt
	}
	return ""
}

type CreateCityPlanningProposalRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id            string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Title         string `protobuf:"bytes,2,opt,name=title,proto3" json:"title,omitempty"`
	Description   string `protobuf:"bytes,3,opt,name=description,proto3" json:"description,omitempty"`
	AffectedAreas string `protobuf:"bytes,4,opt,name=affected_areas,json=affectedAreas,proto3" json:"affected_areas,omitempty"`
	SubmittedBy   string `protobuf:"bytes,5,opt,name=submitted_by,json=submittedBy,proto3" json:"submitted_by,omitempty"`
	SubmittedAt   string `protobuf:"bytes,6,opt,name=submitted_at,json=submittedAt,proto3" json:"submitted_at,omitempty"`
}

func (x *CreateCityPlanningProposalRequest) Reset() {
	*x = CreateCityPlanningProposalRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateCityPlanningProposalRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateCityPlanningProposalRequest) ProtoMessage() {}

func (x *CreateCityPlanningProposalRequest) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateCityPlanningProposalRequest.ProtoReflect.Descriptor instead.
func (*CreateCityPlanningProposalRequest) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_city_proto_rawDescGZIP(), []int{1}
}

func (x *CreateCityPlanningProposalRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *CreateCityPlanningProposalRequest) GetTitle() string {
	if x != nil {
		return x.Title
	}
	return ""
}

func (x *CreateCityPlanningProposalRequest) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *CreateCityPlanningProposalRequest) GetAffectedAreas() string {
	if x != nil {
		return x.AffectedAreas
	}
	return ""
}

func (x *CreateCityPlanningProposalRequest) GetSubmittedBy() string {
	if x != nil {
		return x.SubmittedBy
	}
	return ""
}

func (x *CreateCityPlanningProposalRequest) GetSubmittedAt() string {
	if x != nil {
		return x.SubmittedAt
	}
	return ""
}

type GetCityPlanningProposalRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *GetCityPlanningProposalRequest) Reset() {
	*x = GetCityPlanningProposalRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetCityPlanningProposalRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetCityPlanningProposalRequest) ProtoMessage() {}

func (x *GetCityPlanningProposalRequest) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetCityPlanningProposalRequest.ProtoReflect.Descriptor instead.
func (*GetCityPlanningProposalRequest) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_city_proto_rawDescGZIP(), []int{2}
}

func (x *GetCityPlanningProposalRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type UpdateCityPlanningProposalRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Proposal *CityPlanningProposal `protobuf:"bytes,1,opt,name=proposal,proto3" json:"proposal,omitempty"`
}

func (x *UpdateCityPlanningProposalRequest) Reset() {
	*x = UpdateCityPlanningProposalRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateCityPlanningProposalRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateCityPlanningProposalRequest) ProtoMessage() {}

func (x *UpdateCityPlanningProposalRequest) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateCityPlanningProposalRequest.ProtoReflect.Descriptor instead.
func (*UpdateCityPlanningProposalRequest) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_city_proto_rawDescGZIP(), []int{3}
}

func (x *UpdateCityPlanningProposalRequest) GetProposal() *CityPlanningProposal {
	if x != nil {
		return x.Proposal
	}
	return nil
}

type DeleteCityPlanningProposalRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *DeleteCityPlanningProposalRequest) Reset() {
	*x = DeleteCityPlanningProposalRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeleteCityPlanningProposalRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeleteCityPlanningProposalRequest) ProtoMessage() {}

func (x *DeleteCityPlanningProposalRequest) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeleteCityPlanningProposalRequest.ProtoReflect.Descriptor instead.
func (*DeleteCityPlanningProposalRequest) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_city_proto_rawDescGZIP(), []int{4}
}

func (x *DeleteCityPlanningProposalRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type CreateCityPlanningProposalResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Proposal *CityPlanningProposal `protobuf:"bytes,1,opt,name=proposal,proto3" json:"proposal,omitempty"`
}

func (x *CreateCityPlanningProposalResponse) Reset() {
	*x = CreateCityPlanningProposalResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateCityPlanningProposalResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateCityPlanningProposalResponse) ProtoMessage() {}

func (x *CreateCityPlanningProposalResponse) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateCityPlanningProposalResponse.ProtoReflect.Descriptor instead.
func (*CreateCityPlanningProposalResponse) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_city_proto_rawDescGZIP(), []int{5}
}

func (x *CreateCityPlanningProposalResponse) GetProposal() *CityPlanningProposal {
	if x != nil {
		return x.Proposal
	}
	return nil
}

type GetCityPlanningProposalResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Proposal *CityPlanningProposal `protobuf:"bytes,1,opt,name=proposal,proto3" json:"proposal,omitempty"`
}

func (x *GetCityPlanningProposalResponse) Reset() {
	*x = GetCityPlanningProposalResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetCityPlanningProposalResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetCityPlanningProposalResponse) ProtoMessage() {}

func (x *GetCityPlanningProposalResponse) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetCityPlanningProposalResponse.ProtoReflect.Descriptor instead.
func (*GetCityPlanningProposalResponse) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_city_proto_rawDescGZIP(), []int{6}
}

func (x *GetCityPlanningProposalResponse) GetProposal() *CityPlanningProposal {
	if x != nil {
		return x.Proposal
	}
	return nil
}

type UpdateCityPlanningProposalResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Proposal *CityPlanningProposal `protobuf:"bytes,1,opt,name=proposal,proto3" json:"proposal,omitempty"`
}

func (x *UpdateCityPlanningProposalResponse) Reset() {
	*x = UpdateCityPlanningProposalResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateCityPlanningProposalResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateCityPlanningProposalResponse) ProtoMessage() {}

func (x *UpdateCityPlanningProposalResponse) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateCityPlanningProposalResponse.ProtoReflect.Descriptor instead.
func (*UpdateCityPlanningProposalResponse) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_city_proto_rawDescGZIP(), []int{7}
}

func (x *UpdateCityPlanningProposalResponse) GetProposal() *CityPlanningProposal {
	if x != nil {
		return x.Proposal
	}
	return nil
}

type PlanningVoid struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *PlanningVoid) Reset() {
	*x = PlanningVoid{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PlanningVoid) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PlanningVoid) ProtoMessage() {}

func (x *PlanningVoid) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PlanningVoid.ProtoReflect.Descriptor instead.
func (*PlanningVoid) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_city_proto_rawDescGZIP(), []int{8}
}

type GetAllCityPlanningProposalResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Proposal []*CityPlanningProposal `protobuf:"bytes,1,rep,name=proposal,proto3" json:"proposal,omitempty"`
}

func (x *GetAllCityPlanningProposalResponse) Reset() {
	*x = GetAllCityPlanningProposalResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[9]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAllCityPlanningProposalResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAllCityPlanningProposalResponse) ProtoMessage() {}

func (x *GetAllCityPlanningProposalResponse) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_city_proto_msgTypes[9]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAllCityPlanningProposalResponse.ProtoReflect.Descriptor instead.
func (*GetAllCityPlanningProposalResponse) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_city_proto_rawDescGZIP(), []int{9}
}

func (x *GetAllCityPlanningProposalResponse) GetProposal() []*CityPlanningProposal {
	if x != nil {
		return x.Proposal
	}
	return nil
}

var File_submodule_for_smart_city_city_city_proto protoreflect.FileDescriptor

var file_submodule_for_smart_city_city_city_proto_rawDesc = []byte{
	0x0a, 0x28, 0x73, 0x75, 0x62, 0x6d, 0x6f, 0x64, 0x75, 0x6c, 0x65, 0x2d, 0x66, 0x6f, 0x72, 0x2d,
	0x73, 0x6d, 0x61, 0x72, 0x74, 0x2d, 0x63, 0x69, 0x74, 0x79, 0x2f, 0x63, 0x69, 0x74, 0x79, 0x2f,
	0x63, 0x69, 0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x04, 0x63, 0x69, 0x74, 0x79,
	0x22, 0xcb, 0x01, 0x0a, 0x14, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e,
	0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x69, 0x74,
	0x6c, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x74, 0x69, 0x74, 0x6c, 0x65, 0x12,
	0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f,
	0x6e, 0x12, 0x25, 0x0a, 0x0e, 0x61, 0x66, 0x66, 0x65, 0x63, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x72,
	0x65, 0x61, 0x73, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x61, 0x66, 0x66, 0x65, 0x63,
	0x74, 0x65, 0x64, 0x41, 0x72, 0x65, 0x61, 0x73, 0x12, 0x21, 0x0a, 0x0c, 0x73, 0x75, 0x62, 0x6d,
	0x69, 0x74, 0x74, 0x65, 0x64, 0x5f, 0x62, 0x79, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b,
	0x73, 0x75, 0x62, 0x6d, 0x69, 0x74, 0x74, 0x65, 0x64, 0x42, 0x79, 0x12, 0x21, 0x0a, 0x0c, 0x73,
	0x75, 0x62, 0x6d, 0x69, 0x74, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0b, 0x73, 0x75, 0x62, 0x6d, 0x69, 0x74, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0xd8,
	0x01, 0x0a, 0x21, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61,
	0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x02, 0x69, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x74, 0x69, 0x74, 0x6c, 0x65, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x05, 0x74, 0x69, 0x74, 0x6c, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65,
	0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x25, 0x0a, 0x0e,
	0x61, 0x66, 0x66, 0x65, 0x63, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x72, 0x65, 0x61, 0x73, 0x18, 0x04,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x61, 0x66, 0x66, 0x65, 0x63, 0x74, 0x65, 0x64, 0x41, 0x72,
	0x65, 0x61, 0x73, 0x12, 0x21, 0x0a, 0x0c, 0x73, 0x75, 0x62, 0x6d, 0x69, 0x74, 0x74, 0x65, 0x64,
	0x5f, 0x62, 0x79, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x73, 0x75, 0x62, 0x6d, 0x69,
	0x74, 0x74, 0x65, 0x64, 0x42, 0x79, 0x12, 0x21, 0x0a, 0x0c, 0x73, 0x75, 0x62, 0x6d, 0x69, 0x74,
	0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x73, 0x75,
	0x62, 0x6d, 0x69, 0x74, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0x30, 0x0a, 0x1e, 0x47, 0x65, 0x74,
	0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70,
	0x6f, 0x73, 0x61, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x5b, 0x0a, 0x21, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e,
	0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x36, 0x0a, 0x08, 0x70, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c,
	0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52, 0x08,
	0x70, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x22, 0x33, 0x0a, 0x21, 0x44, 0x65, 0x6c, 0x65,
	0x74, 0x65, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72,
	0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a,
	0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x5c, 0x0a,
	0x22, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e,
	0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x12, 0x36, 0x0a, 0x08, 0x70, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x43, 0x69, 0x74,
	0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61,
	0x6c, 0x52, 0x08, 0x70, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x22, 0x59, 0x0a, 0x1f, 0x47,
	0x65, 0x74, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72,
	0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x36,
	0x0a, 0x08, 0x70, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x1a, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e,
	0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52, 0x08, 0x70, 0x72,
	0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x22, 0x5c, 0x0a, 0x22, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70,
	0x6f, 0x73, 0x61, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x36, 0x0a, 0x08,
	0x70, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a,
	0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69,
	0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52, 0x08, 0x70, 0x72, 0x6f, 0x70,
	0x6f, 0x73, 0x61, 0x6c, 0x22, 0x0e, 0x0a, 0x0c, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67,
	0x56, 0x6f, 0x69, 0x64, 0x22, 0x5c, 0x0a, 0x22, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x43, 0x69,
	0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73,
	0x61, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x36, 0x0a, 0x08, 0x70, 0x72,
	0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x63,
	0x69, 0x74, 0x79, 0x2e, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67,
	0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52, 0x08, 0x70, 0x72, 0x6f, 0x70, 0x6f, 0x73,
	0x61, 0x6c, 0x32, 0xa8, 0x04, 0x0a, 0x1b, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e,
	0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x53, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x12, 0x71, 0x0a, 0x1a, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x43, 0x69, 0x74, 0x79,
	0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c,
	0x12, 0x27, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x43, 0x69,
	0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73,
	0x61, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x28, 0x2e, 0x63, 0x69, 0x74, 0x79,
	0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e,
	0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x68, 0x0a, 0x17, 0x47, 0x65, 0x74, 0x43, 0x69, 0x74, 0x79,
	0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c,
	0x12, 0x24, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x47, 0x65, 0x74, 0x43, 0x69, 0x74, 0x79, 0x50,
	0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x25, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x47, 0x65,
	0x74, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f,
	0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12,
	0x71, 0x0a, 0x1a, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61,
	0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x12, 0x27, 0x2e,
	0x63, 0x69, 0x74, 0x79, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x43, 0x69, 0x74, 0x79, 0x50,
	0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x28, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x55, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67,
	0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x22, 0x00, 0x12, 0x5b, 0x0a, 0x1a, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x43, 0x69, 0x74, 0x79,
	0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c,
	0x12, 0x27, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x43, 0x69,
	0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73,
	0x61, 0x6c, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x12, 0x2e, 0x63, 0x69, 0x74, 0x79,
	0x2e, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x56, 0x6f, 0x69, 0x64, 0x22, 0x00, 0x12,
	0x5c, 0x0a, 0x1a, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x43, 0x69, 0x74, 0x79, 0x50, 0x6c, 0x61,
	0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f, 0x73, 0x61, 0x6c, 0x12, 0x12, 0x2e,
	0x63, 0x69, 0x74, 0x79, 0x2e, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x56, 0x6f, 0x69,
	0x64, 0x1a, 0x28, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x43,
	0x69, 0x74, 0x79, 0x50, 0x6c, 0x61, 0x6e, 0x6e, 0x69, 0x6e, 0x67, 0x50, 0x72, 0x6f, 0x70, 0x6f,
	0x73, 0x61, 0x6c, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x42, 0x0f, 0x5a,
	0x0d, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x63, 0x69, 0x74, 0x79, 0x62, 0x06,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_submodule_for_smart_city_city_city_proto_rawDescOnce sync.Once
	file_submodule_for_smart_city_city_city_proto_rawDescData = file_submodule_for_smart_city_city_city_proto_rawDesc
)

func file_submodule_for_smart_city_city_city_proto_rawDescGZIP() []byte {
	file_submodule_for_smart_city_city_city_proto_rawDescOnce.Do(func() {
		file_submodule_for_smart_city_city_city_proto_rawDescData = protoimpl.X.CompressGZIP(file_submodule_for_smart_city_city_city_proto_rawDescData)
	})
	return file_submodule_for_smart_city_city_city_proto_rawDescData
}

var file_submodule_for_smart_city_city_city_proto_msgTypes = make([]protoimpl.MessageInfo, 10)
var file_submodule_for_smart_city_city_city_proto_goTypes = []any{
	(*CityPlanningProposal)(nil),               // 0: city.CityPlanningProposal
	(*CreateCityPlanningProposalRequest)(nil),  // 1: city.CreateCityPlanningProposalRequest
	(*GetCityPlanningProposalRequest)(nil),     // 2: city.GetCityPlanningProposalRequest
	(*UpdateCityPlanningProposalRequest)(nil),  // 3: city.UpdateCityPlanningProposalRequest
	(*DeleteCityPlanningProposalRequest)(nil),  // 4: city.DeleteCityPlanningProposalRequest
	(*CreateCityPlanningProposalResponse)(nil), // 5: city.CreateCityPlanningProposalResponse
	(*GetCityPlanningProposalResponse)(nil),    // 6: city.GetCityPlanningProposalResponse
	(*UpdateCityPlanningProposalResponse)(nil), // 7: city.UpdateCityPlanningProposalResponse
	(*PlanningVoid)(nil),                       // 8: city.PlanningVoid
	(*GetAllCityPlanningProposalResponse)(nil), // 9: city.GetAllCityPlanningProposalResponse
}
var file_submodule_for_smart_city_city_city_proto_depIdxs = []int32{
	0,  // 0: city.UpdateCityPlanningProposalRequest.proposal:type_name -> city.CityPlanningProposal
	0,  // 1: city.CreateCityPlanningProposalResponse.proposal:type_name -> city.CityPlanningProposal
	0,  // 2: city.GetCityPlanningProposalResponse.proposal:type_name -> city.CityPlanningProposal
	0,  // 3: city.UpdateCityPlanningProposalResponse.proposal:type_name -> city.CityPlanningProposal
	0,  // 4: city.GetAllCityPlanningProposalResponse.proposal:type_name -> city.CityPlanningProposal
	1,  // 5: city.CityPlanningProposalService.CreateCityPlanningProposal:input_type -> city.CreateCityPlanningProposalRequest
	2,  // 6: city.CityPlanningProposalService.GetCityPlanningProposal:input_type -> city.GetCityPlanningProposalRequest
	3,  // 7: city.CityPlanningProposalService.UpdateCityPlanningProposal:input_type -> city.UpdateCityPlanningProposalRequest
	4,  // 8: city.CityPlanningProposalService.DeleteCityPlanningProposal:input_type -> city.DeleteCityPlanningProposalRequest
	8,  // 9: city.CityPlanningProposalService.GetAllCityPlanningProposal:input_type -> city.PlanningVoid
	5,  // 10: city.CityPlanningProposalService.CreateCityPlanningProposal:output_type -> city.CreateCityPlanningProposalResponse
	6,  // 11: city.CityPlanningProposalService.GetCityPlanningProposal:output_type -> city.GetCityPlanningProposalResponse
	7,  // 12: city.CityPlanningProposalService.UpdateCityPlanningProposal:output_type -> city.UpdateCityPlanningProposalResponse
	8,  // 13: city.CityPlanningProposalService.DeleteCityPlanningProposal:output_type -> city.PlanningVoid
	9,  // 14: city.CityPlanningProposalService.GetAllCityPlanningProposal:output_type -> city.GetAllCityPlanningProposalResponse
	10, // [10:15] is the sub-list for method output_type
	5,  // [5:10] is the sub-list for method input_type
	5,  // [5:5] is the sub-list for extension type_name
	5,  // [5:5] is the sub-list for extension extendee
	0,  // [0:5] is the sub-list for field type_name
}

func init() { file_submodule_for_smart_city_city_city_proto_init() }
func file_submodule_for_smart_city_city_city_proto_init() {
	if File_submodule_for_smart_city_city_city_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_submodule_for_smart_city_city_city_proto_msgTypes[0].Exporter = func(v any, i int) any {
			switch v := v.(*CityPlanningProposal); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_city_proto_msgTypes[1].Exporter = func(v any, i int) any {
			switch v := v.(*CreateCityPlanningProposalRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_city_proto_msgTypes[2].Exporter = func(v any, i int) any {
			switch v := v.(*GetCityPlanningProposalRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_city_proto_msgTypes[3].Exporter = func(v any, i int) any {
			switch v := v.(*UpdateCityPlanningProposalRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_city_proto_msgTypes[4].Exporter = func(v any, i int) any {
			switch v := v.(*DeleteCityPlanningProposalRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_city_proto_msgTypes[5].Exporter = func(v any, i int) any {
			switch v := v.(*CreateCityPlanningProposalResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_city_proto_msgTypes[6].Exporter = func(v any, i int) any {
			switch v := v.(*GetCityPlanningProposalResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_city_proto_msgTypes[7].Exporter = func(v any, i int) any {
			switch v := v.(*UpdateCityPlanningProposalResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_city_proto_msgTypes[8].Exporter = func(v any, i int) any {
			switch v := v.(*PlanningVoid); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_city_proto_msgTypes[9].Exporter = func(v any, i int) any {
			switch v := v.(*GetAllCityPlanningProposalResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_submodule_for_smart_city_city_city_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   10,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_submodule_for_smart_city_city_city_proto_goTypes,
		DependencyIndexes: file_submodule_for_smart_city_city_city_proto_depIdxs,
		MessageInfos:      file_submodule_for_smart_city_city_city_proto_msgTypes,
	}.Build()
	File_submodule_for_smart_city_city_city_proto = out.File
	file_submodule_for_smart_city_city_city_proto_rawDesc = nil
	file_submodule_for_smart_city_city_city_proto_goTypes = nil
	file_submodule_for_smart_city_city_city_proto_depIdxs = nil
}
