// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.2
// 	protoc        v5.27.1
// source: submodule-for-smart-city/city/demographic_data.proto

package city

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type DemographicData struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id           string    `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	ZoneId       *CityZone `protobuf:"bytes,2,opt,name=zone_id,json=zoneId,proto3" json:"zone_id,omitempty"`
	Population   int32     `protobuf:"varint,3,opt,name=population,proto3" json:"population,omitempty"`
	MedianAge    int32     `protobuf:"varint,4,opt,name=median_age,json=medianAge,proto3" json:"median_age,omitempty"`
	MedianIncome float32   `protobuf:"fixed32,5,opt,name=median_income,json=medianIncome,proto3" json:"median_income,omitempty"`
	DataYear     string    `protobuf:"bytes,6,opt,name=data_year,json=dataYear,proto3" json:"data_year,omitempty"`
	CreatedAt    string    `protobuf:"bytes,7,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt    string    `protobuf:"bytes,8,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	DeletedAt    int64     `protobuf:"varint,9,opt,name=deleted_at,json=deletedAt,proto3" json:"deleted_at,omitempty"`
}

func (x *DemographicData) Reset() {
	*x = DemographicData{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DemographicData) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DemographicData) ProtoMessage() {}

func (x *DemographicData) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DemographicData.ProtoReflect.Descriptor instead.
func (*DemographicData) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_demographic_data_proto_rawDescGZIP(), []int{0}
}

func (x *DemographicData) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *DemographicData) GetZoneId() *CityZone {
	if x != nil {
		return x.ZoneId
	}
	return nil
}

func (x *DemographicData) GetPopulation() int32 {
	if x != nil {
		return x.Population
	}
	return 0
}

func (x *DemographicData) GetMedianAge() int32 {
	if x != nil {
		return x.MedianAge
	}
	return 0
}

func (x *DemographicData) GetMedianIncome() float32 {
	if x != nil {
		return x.MedianIncome
	}
	return 0
}

func (x *DemographicData) GetDataYear() string {
	if x != nil {
		return x.DataYear
	}
	return ""
}

func (x *DemographicData) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *DemographicData) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

func (x *DemographicData) GetDeletedAt() int64 {
	if x != nil {
		return x.DeletedAt
	}
	return 0
}

type CreateDemographicDataRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id           string  `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	ZoneId       string  `protobuf:"bytes,2,opt,name=zone_id,json=zoneId,proto3" json:"zone_id,omitempty"`
	Population   int32   `protobuf:"varint,3,opt,name=population,proto3" json:"population,omitempty"`
	MedianAge    int32   `protobuf:"varint,4,opt,name=median_age,json=medianAge,proto3" json:"median_age,omitempty"`
	MedianIncome float32 `protobuf:"fixed32,5,opt,name=median_income,json=medianIncome,proto3" json:"median_income,omitempty"`
	DataYear     string  `protobuf:"bytes,6,opt,name=data_year,json=dataYear,proto3" json:"data_year,omitempty"`
	CreatedAt    string  `protobuf:"bytes,7,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt    string  `protobuf:"bytes,8,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	DeletedAt    int64   `protobuf:"varint,9,opt,name=deleted_at,json=deletedAt,proto3" json:"deleted_at,omitempty"`
}

func (x *CreateDemographicDataRequest) Reset() {
	*x = CreateDemographicDataRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateDemographicDataRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateDemographicDataRequest) ProtoMessage() {}

func (x *CreateDemographicDataRequest) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateDemographicDataRequest.ProtoReflect.Descriptor instead.
func (*CreateDemographicDataRequest) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_demographic_data_proto_rawDescGZIP(), []int{1}
}

func (x *CreateDemographicDataRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *CreateDemographicDataRequest) GetZoneId() string {
	if x != nil {
		return x.ZoneId
	}
	return ""
}

func (x *CreateDemographicDataRequest) GetPopulation() int32 {
	if x != nil {
		return x.Population
	}
	return 0
}

func (x *CreateDemographicDataRequest) GetMedianAge() int32 {
	if x != nil {
		return x.MedianAge
	}
	return 0
}

func (x *CreateDemographicDataRequest) GetMedianIncome() float32 {
	if x != nil {
		return x.MedianIncome
	}
	return 0
}

func (x *CreateDemographicDataRequest) GetDataYear() string {
	if x != nil {
		return x.DataYear
	}
	return ""
}

func (x *CreateDemographicDataRequest) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *CreateDemographicDataRequest) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

func (x *CreateDemographicDataRequest) GetDeletedAt() int64 {
	if x != nil {
		return x.DeletedAt
	}
	return 0
}

type GetDemographicDataRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *GetDemographicDataRequest) Reset() {
	*x = GetDemographicDataRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetDemographicDataRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetDemographicDataRequest) ProtoMessage() {}

func (x *GetDemographicDataRequest) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetDemographicDataRequest.ProtoReflect.Descriptor instead.
func (*GetDemographicDataRequest) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_demographic_data_proto_rawDescGZIP(), []int{2}
}

func (x *GetDemographicDataRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type UpdateDemographicDataRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data *CreateDemographicDataRequest `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *UpdateDemographicDataRequest) Reset() {
	*x = UpdateDemographicDataRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateDemographicDataRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateDemographicDataRequest) ProtoMessage() {}

func (x *UpdateDemographicDataRequest) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateDemographicDataRequest.ProtoReflect.Descriptor instead.
func (*UpdateDemographicDataRequest) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_demographic_data_proto_rawDescGZIP(), []int{3}
}

func (x *UpdateDemographicDataRequest) GetData() *CreateDemographicDataRequest {
	if x != nil {
		return x.Data
	}
	return nil
}

type DeleteDemographicDataRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *DeleteDemographicDataRequest) Reset() {
	*x = DeleteDemographicDataRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeleteDemographicDataRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeleteDemographicDataRequest) ProtoMessage() {}

func (x *DeleteDemographicDataRequest) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeleteDemographicDataRequest.ProtoReflect.Descriptor instead.
func (*DeleteDemographicDataRequest) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_demographic_data_proto_rawDescGZIP(), []int{4}
}

func (x *DeleteDemographicDataRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type CreateDemographicDataResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id           string  `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	ZoneId       string  `protobuf:"bytes,2,opt,name=zone_id,json=zoneId,proto3" json:"zone_id,omitempty"`
	Population   int32   `protobuf:"varint,3,opt,name=population,proto3" json:"population,omitempty"`
	MedianAge    int32   `protobuf:"varint,4,opt,name=median_age,json=medianAge,proto3" json:"median_age,omitempty"`
	MedianIncome float32 `protobuf:"fixed32,5,opt,name=median_income,json=medianIncome,proto3" json:"median_income,omitempty"`
	DataYear     string  `protobuf:"bytes,6,opt,name=data_year,json=dataYear,proto3" json:"data_year,omitempty"`
	CreatedAt    string  `protobuf:"bytes,7,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt    string  `protobuf:"bytes,8,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	DeletedAt    int64   `protobuf:"varint,9,opt,name=deleted_at,json=deletedAt,proto3" json:"deleted_at,omitempty"`
}

func (x *CreateDemographicDataResponse) Reset() {
	*x = CreateDemographicDataResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateDemographicDataResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateDemographicDataResponse) ProtoMessage() {}

func (x *CreateDemographicDataResponse) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateDemographicDataResponse.ProtoReflect.Descriptor instead.
func (*CreateDemographicDataResponse) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_demographic_data_proto_rawDescGZIP(), []int{5}
}

func (x *CreateDemographicDataResponse) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *CreateDemographicDataResponse) GetZoneId() string {
	if x != nil {
		return x.ZoneId
	}
	return ""
}

func (x *CreateDemographicDataResponse) GetPopulation() int32 {
	if x != nil {
		return x.Population
	}
	return 0
}

func (x *CreateDemographicDataResponse) GetMedianAge() int32 {
	if x != nil {
		return x.MedianAge
	}
	return 0
}

func (x *CreateDemographicDataResponse) GetMedianIncome() float32 {
	if x != nil {
		return x.MedianIncome
	}
	return 0
}

func (x *CreateDemographicDataResponse) GetDataYear() string {
	if x != nil {
		return x.DataYear
	}
	return ""
}

func (x *CreateDemographicDataResponse) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *CreateDemographicDataResponse) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

func (x *CreateDemographicDataResponse) GetDeletedAt() int64 {
	if x != nil {
		return x.DeletedAt
	}
	return 0
}

type GetDemographicDataResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data *DemographicData `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *GetDemographicDataResponse) Reset() {
	*x = GetDemographicDataResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetDemographicDataResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetDemographicDataResponse) ProtoMessage() {}

func (x *GetDemographicDataResponse) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetDemographicDataResponse.ProtoReflect.Descriptor instead.
func (*GetDemographicDataResponse) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_demographic_data_proto_rawDescGZIP(), []int{6}
}

func (x *GetDemographicDataResponse) GetData() *DemographicData {
	if x != nil {
		return x.Data
	}
	return nil
}

type UpdateDemographicDataResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data *DemographicData `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *UpdateDemographicDataResponse) Reset() {
	*x = UpdateDemographicDataResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateDemographicDataResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateDemographicDataResponse) ProtoMessage() {}

func (x *UpdateDemographicDataResponse) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateDemographicDataResponse.ProtoReflect.Descriptor instead.
func (*UpdateDemographicDataResponse) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_demographic_data_proto_rawDescGZIP(), []int{7}
}

func (x *UpdateDemographicDataResponse) GetData() *DemographicData {
	if x != nil {
		return x.Data
	}
	return nil
}

type DemographicDataVoid struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *DemographicDataVoid) Reset() {
	*x = DemographicDataVoid{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DemographicDataVoid) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DemographicDataVoid) ProtoMessage() {}

func (x *DemographicDataVoid) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DemographicDataVoid.ProtoReflect.Descriptor instead.
func (*DemographicDataVoid) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_demographic_data_proto_rawDescGZIP(), []int{8}
}

type GetAllDemographicDataResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data []*DemographicData `protobuf:"bytes,1,rep,name=data,proto3" json:"data,omitempty"`
}

func (x *GetAllDemographicDataResponse) Reset() {
	*x = GetAllDemographicDataResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[9]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAllDemographicDataResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAllDemographicDataResponse) ProtoMessage() {}

func (x *GetAllDemographicDataResponse) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[9]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAllDemographicDataResponse.ProtoReflect.Descriptor instead.
func (*GetAllDemographicDataResponse) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_demographic_data_proto_rawDescGZIP(), []int{9}
}

func (x *GetAllDemographicDataResponse) GetData() []*DemographicData {
	if x != nil {
		return x.Data
	}
	return nil
}

var File_submodule_for_smart_city_city_demographic_data_proto protoreflect.FileDescriptor

var file_submodule_for_smart_city_city_demographic_data_proto_rawDesc = []byte{
	0x0a, 0x34, 0x73, 0x75, 0x62, 0x6d, 0x6f, 0x64, 0x75, 0x6c, 0x65, 0x2d, 0x66, 0x6f, 0x72, 0x2d,
	0x73, 0x6d, 0x61, 0x72, 0x74, 0x2d, 0x63, 0x69, 0x74, 0x79, 0x2f, 0x63, 0x69, 0x74, 0x79, 0x2f,
	0x64, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x5f, 0x64, 0x61, 0x74, 0x61,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x04, 0x63, 0x69, 0x74, 0x79, 0x1a, 0x2e, 0x73, 0x75,
	0x62, 0x6d, 0x6f, 0x64, 0x75, 0x6c, 0x65, 0x2d, 0x66, 0x6f, 0x72, 0x2d, 0x73, 0x6d, 0x61, 0x72,
	0x74, 0x2d, 0x63, 0x69, 0x74, 0x79, 0x2f, 0x63, 0x69, 0x74, 0x79, 0x2f, 0x63, 0x69, 0x74, 0x79,
	0x5f, 0x7a, 0x6f, 0x6e, 0x65, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xa8, 0x02, 0x0a,
	0x0f, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74, 0x61,
	0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64,
	0x12, 0x27, 0x0a, 0x07, 0x7a, 0x6f, 0x6e, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x0e, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x43, 0x69, 0x74, 0x79, 0x5a, 0x6f, 0x6e,
	0x65, 0x52, 0x06, 0x7a, 0x6f, 0x6e, 0x65, 0x49, 0x64, 0x12, 0x1e, 0x0a, 0x0a, 0x70, 0x6f, 0x70,
	0x75, 0x6c, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x03, 0x20, 0x01, 0x28, 0x05, 0x52, 0x0a, 0x70,
	0x6f, 0x70, 0x75, 0x6c, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x1d, 0x0a, 0x0a, 0x6d, 0x65, 0x64,
	0x69, 0x61, 0x6e, 0x5f, 0x61, 0x67, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x05, 0x52, 0x09, 0x6d,
	0x65, 0x64, 0x69, 0x61, 0x6e, 0x41, 0x67, 0x65, 0x12, 0x23, 0x0a, 0x0d, 0x6d, 0x65, 0x64, 0x69,
	0x61, 0x6e, 0x5f, 0x69, 0x6e, 0x63, 0x6f, 0x6d, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x02, 0x52,
	0x0c, 0x6d, 0x65, 0x64, 0x69, 0x61, 0x6e, 0x49, 0x6e, 0x63, 0x6f, 0x6d, 0x65, 0x12, 0x1b, 0x0a,
	0x09, 0x64, 0x61, 0x74, 0x61, 0x5f, 0x79, 0x65, 0x61, 0x72, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x08, 0x64, 0x61, 0x74, 0x61, 0x59, 0x65, 0x61, 0x72, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72,
	0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09,
	0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64,
	0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x64, 0x65, 0x6c, 0x65,
	0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x09, 0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x64, 0x65,
	0x6c, 0x65, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0xa5, 0x02, 0x0a, 0x1c, 0x43, 0x72, 0x65, 0x61,
	0x74, 0x65, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74,
	0x61, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x17, 0x0a, 0x07, 0x7a, 0x6f, 0x6e, 0x65,
	0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x7a, 0x6f, 0x6e, 0x65, 0x49,
	0x64, 0x12, 0x1e, 0x0a, 0x0a, 0x70, 0x6f, 0x70, 0x75, 0x6c, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x05, 0x52, 0x0a, 0x70, 0x6f, 0x70, 0x75, 0x6c, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x12, 0x1d, 0x0a, 0x0a, 0x6d, 0x65, 0x64, 0x69, 0x61, 0x6e, 0x5f, 0x61, 0x67, 0x65, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x05, 0x52, 0x09, 0x6d, 0x65, 0x64, 0x69, 0x61, 0x6e, 0x41, 0x67, 0x65,
	0x12, 0x23, 0x0a, 0x0d, 0x6d, 0x65, 0x64, 0x69, 0x61, 0x6e, 0x5f, 0x69, 0x6e, 0x63, 0x6f, 0x6d,
	0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x02, 0x52, 0x0c, 0x6d, 0x65, 0x64, 0x69, 0x61, 0x6e, 0x49,
	0x6e, 0x63, 0x6f, 0x6d, 0x65, 0x12, 0x1b, 0x0a, 0x09, 0x64, 0x61, 0x74, 0x61, 0x5f, 0x79, 0x65,
	0x61, 0x72, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x64, 0x61, 0x74, 0x61, 0x59, 0x65,
	0x61, 0x72, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74,
	0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41,
	0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18,
	0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74,
	0x12, 0x1d, 0x0a, 0x0a, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x09,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22,
	0x2b, 0x0a, 0x19, 0x47, 0x65, 0x74, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69,
	0x63, 0x44, 0x61, 0x74, 0x61, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x56, 0x0a, 0x1c,
	0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69,
	0x63, 0x44, 0x61, 0x74, 0x61, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x36, 0x0a, 0x04,
	0x64, 0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x22, 0x2e, 0x63, 0x69, 0x74,
	0x79, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70,
	0x68, 0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x04,
	0x64, 0x61, 0x74, 0x61, 0x22, 0x2e, 0x0a, 0x1c, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x44, 0x65,
	0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x02, 0x69, 0x64, 0x22, 0xa6, 0x02, 0x0a, 0x1d, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x44,
	0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x17, 0x0a, 0x07, 0x7a, 0x6f, 0x6e, 0x65, 0x5f, 0x69,
	0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x7a, 0x6f, 0x6e, 0x65, 0x49, 0x64, 0x12,
	0x1e, 0x0a, 0x0a, 0x70, 0x6f, 0x70, 0x75, 0x6c, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x05, 0x52, 0x0a, 0x70, 0x6f, 0x70, 0x75, 0x6c, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x12,
	0x1d, 0x0a, 0x0a, 0x6d, 0x65, 0x64, 0x69, 0x61, 0x6e, 0x5f, 0x61, 0x67, 0x65, 0x18, 0x04, 0x20,
	0x01, 0x28, 0x05, 0x52, 0x09, 0x6d, 0x65, 0x64, 0x69, 0x61, 0x6e, 0x41, 0x67, 0x65, 0x12, 0x23,
	0x0a, 0x0d, 0x6d, 0x65, 0x64, 0x69, 0x61, 0x6e, 0x5f, 0x69, 0x6e, 0x63, 0x6f, 0x6d, 0x65, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x02, 0x52, 0x0c, 0x6d, 0x65, 0x64, 0x69, 0x61, 0x6e, 0x49, 0x6e, 0x63,
	0x6f, 0x6d, 0x65, 0x12, 0x1b, 0x0a, 0x09, 0x64, 0x61, 0x74, 0x61, 0x5f, 0x79, 0x65, 0x61, 0x72,
	0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x64, 0x61, 0x74, 0x61, 0x59, 0x65, 0x61, 0x72,
	0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x07,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12,
	0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x08, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d,
	0x0a, 0x0a, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x09, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x09, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0x47, 0x0a,
	0x1a, 0x47, 0x65, 0x74, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44,
	0x61, 0x74, 0x61, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x29, 0x0a, 0x04, 0x64,
	0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x15, 0x2e, 0x63, 0x69, 0x74, 0x79,
	0x2e, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74, 0x61,
	0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x22, 0x4a, 0x0a, 0x1d, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x29, 0x0a, 0x04, 0x64, 0x61, 0x74, 0x61, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x15, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x44, 0x65, 0x6d,
	0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x52, 0x04, 0x64, 0x61,
	0x74, 0x61, 0x22, 0x15, 0x0a, 0x13, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69,
	0x63, 0x44, 0x61, 0x74, 0x61, 0x56, 0x6f, 0x69, 0x64, 0x22, 0x4a, 0x0a, 0x1d, 0x47, 0x65, 0x74,
	0x41, 0x6c, 0x6c, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61,
	0x74, 0x61, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x29, 0x0a, 0x04, 0x64, 0x61,
	0x74, 0x61, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x15, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e,
	0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x52,
	0x04, 0x64, 0x61, 0x74, 0x61, 0x32, 0xe6, 0x03, 0x0a, 0x16, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72,
	0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x12, 0x60, 0x0a, 0x15, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72,
	0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x12, 0x22, 0x2e, 0x63, 0x69, 0x74, 0x79,
	0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68,
	0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x23, 0x2e,
	0x63, 0x69, 0x74, 0x79, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x44, 0x65, 0x6d, 0x6f, 0x67,
	0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x57, 0x0a, 0x12, 0x47, 0x65, 0x74, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61,
	0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x12, 0x1f, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e,
	0x47, 0x65, 0x74, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61,
	0x74, 0x61, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x63, 0x69, 0x74, 0x79,
	0x2e, 0x47, 0x65, 0x74, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44,
	0x61, 0x74, 0x61, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x60, 0x0a, 0x15, 0x55,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63,
	0x44, 0x61, 0x74, 0x61, 0x12, 0x22, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x55, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74,
	0x61, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x23, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e,
	0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69,
	0x63, 0x44, 0x61, 0x74, 0x61, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x56, 0x0a,
	0x15, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68,
	0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x12, 0x22, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x44, 0x65,
	0x6c, 0x65, 0x74, 0x65, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44,
	0x61, 0x74, 0x61, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x63, 0x69, 0x74,
	0x79, 0x2e, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74,
	0x61, 0x56, 0x6f, 0x69, 0x64, 0x12, 0x57, 0x0a, 0x15, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x44,
	0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x12, 0x19,
	0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68, 0x69,
	0x63, 0x44, 0x61, 0x74, 0x61, 0x56, 0x6f, 0x69, 0x64, 0x1a, 0x23, 0x2e, 0x63, 0x69, 0x74, 0x79,
	0x2e, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x44, 0x65, 0x6d, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68,
	0x69, 0x63, 0x44, 0x61, 0x74, 0x61, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x0f,
	0x5a, 0x0d, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x63, 0x69, 0x74, 0x79, 0x62,
	0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_submodule_for_smart_city_city_demographic_data_proto_rawDescOnce sync.Once
	file_submodule_for_smart_city_city_demographic_data_proto_rawDescData = file_submodule_for_smart_city_city_demographic_data_proto_rawDesc
)

func file_submodule_for_smart_city_city_demographic_data_proto_rawDescGZIP() []byte {
	file_submodule_for_smart_city_city_demographic_data_proto_rawDescOnce.Do(func() {
		file_submodule_for_smart_city_city_demographic_data_proto_rawDescData = protoimpl.X.CompressGZIP(file_submodule_for_smart_city_city_demographic_data_proto_rawDescData)
	})
	return file_submodule_for_smart_city_city_demographic_data_proto_rawDescData
}

var file_submodule_for_smart_city_city_demographic_data_proto_msgTypes = make([]protoimpl.MessageInfo, 10)
var file_submodule_for_smart_city_city_demographic_data_proto_goTypes = []any{
	(*DemographicData)(nil),               // 0: city.DemographicData
	(*CreateDemographicDataRequest)(nil),  // 1: city.CreateDemographicDataRequest
	(*GetDemographicDataRequest)(nil),     // 2: city.GetDemographicDataRequest
	(*UpdateDemographicDataRequest)(nil),  // 3: city.UpdateDemographicDataRequest
	(*DeleteDemographicDataRequest)(nil),  // 4: city.DeleteDemographicDataRequest
	(*CreateDemographicDataResponse)(nil), // 5: city.CreateDemographicDataResponse
	(*GetDemographicDataResponse)(nil),    // 6: city.GetDemographicDataResponse
	(*UpdateDemographicDataResponse)(nil), // 7: city.UpdateDemographicDataResponse
	(*DemographicDataVoid)(nil),           // 8: city.DemographicDataVoid
	(*GetAllDemographicDataResponse)(nil), // 9: city.GetAllDemographicDataResponse
	(*CityZone)(nil),                      // 10: city.CityZone
}
var file_submodule_for_smart_city_city_demographic_data_proto_depIdxs = []int32{
	10, // 0: city.DemographicData.zone_id:type_name -> city.CityZone
	1,  // 1: city.UpdateDemographicDataRequest.data:type_name -> city.CreateDemographicDataRequest
	0,  // 2: city.GetDemographicDataResponse.data:type_name -> city.DemographicData
	0,  // 3: city.UpdateDemographicDataResponse.data:type_name -> city.DemographicData
	0,  // 4: city.GetAllDemographicDataResponse.data:type_name -> city.DemographicData
	1,  // 5: city.DemographicDataService.CreateDemographicData:input_type -> city.CreateDemographicDataRequest
	2,  // 6: city.DemographicDataService.GetDemographicData:input_type -> city.GetDemographicDataRequest
	3,  // 7: city.DemographicDataService.UpdateDemographicData:input_type -> city.UpdateDemographicDataRequest
	4,  // 8: city.DemographicDataService.DeleteDemographicData:input_type -> city.DeleteDemographicDataRequest
	8,  // 9: city.DemographicDataService.GetAllDemographicData:input_type -> city.DemographicDataVoid
	5,  // 10: city.DemographicDataService.CreateDemographicData:output_type -> city.CreateDemographicDataResponse
	6,  // 11: city.DemographicDataService.GetDemographicData:output_type -> city.GetDemographicDataResponse
	7,  // 12: city.DemographicDataService.UpdateDemographicData:output_type -> city.UpdateDemographicDataResponse
	8,  // 13: city.DemographicDataService.DeleteDemographicData:output_type -> city.DemographicDataVoid
	9,  // 14: city.DemographicDataService.GetAllDemographicData:output_type -> city.GetAllDemographicDataResponse
	10, // [10:15] is the sub-list for method output_type
	5,  // [5:10] is the sub-list for method input_type
	5,  // [5:5] is the sub-list for extension type_name
	5,  // [5:5] is the sub-list for extension extendee
	0,  // [0:5] is the sub-list for field type_name
}

func init() { file_submodule_for_smart_city_city_demographic_data_proto_init() }
func file_submodule_for_smart_city_city_demographic_data_proto_init() {
	if File_submodule_for_smart_city_city_demographic_data_proto != nil {
		return
	}
	file_submodule_for_smart_city_city_city_zones_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[0].Exporter = func(v any, i int) any {
			switch v := v.(*DemographicData); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[1].Exporter = func(v any, i int) any {
			switch v := v.(*CreateDemographicDataRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[2].Exporter = func(v any, i int) any {
			switch v := v.(*GetDemographicDataRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[3].Exporter = func(v any, i int) any {
			switch v := v.(*UpdateDemographicDataRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[4].Exporter = func(v any, i int) any {
			switch v := v.(*DeleteDemographicDataRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[5].Exporter = func(v any, i int) any {
			switch v := v.(*CreateDemographicDataResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[6].Exporter = func(v any, i int) any {
			switch v := v.(*GetDemographicDataResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[7].Exporter = func(v any, i int) any {
			switch v := v.(*UpdateDemographicDataResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[8].Exporter = func(v any, i int) any {
			switch v := v.(*DemographicDataVoid); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_demographic_data_proto_msgTypes[9].Exporter = func(v any, i int) any {
			switch v := v.(*GetAllDemographicDataResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_submodule_for_smart_city_city_demographic_data_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   10,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_submodule_for_smart_city_city_demographic_data_proto_goTypes,
		DependencyIndexes: file_submodule_for_smart_city_city_demographic_data_proto_depIdxs,
		MessageInfos:      file_submodule_for_smart_city_city_demographic_data_proto_msgTypes,
	}.Build()
	File_submodule_for_smart_city_city_demographic_data_proto = out.File
	file_submodule_for_smart_city_city_demographic_data_proto_rawDesc = nil
	file_submodule_for_smart_city_city_demographic_data_proto_goTypes = nil
	file_submodule_for_smart_city_city_demographic_data_proto_depIdxs = nil
}
