// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.2
// 	protoc        v5.27.1
// source: submodule-for-smart-city/city/building_permits.proto

package city

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type BuildingPermit struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id            string    `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	ZoneId        *CityZone `protobuf:"bytes,2,opt,name=zone_id,json=zoneId,proto3" json:"zone_id,omitempty"`
	PermitType    string    `protobuf:"bytes,3,opt,name=permit_type,json=permitType,proto3" json:"permit_type,omitempty"`
	ApplicantName string    `protobuf:"bytes,4,opt,name=applicant_name,json=applicantName,proto3" json:"applicant_name,omitempty"`
	Status        string    `protobuf:"bytes,5,opt,name=status,proto3" json:"status,omitempty"`
	AppliedAt     string    `protobuf:"bytes,6,opt,name=applied_at,json=appliedAt,proto3" json:"applied_at,omitempty"`
	CreatedAt     string    `protobuf:"bytes,7,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt     string    `protobuf:"bytes,8,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	DeletedAt     int64     `protobuf:"varint,9,opt,name=deleted_at,json=deletedAt,proto3" json:"deleted_at,omitempty"`
}

func (x *BuildingPermit) Reset() {
	*x = BuildingPermit{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BuildingPermit) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BuildingPermit) ProtoMessage() {}

func (x *BuildingPermit) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BuildingPermit.ProtoReflect.Descriptor instead.
func (*BuildingPermit) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_building_permits_proto_rawDescGZIP(), []int{0}
}

func (x *BuildingPermit) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *BuildingPermit) GetZoneId() *CityZone {
	if x != nil {
		return x.ZoneId
	}
	return nil
}

func (x *BuildingPermit) GetPermitType() string {
	if x != nil {
		return x.PermitType
	}
	return ""
}

func (x *BuildingPermit) GetApplicantName() string {
	if x != nil {
		return x.ApplicantName
	}
	return ""
}

func (x *BuildingPermit) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

func (x *BuildingPermit) GetAppliedAt() string {
	if x != nil {
		return x.AppliedAt
	}
	return ""
}

func (x *BuildingPermit) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *BuildingPermit) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

func (x *BuildingPermit) GetDeletedAt() int64 {
	if x != nil {
		return x.DeletedAt
	}
	return 0
}

type CreateBuildingPermitRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id            string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	ZoneId        string `protobuf:"bytes,2,opt,name=zone_id,json=zoneId,proto3" json:"zone_id,omitempty"`
	PermitType    string `protobuf:"bytes,3,opt,name=permit_type,json=permitType,proto3" json:"permit_type,omitempty"`
	ApplicantName string `protobuf:"bytes,4,opt,name=applicant_name,json=applicantName,proto3" json:"applicant_name,omitempty"`
	Status        string `protobuf:"bytes,5,opt,name=status,proto3" json:"status,omitempty"`
	AppliedAt     string `protobuf:"bytes,6,opt,name=applied_at,json=appliedAt,proto3" json:"applied_at,omitempty"`
	CreatedAt     string `protobuf:"bytes,7,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt     string `protobuf:"bytes,8,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	DeletedAt     int64  `protobuf:"varint,9,opt,name=deleted_at,json=deletedAt,proto3" json:"deleted_at,omitempty"`
}

func (x *CreateBuildingPermitRequest) Reset() {
	*x = CreateBuildingPermitRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateBuildingPermitRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateBuildingPermitRequest) ProtoMessage() {}

func (x *CreateBuildingPermitRequest) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateBuildingPermitRequest.ProtoReflect.Descriptor instead.
func (*CreateBuildingPermitRequest) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_building_permits_proto_rawDescGZIP(), []int{1}
}

func (x *CreateBuildingPermitRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *CreateBuildingPermitRequest) GetZoneId() string {
	if x != nil {
		return x.ZoneId
	}
	return ""
}

func (x *CreateBuildingPermitRequest) GetPermitType() string {
	if x != nil {
		return x.PermitType
	}
	return ""
}

func (x *CreateBuildingPermitRequest) GetApplicantName() string {
	if x != nil {
		return x.ApplicantName
	}
	return ""
}

func (x *CreateBuildingPermitRequest) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

func (x *CreateBuildingPermitRequest) GetAppliedAt() string {
	if x != nil {
		return x.AppliedAt
	}
	return ""
}

func (x *CreateBuildingPermitRequest) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *CreateBuildingPermitRequest) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

func (x *CreateBuildingPermitRequest) GetDeletedAt() int64 {
	if x != nil {
		return x.DeletedAt
	}
	return 0
}

type GetBuildingPermitRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *GetBuildingPermitRequest) Reset() {
	*x = GetBuildingPermitRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetBuildingPermitRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetBuildingPermitRequest) ProtoMessage() {}

func (x *GetBuildingPermitRequest) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetBuildingPermitRequest.ProtoReflect.Descriptor instead.
func (*GetBuildingPermitRequest) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_building_permits_proto_rawDescGZIP(), []int{2}
}

func (x *GetBuildingPermitRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type UpdateBuildingPermitRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Permit *CreateBuildingPermitRequest `protobuf:"bytes,1,opt,name=permit,proto3" json:"permit,omitempty"`
}

func (x *UpdateBuildingPermitRequest) Reset() {
	*x = UpdateBuildingPermitRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateBuildingPermitRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateBuildingPermitRequest) ProtoMessage() {}

func (x *UpdateBuildingPermitRequest) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateBuildingPermitRequest.ProtoReflect.Descriptor instead.
func (*UpdateBuildingPermitRequest) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_building_permits_proto_rawDescGZIP(), []int{3}
}

func (x *UpdateBuildingPermitRequest) GetPermit() *CreateBuildingPermitRequest {
	if x != nil {
		return x.Permit
	}
	return nil
}

type DeleteBuildingPermitRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *DeleteBuildingPermitRequest) Reset() {
	*x = DeleteBuildingPermitRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeleteBuildingPermitRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeleteBuildingPermitRequest) ProtoMessage() {}

func (x *DeleteBuildingPermitRequest) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeleteBuildingPermitRequest.ProtoReflect.Descriptor instead.
func (*DeleteBuildingPermitRequest) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_building_permits_proto_rawDescGZIP(), []int{4}
}

func (x *DeleteBuildingPermitRequest) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type CreateBuildingPermitResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id            string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	ZoneId        string `protobuf:"bytes,2,opt,name=zone_id,json=zoneId,proto3" json:"zone_id,omitempty"`
	PermitType    string `protobuf:"bytes,3,opt,name=permit_type,json=permitType,proto3" json:"permit_type,omitempty"`
	ApplicantName string `protobuf:"bytes,4,opt,name=applicant_name,json=applicantName,proto3" json:"applicant_name,omitempty"`
	Status        string `protobuf:"bytes,5,opt,name=status,proto3" json:"status,omitempty"`
	AppliedAt     string `protobuf:"bytes,6,opt,name=applied_at,json=appliedAt,proto3" json:"applied_at,omitempty"`
	CreatedAt     string `protobuf:"bytes,7,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt     string `protobuf:"bytes,8,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	DeletedAt     int64  `protobuf:"varint,9,opt,name=deleted_at,json=deletedAt,proto3" json:"deleted_at,omitempty"`
}

func (x *CreateBuildingPermitResponse) Reset() {
	*x = CreateBuildingPermitResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateBuildingPermitResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateBuildingPermitResponse) ProtoMessage() {}

func (x *CreateBuildingPermitResponse) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateBuildingPermitResponse.ProtoReflect.Descriptor instead.
func (*CreateBuildingPermitResponse) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_building_permits_proto_rawDescGZIP(), []int{5}
}

func (x *CreateBuildingPermitResponse) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *CreateBuildingPermitResponse) GetZoneId() string {
	if x != nil {
		return x.ZoneId
	}
	return ""
}

func (x *CreateBuildingPermitResponse) GetPermitType() string {
	if x != nil {
		return x.PermitType
	}
	return ""
}

func (x *CreateBuildingPermitResponse) GetApplicantName() string {
	if x != nil {
		return x.ApplicantName
	}
	return ""
}

func (x *CreateBuildingPermitResponse) GetStatus() string {
	if x != nil {
		return x.Status
	}
	return ""
}

func (x *CreateBuildingPermitResponse) GetAppliedAt() string {
	if x != nil {
		return x.AppliedAt
	}
	return ""
}

func (x *CreateBuildingPermitResponse) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *CreateBuildingPermitResponse) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

func (x *CreateBuildingPermitResponse) GetDeletedAt() int64 {
	if x != nil {
		return x.DeletedAt
	}
	return 0
}

type GetBuildingPermitResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Permit *BuildingPermit `protobuf:"bytes,1,opt,name=permit,proto3" json:"permit,omitempty"`
}

func (x *GetBuildingPermitResponse) Reset() {
	*x = GetBuildingPermitResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetBuildingPermitResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetBuildingPermitResponse) ProtoMessage() {}

func (x *GetBuildingPermitResponse) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetBuildingPermitResponse.ProtoReflect.Descriptor instead.
func (*GetBuildingPermitResponse) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_building_permits_proto_rawDescGZIP(), []int{6}
}

func (x *GetBuildingPermitResponse) GetPermit() *BuildingPermit {
	if x != nil {
		return x.Permit
	}
	return nil
}

type UpdateBuildingPermitResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Permit *BuildingPermit `protobuf:"bytes,1,opt,name=permit,proto3" json:"permit,omitempty"`
}

func (x *UpdateBuildingPermitResponse) Reset() {
	*x = UpdateBuildingPermitResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateBuildingPermitResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateBuildingPermitResponse) ProtoMessage() {}

func (x *UpdateBuildingPermitResponse) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateBuildingPermitResponse.ProtoReflect.Descriptor instead.
func (*UpdateBuildingPermitResponse) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_building_permits_proto_rawDescGZIP(), []int{7}
}

func (x *UpdateBuildingPermitResponse) GetPermit() *BuildingPermit {
	if x != nil {
		return x.Permit
	}
	return nil
}

type BuildingPermitsVoid struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *BuildingPermitsVoid) Reset() {
	*x = BuildingPermitsVoid{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BuildingPermitsVoid) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BuildingPermitsVoid) ProtoMessage() {}

func (x *BuildingPermitsVoid) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BuildingPermitsVoid.ProtoReflect.Descriptor instead.
func (*BuildingPermitsVoid) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_building_permits_proto_rawDescGZIP(), []int{8}
}

type GetAllBuildingPermitsResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Permits []*BuildingPermit `protobuf:"bytes,1,rep,name=permits,proto3" json:"permits,omitempty"`
}

func (x *GetAllBuildingPermitsResponse) Reset() {
	*x = GetAllBuildingPermitsResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[9]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAllBuildingPermitsResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAllBuildingPermitsResponse) ProtoMessage() {}

func (x *GetAllBuildingPermitsResponse) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_city_building_permits_proto_msgTypes[9]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAllBuildingPermitsResponse.ProtoReflect.Descriptor instead.
func (*GetAllBuildingPermitsResponse) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_city_building_permits_proto_rawDescGZIP(), []int{9}
}

func (x *GetAllBuildingPermitsResponse) GetPermits() []*BuildingPermit {
	if x != nil {
		return x.Permits
	}
	return nil
}

var File_submodule_for_smart_city_city_building_permits_proto protoreflect.FileDescriptor

var file_submodule_for_smart_city_city_building_permits_proto_rawDesc = []byte{
	0x0a, 0x34, 0x73, 0x75, 0x62, 0x6d, 0x6f, 0x64, 0x75, 0x6c, 0x65, 0x2d, 0x66, 0x6f, 0x72, 0x2d,
	0x73, 0x6d, 0x61, 0x72, 0x74, 0x2d, 0x63, 0x69, 0x74, 0x79, 0x2f, 0x63, 0x69, 0x74, 0x79, 0x2f,
	0x62, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x5f, 0x70, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x73,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x04, 0x63, 0x69, 0x74, 0x79, 0x1a, 0x2e, 0x73, 0x75,
	0x62, 0x6d, 0x6f, 0x64, 0x75, 0x6c, 0x65, 0x2d, 0x66, 0x6f, 0x72, 0x2d, 0x73, 0x6d, 0x61, 0x72,
	0x74, 0x2d, 0x63, 0x69, 0x74, 0x79, 0x2f, 0x63, 0x69, 0x74, 0x79, 0x2f, 0x63, 0x69, 0x74, 0x79,
	0x5f, 0x7a, 0x6f, 0x6e, 0x65, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xa5, 0x02, 0x0a,
	0x0e, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x12,
	0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12,
	0x27, 0x0a, 0x07, 0x7a, 0x6f, 0x6e, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x0e, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x43, 0x69, 0x74, 0x79, 0x5a, 0x6f, 0x6e, 0x65,
	0x52, 0x06, 0x7a, 0x6f, 0x6e, 0x65, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x70, 0x65, 0x72, 0x6d,
	0x69, 0x74, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x70,
	0x65, 0x72, 0x6d, 0x69, 0x74, 0x54, 0x79, 0x70, 0x65, 0x12, 0x25, 0x0a, 0x0e, 0x61, 0x70, 0x70,
	0x6c, 0x69, 0x63, 0x61, 0x6e, 0x74, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0d, 0x61, 0x70, 0x70, 0x6c, 0x69, 0x63, 0x61, 0x6e, 0x74, 0x4e, 0x61, 0x6d, 0x65,
	0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x1d, 0x0a, 0x0a, 0x61, 0x70, 0x70, 0x6c,
	0x69, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x61, 0x70,
	0x70, 0x6c, 0x69, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65,
	0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x64, 0x5f, 0x61, 0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64,
	0x5f, 0x61, 0x74, 0x18, 0x09, 0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x64, 0x65, 0x6c, 0x65, 0x74,
	0x65, 0x64, 0x41, 0x74, 0x22, 0xa2, 0x02, 0x0a, 0x1b, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x42,
	0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x02, 0x69, 0x64, 0x12, 0x17, 0x0a, 0x07, 0x7a, 0x6f, 0x6e, 0x65, 0x5f, 0x69, 0x64, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x7a, 0x6f, 0x6e, 0x65, 0x49, 0x64, 0x12, 0x1f, 0x0a,
	0x0b, 0x70, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x0a, 0x70, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x54, 0x79, 0x70, 0x65, 0x12, 0x25,
	0x0a, 0x0e, 0x61, 0x70, 0x70, 0x6c, 0x69, 0x63, 0x61, 0x6e, 0x74, 0x5f, 0x6e, 0x61, 0x6d, 0x65,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x61, 0x70, 0x70, 0x6c, 0x69, 0x63, 0x61, 0x6e,
	0x74, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x1d, 0x0a,
	0x0a, 0x61, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x09, 0x61, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a,
	0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x64, 0x65,
	0x6c, 0x65, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x09, 0x20, 0x01, 0x28, 0x03, 0x52, 0x09,
	0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0x2a, 0x0a, 0x18, 0x47, 0x65, 0x74,
	0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x58, 0x0a, 0x1b, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x42,
	0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x39, 0x0a, 0x06, 0x70, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x0b, 0x32, 0x21, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x43, 0x72, 0x65, 0x61,
	0x74, 0x65, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x52, 0x06, 0x70, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x22,
	0x2d, 0x0a, 0x1b, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e,
	0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e,
	0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0xa3,
	0x02, 0x0a, 0x1c, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e,
	0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12,
	0x17, 0x0a, 0x07, 0x7a, 0x6f, 0x6e, 0x65, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x06, 0x7a, 0x6f, 0x6e, 0x65, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x70, 0x65, 0x72, 0x6d,
	0x69, 0x74, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x70,
	0x65, 0x72, 0x6d, 0x69, 0x74, 0x54, 0x79, 0x70, 0x65, 0x12, 0x25, 0x0a, 0x0e, 0x61, 0x70, 0x70,
	0x6c, 0x69, 0x63, 0x61, 0x6e, 0x74, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0d, 0x61, 0x70, 0x70, 0x6c, 0x69, 0x63, 0x61, 0x6e, 0x74, 0x4e, 0x61, 0x6d, 0x65,
	0x12, 0x16, 0x0a, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x1d, 0x0a, 0x0a, 0x61, 0x70, 0x70, 0x6c,
	0x69, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x61, 0x70,
	0x70, 0x6c, 0x69, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65,
	0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x64, 0x5f, 0x61, 0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x64, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x64,
	0x5f, 0x61, 0x74, 0x18, 0x09, 0x20, 0x01, 0x28, 0x03, 0x52, 0x09, 0x64, 0x65, 0x6c, 0x65, 0x74,
	0x65, 0x64, 0x41, 0x74, 0x22, 0x49, 0x0a, 0x19, 0x47, 0x65, 0x74, 0x42, 0x75, 0x69, 0x6c, 0x64,
	0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x12, 0x2c, 0x0a, 0x06, 0x70, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x14, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e,
	0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x06, 0x70, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x22,
	0x4c, 0x0a, 0x1c, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e,
	0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12,
	0x2c, 0x0a, 0x06, 0x70, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32,
	0x14, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50,
	0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x06, 0x70, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x22, 0x15, 0x0a,
	0x13, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x73,
	0x56, 0x6f, 0x69, 0x64, 0x22, 0x4f, 0x0a, 0x1d, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x42, 0x75,
	0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x73, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x2e, 0x0a, 0x07, 0x70, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x73,
	0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x14, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x42, 0x75,
	0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x07, 0x70, 0x65,
	0x72, 0x6d, 0x69, 0x74, 0x73, 0x32, 0xda, 0x03, 0x0a, 0x15, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69,
	0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12,
	0x5d, 0x0a, 0x14, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e,
	0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x12, 0x21, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x43,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72,
	0x6d, 0x69, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x22, 0x2e, 0x63, 0x69, 0x74,
	0x79, 0x2e, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67,
	0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x54,
	0x0a, 0x11, 0x47, 0x65, 0x74, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72,
	0x6d, 0x69, 0x74, 0x12, 0x1e, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x47, 0x65, 0x74, 0x42, 0x75,
	0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x1f, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x47, 0x65, 0x74, 0x42, 0x75,
	0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x12, 0x5d, 0x0a, 0x14, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x42, 0x75,
	0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x12, 0x21, 0x2e, 0x63,
	0x69, 0x74, 0x79, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69,
	0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a,
	0x22, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x42, 0x75, 0x69,
	0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x12, 0x54, 0x0a, 0x14, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x42, 0x75, 0x69,
	0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x12, 0x21, 0x2e, 0x63, 0x69,
	0x74, 0x79, 0x2e, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e,
	0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19,
	0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65,
	0x72, 0x6d, 0x69, 0x74, 0x73, 0x56, 0x6f, 0x69, 0x64, 0x12, 0x57, 0x0a, 0x15, 0x47, 0x65, 0x74,
	0x41, 0x6c, 0x6c, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69,
	0x74, 0x73, 0x12, 0x19, 0x2e, 0x63, 0x69, 0x74, 0x79, 0x2e, 0x42, 0x75, 0x69, 0x6c, 0x64, 0x69,
	0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x73, 0x56, 0x6f, 0x69, 0x64, 0x1a, 0x23, 0x2e,
	0x63, 0x69, 0x74, 0x79, 0x2e, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x42, 0x75, 0x69, 0x6c, 0x64,
	0x69, 0x6e, 0x67, 0x50, 0x65, 0x72, 0x6d, 0x69, 0x74, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x42, 0x0f, 0x5a, 0x0d, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x63,
	0x69, 0x74, 0x79, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_submodule_for_smart_city_city_building_permits_proto_rawDescOnce sync.Once
	file_submodule_for_smart_city_city_building_permits_proto_rawDescData = file_submodule_for_smart_city_city_building_permits_proto_rawDesc
)

func file_submodule_for_smart_city_city_building_permits_proto_rawDescGZIP() []byte {
	file_submodule_for_smart_city_city_building_permits_proto_rawDescOnce.Do(func() {
		file_submodule_for_smart_city_city_building_permits_proto_rawDescData = protoimpl.X.CompressGZIP(file_submodule_for_smart_city_city_building_permits_proto_rawDescData)
	})
	return file_submodule_for_smart_city_city_building_permits_proto_rawDescData
}

var file_submodule_for_smart_city_city_building_permits_proto_msgTypes = make([]protoimpl.MessageInfo, 10)
var file_submodule_for_smart_city_city_building_permits_proto_goTypes = []any{
	(*BuildingPermit)(nil),                // 0: city.BuildingPermit
	(*CreateBuildingPermitRequest)(nil),   // 1: city.CreateBuildingPermitRequest
	(*GetBuildingPermitRequest)(nil),      // 2: city.GetBuildingPermitRequest
	(*UpdateBuildingPermitRequest)(nil),   // 3: city.UpdateBuildingPermitRequest
	(*DeleteBuildingPermitRequest)(nil),   // 4: city.DeleteBuildingPermitRequest
	(*CreateBuildingPermitResponse)(nil),  // 5: city.CreateBuildingPermitResponse
	(*GetBuildingPermitResponse)(nil),     // 6: city.GetBuildingPermitResponse
	(*UpdateBuildingPermitResponse)(nil),  // 7: city.UpdateBuildingPermitResponse
	(*BuildingPermitsVoid)(nil),           // 8: city.BuildingPermitsVoid
	(*GetAllBuildingPermitsResponse)(nil), // 9: city.GetAllBuildingPermitsResponse
	(*CityZone)(nil),                      // 10: city.CityZone
}
var file_submodule_for_smart_city_city_building_permits_proto_depIdxs = []int32{
	10, // 0: city.BuildingPermit.zone_id:type_name -> city.CityZone
	1,  // 1: city.UpdateBuildingPermitRequest.permit:type_name -> city.CreateBuildingPermitRequest
	0,  // 2: city.GetBuildingPermitResponse.permit:type_name -> city.BuildingPermit
	0,  // 3: city.UpdateBuildingPermitResponse.permit:type_name -> city.BuildingPermit
	0,  // 4: city.GetAllBuildingPermitsResponse.permits:type_name -> city.BuildingPermit
	1,  // 5: city.BuildingPermitService.CreateBuildingPermit:input_type -> city.CreateBuildingPermitRequest
	2,  // 6: city.BuildingPermitService.GetBuildingPermit:input_type -> city.GetBuildingPermitRequest
	3,  // 7: city.BuildingPermitService.UpdateBuildingPermit:input_type -> city.UpdateBuildingPermitRequest
	4,  // 8: city.BuildingPermitService.DeleteBuildingPermit:input_type -> city.DeleteBuildingPermitRequest
	8,  // 9: city.BuildingPermitService.GetAllBuildingPermits:input_type -> city.BuildingPermitsVoid
	5,  // 10: city.BuildingPermitService.CreateBuildingPermit:output_type -> city.CreateBuildingPermitResponse
	6,  // 11: city.BuildingPermitService.GetBuildingPermit:output_type -> city.GetBuildingPermitResponse
	7,  // 12: city.BuildingPermitService.UpdateBuildingPermit:output_type -> city.UpdateBuildingPermitResponse
	8,  // 13: city.BuildingPermitService.DeleteBuildingPermit:output_type -> city.BuildingPermitsVoid
	9,  // 14: city.BuildingPermitService.GetAllBuildingPermits:output_type -> city.GetAllBuildingPermitsResponse
	10, // [10:15] is the sub-list for method output_type
	5,  // [5:10] is the sub-list for method input_type
	5,  // [5:5] is the sub-list for extension type_name
	5,  // [5:5] is the sub-list for extension extendee
	0,  // [0:5] is the sub-list for field type_name
}

func init() { file_submodule_for_smart_city_city_building_permits_proto_init() }
func file_submodule_for_smart_city_city_building_permits_proto_init() {
	if File_submodule_for_smart_city_city_building_permits_proto != nil {
		return
	}
	file_submodule_for_smart_city_city_city_zones_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_submodule_for_smart_city_city_building_permits_proto_msgTypes[0].Exporter = func(v any, i int) any {
			switch v := v.(*BuildingPermit); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_building_permits_proto_msgTypes[1].Exporter = func(v any, i int) any {
			switch v := v.(*CreateBuildingPermitRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_building_permits_proto_msgTypes[2].Exporter = func(v any, i int) any {
			switch v := v.(*GetBuildingPermitRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_building_permits_proto_msgTypes[3].Exporter = func(v any, i int) any {
			switch v := v.(*UpdateBuildingPermitRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_building_permits_proto_msgTypes[4].Exporter = func(v any, i int) any {
			switch v := v.(*DeleteBuildingPermitRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_building_permits_proto_msgTypes[5].Exporter = func(v any, i int) any {
			switch v := v.(*CreateBuildingPermitResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_building_permits_proto_msgTypes[6].Exporter = func(v any, i int) any {
			switch v := v.(*GetBuildingPermitResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_building_permits_proto_msgTypes[7].Exporter = func(v any, i int) any {
			switch v := v.(*UpdateBuildingPermitResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_building_permits_proto_msgTypes[8].Exporter = func(v any, i int) any {
			switch v := v.(*BuildingPermitsVoid); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_city_building_permits_proto_msgTypes[9].Exporter = func(v any, i int) any {
			switch v := v.(*GetAllBuildingPermitsResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_submodule_for_smart_city_city_building_permits_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   10,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_submodule_for_smart_city_city_building_permits_proto_goTypes,
		DependencyIndexes: file_submodule_for_smart_city_city_building_permits_proto_depIdxs,
		MessageInfos:      file_submodule_for_smart_city_city_building_permits_proto_msgTypes,
	}.Build()
	File_submodule_for_smart_city_city_building_permits_proto = out.File
	file_submodule_for_smart_city_city_building_permits_proto_rawDesc = nil
	file_submodule_for_smart_city_city_building_permits_proto_goTypes = nil
	file_submodule_for_smart_city_city_building_permits_proto_depIdxs = nil
}
