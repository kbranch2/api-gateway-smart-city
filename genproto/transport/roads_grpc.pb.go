// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.4.0
// - protoc             v5.27.1
// source: submodule-for-smart-city/transportation-service/roads.proto

package transport

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.62.0 or later.
const _ = grpc.SupportPackageIsVersion8

const (
	RoadService_Create_FullMethodName  = "/transport.RoadService/Create"
	RoadService_Delete_FullMethodName  = "/transport.RoadService/Delete"
	RoadService_Update_FullMethodName  = "/transport.RoadService/Update"
	RoadService_GetById_FullMethodName = "/transport.RoadService/GetById"
	RoadService_GetAll_FullMethodName  = "/transport.RoadService/GetAll"
)

// RoadServiceClient is the client API for RoadService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type RoadServiceClient interface {
	Create(ctx context.Context, in *Road, opts ...grpc.CallOption) (*Void, error)
	Delete(ctx context.Context, in *ById, opts ...grpc.CallOption) (*Void, error)
	Update(ctx context.Context, in *Road, opts ...grpc.CallOption) (*Void, error)
	GetById(ctx context.Context, in *ById, opts ...grpc.CallOption) (*Road, error)
	GetAll(ctx context.Context, in *Road, opts ...grpc.CallOption) (*AllRoads, error)
}

type roadServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewRoadServiceClient(cc grpc.ClientConnInterface) RoadServiceClient {
	return &roadServiceClient{cc}
}

func (c *roadServiceClient) Create(ctx context.Context, in *Road, opts ...grpc.CallOption) (*Void, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(Void)
	err := c.cc.Invoke(ctx, RoadService_Create_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *roadServiceClient) Delete(ctx context.Context, in *ById, opts ...grpc.CallOption) (*Void, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(Void)
	err := c.cc.Invoke(ctx, RoadService_Delete_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *roadServiceClient) Update(ctx context.Context, in *Road, opts ...grpc.CallOption) (*Void, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(Void)
	err := c.cc.Invoke(ctx, RoadService_Update_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *roadServiceClient) GetById(ctx context.Context, in *ById, opts ...grpc.CallOption) (*Road, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(Road)
	err := c.cc.Invoke(ctx, RoadService_GetById_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *roadServiceClient) GetAll(ctx context.Context, in *Road, opts ...grpc.CallOption) (*AllRoads, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(AllRoads)
	err := c.cc.Invoke(ctx, RoadService_GetAll_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RoadServiceServer is the server API for RoadService service.
// All implementations must embed UnimplementedRoadServiceServer
// for forward compatibility
type RoadServiceServer interface {
	Create(context.Context, *Road) (*Void, error)
	Delete(context.Context, *ById) (*Void, error)
	Update(context.Context, *Road) (*Void, error)
	GetById(context.Context, *ById) (*Road, error)
	GetAll(context.Context, *Road) (*AllRoads, error)
	mustEmbedUnimplementedRoadServiceServer()
}

// UnimplementedRoadServiceServer must be embedded to have forward compatible implementations.
type UnimplementedRoadServiceServer struct {
}

func (UnimplementedRoadServiceServer) Create(context.Context, *Road) (*Void, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedRoadServiceServer) Delete(context.Context, *ById) (*Void, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedRoadServiceServer) Update(context.Context, *Road) (*Void, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedRoadServiceServer) GetById(context.Context, *ById) (*Road, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetById not implemented")
}
func (UnimplementedRoadServiceServer) GetAll(context.Context, *Road) (*AllRoads, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAll not implemented")
}
func (UnimplementedRoadServiceServer) mustEmbedUnimplementedRoadServiceServer() {}

// UnsafeRoadServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to RoadServiceServer will
// result in compilation errors.
type UnsafeRoadServiceServer interface {
	mustEmbedUnimplementedRoadServiceServer()
}

func RegisterRoadServiceServer(s grpc.ServiceRegistrar, srv RoadServiceServer) {
	s.RegisterService(&RoadService_ServiceDesc, srv)
}

func _RoadService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Road)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RoadServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RoadService_Create_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RoadServiceServer).Create(ctx, req.(*Road))
	}
	return interceptor(ctx, in, info, handler)
}

func _RoadService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ById)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RoadServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RoadService_Delete_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RoadServiceServer).Delete(ctx, req.(*ById))
	}
	return interceptor(ctx, in, info, handler)
}

func _RoadService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Road)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RoadServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RoadService_Update_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RoadServiceServer).Update(ctx, req.(*Road))
	}
	return interceptor(ctx, in, info, handler)
}

func _RoadService_GetById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ById)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RoadServiceServer).GetById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RoadService_GetById_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RoadServiceServer).GetById(ctx, req.(*ById))
	}
	return interceptor(ctx, in, info, handler)
}

func _RoadService_GetAll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Road)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RoadServiceServer).GetAll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: RoadService_GetAll_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RoadServiceServer).GetAll(ctx, req.(*Road))
	}
	return interceptor(ctx, in, info, handler)
}

// RoadService_ServiceDesc is the grpc.ServiceDesc for RoadService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var RoadService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "transport.RoadService",
	HandlerType: (*RoadServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _RoadService_Create_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _RoadService_Delete_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _RoadService_Update_Handler,
		},
		{
			MethodName: "GetById",
			Handler:    _RoadService_GetById_Handler,
		},
		{
			MethodName: "GetAll",
			Handler:    _RoadService_GetAll_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "submodule-for-smart-city/transportation-service/roads.proto",
}
