// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.2
// 	protoc        v5.27.1
// source: submodule-for-smart-city/transportation-service/traffic_conditions.proto

package transport

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type TrafficCondition struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ConditionId  string `protobuf:"bytes,1,opt,name=condition_id,json=conditionId,proto3" json:"condition_id,omitempty"`
	RoadId       string `protobuf:"bytes,2,opt,name=road_id,json=roadId,proto3" json:"road_id,omitempty"`
	TrafficLevel int32  `protobuf:"varint,3,opt,name=traffic_level,json=trafficLevel,proto3" json:"traffic_level,omitempty"`
	Timestamp    string `protobuf:"bytes,4,opt,name=timestamp,proto3" json:"timestamp,omitempty"`
}

func (x *TrafficCondition) Reset() {
	*x = TrafficCondition{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TrafficCondition) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TrafficCondition) ProtoMessage() {}

func (x *TrafficCondition) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TrafficCondition.ProtoReflect.Descriptor instead.
func (*TrafficCondition) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_rawDescGZIP(), []int{0}
}

func (x *TrafficCondition) GetConditionId() string {
	if x != nil {
		return x.ConditionId
	}
	return ""
}

func (x *TrafficCondition) GetRoadId() string {
	if x != nil {
		return x.RoadId
	}
	return ""
}

func (x *TrafficCondition) GetTrafficLevel() int32 {
	if x != nil {
		return x.TrafficLevel
	}
	return 0
}

func (x *TrafficCondition) GetTimestamp() string {
	if x != nil {
		return x.Timestamp
	}
	return ""
}

type AllTrafficConditions struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	TrafficConditions []*TrafficCondition `protobuf:"bytes,1,rep,name=TrafficConditions,proto3" json:"TrafficConditions,omitempty"`
}

func (x *AllTrafficConditions) Reset() {
	*x = AllTrafficConditions{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AllTrafficConditions) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AllTrafficConditions) ProtoMessage() {}

func (x *AllTrafficConditions) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AllTrafficConditions.ProtoReflect.Descriptor instead.
func (*AllTrafficConditions) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_rawDescGZIP(), []int{1}
}

func (x *AllTrafficConditions) GetTrafficConditions() []*TrafficCondition {
	if x != nil {
		return x.TrafficConditions
	}
	return nil
}

var File_submodule_for_smart_city_transportation_service_traffic_conditions_proto protoreflect.FileDescriptor

var file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_rawDesc = []byte{
	0x0a, 0x48, 0x73, 0x75, 0x62, 0x6d, 0x6f, 0x64, 0x75, 0x6c, 0x65, 0x2d, 0x66, 0x6f, 0x72, 0x2d,
	0x73, 0x6d, 0x61, 0x72, 0x74, 0x2d, 0x63, 0x69, 0x74, 0x79, 0x2f, 0x74, 0x72, 0x61, 0x6e, 0x73,
	0x70, 0x6f, 0x72, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2d, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x2f, 0x74, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63, 0x5f, 0x63, 0x6f, 0x6e, 0x64, 0x69, 0x74,
	0x69, 0x6f, 0x6e, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x09, 0x74, 0x72, 0x61, 0x6e,
	0x73, 0x70, 0x6f, 0x72, 0x74, 0x1a, 0x3b, 0x73, 0x75, 0x62, 0x6d, 0x6f, 0x64, 0x75, 0x6c, 0x65,
	0x2d, 0x66, 0x6f, 0x72, 0x2d, 0x73, 0x6d, 0x61, 0x72, 0x74, 0x2d, 0x63, 0x69, 0x74, 0x79, 0x2f,
	0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2d, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2f, 0x72, 0x6f, 0x61, 0x64, 0x73, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x22, 0x91, 0x01, 0x0a, 0x10, 0x54, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63, 0x43, 0x6f,
	0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x21, 0x0a, 0x0c, 0x63, 0x6f, 0x6e, 0x64, 0x69,
	0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x63,
	0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x49, 0x64, 0x12, 0x17, 0x0a, 0x07, 0x72, 0x6f,
	0x61, 0x64, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x72, 0x6f, 0x61,
	0x64, 0x49, 0x64, 0x12, 0x23, 0x0a, 0x0d, 0x74, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63, 0x5f, 0x6c,
	0x65, 0x76, 0x65, 0x6c, 0x18, 0x03, 0x20, 0x01, 0x28, 0x05, 0x52, 0x0c, 0x74, 0x72, 0x61, 0x66,
	0x66, 0x69, 0x63, 0x4c, 0x65, 0x76, 0x65, 0x6c, 0x12, 0x1c, 0x0a, 0x09, 0x74, 0x69, 0x6d, 0x65,
	0x73, 0x74, 0x61, 0x6d, 0x70, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x74, 0x69, 0x6d,
	0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x22, 0x61, 0x0a, 0x14, 0x41, 0x6c, 0x6c, 0x54, 0x72, 0x61,
	0x66, 0x66, 0x69, 0x63, 0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x12, 0x49,
	0x0a, 0x11, 0x54, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63, 0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69,
	0x6f, 0x6e, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x1b, 0x2e, 0x74, 0x72, 0x61, 0x6e,
	0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x54, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63, 0x43, 0x6f, 0x6e,
	0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x11, 0x54, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63, 0x43,
	0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x32, 0xc0, 0x02, 0x0a, 0x17, 0x54, 0x72,
	0x61, 0x66, 0x66, 0x69, 0x63, 0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x53, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x38, 0x0a, 0x06, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x12,
	0x1b, 0x2e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x54, 0x72, 0x61, 0x66,
	0x66, 0x69, 0x63, 0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x1a, 0x0f, 0x2e, 0x74,
	0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x56, 0x6f, 0x69, 0x64, 0x22, 0x00, 0x12,
	0x2c, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x0f, 0x2e, 0x74, 0x72, 0x61, 0x6e,
	0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x42, 0x79, 0x49, 0x64, 0x1a, 0x0f, 0x2e, 0x74, 0x72, 0x61,
	0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x56, 0x6f, 0x69, 0x64, 0x22, 0x00, 0x12, 0x38, 0x0a,
	0x06, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x1b, 0x2e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70,
	0x6f, 0x72, 0x74, 0x2e, 0x54, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63, 0x43, 0x6f, 0x6e, 0x64, 0x69,
	0x74, 0x69, 0x6f, 0x6e, 0x1a, 0x0f, 0x2e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74,
	0x2e, 0x56, 0x6f, 0x69, 0x64, 0x22, 0x00, 0x12, 0x39, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x42, 0x79,
	0x49, 0x64, 0x12, 0x0f, 0x2e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x42,
	0x79, 0x49, 0x64, 0x1a, 0x1b, 0x2e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e,
	0x54, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63, 0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e,
	0x22, 0x00, 0x12, 0x48, 0x0a, 0x06, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x12, 0x1b, 0x2e, 0x74,
	0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x54, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63,
	0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x1a, 0x1f, 0x2e, 0x74, 0x72, 0x61, 0x6e,
	0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x41, 0x6c, 0x6c, 0x54, 0x72, 0x61, 0x66, 0x66, 0x69, 0x63,
	0x43, 0x6f, 0x6e, 0x64, 0x69, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x22, 0x00, 0x42, 0x15, 0x5a, 0x13,
	0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f,
	0x72, 0x74, 0x2f, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_rawDescOnce sync.Once
	file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_rawDescData = file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_rawDesc
)

func file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_rawDescGZIP() []byte {
	file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_rawDescOnce.Do(func() {
		file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_rawDescData = protoimpl.X.CompressGZIP(file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_rawDescData)
	})
	return file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_rawDescData
}

var file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_goTypes = []any{
	(*TrafficCondition)(nil),     // 0: transport.TrafficCondition
	(*AllTrafficConditions)(nil), // 1: transport.AllTrafficConditions
	(*ById)(nil),                 // 2: transport.ById
	(*Void)(nil),                 // 3: transport.Void
}
var file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_depIdxs = []int32{
	0, // 0: transport.AllTrafficConditions.TrafficConditions:type_name -> transport.TrafficCondition
	0, // 1: transport.TrafficConditionService.Create:input_type -> transport.TrafficCondition
	2, // 2: transport.TrafficConditionService.Delete:input_type -> transport.ById
	0, // 3: transport.TrafficConditionService.Update:input_type -> transport.TrafficCondition
	2, // 4: transport.TrafficConditionService.GetById:input_type -> transport.ById
	0, // 5: transport.TrafficConditionService.GetAll:input_type -> transport.TrafficCondition
	3, // 6: transport.TrafficConditionService.Create:output_type -> transport.Void
	3, // 7: transport.TrafficConditionService.Delete:output_type -> transport.Void
	3, // 8: transport.TrafficConditionService.Update:output_type -> transport.Void
	0, // 9: transport.TrafficConditionService.GetById:output_type -> transport.TrafficCondition
	1, // 10: transport.TrafficConditionService.GetAll:output_type -> transport.AllTrafficConditions
	6, // [6:11] is the sub-list for method output_type
	1, // [1:6] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_init() }
func file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_init() {
	if File_submodule_for_smart_city_transportation_service_traffic_conditions_proto != nil {
		return
	}
	file_submodule_for_smart_city_transportation_service_roads_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_msgTypes[0].Exporter = func(v any, i int) any {
			switch v := v.(*TrafficCondition); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_msgTypes[1].Exporter = func(v any, i int) any {
			switch v := v.(*AllTrafficConditions); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_goTypes,
		DependencyIndexes: file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_depIdxs,
		MessageInfos:      file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_msgTypes,
	}.Build()
	File_submodule_for_smart_city_transportation_service_traffic_conditions_proto = out.File
	file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_rawDesc = nil
	file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_goTypes = nil
	file_submodule_for_smart_city_transportation_service_traffic_conditions_proto_depIdxs = nil
}
