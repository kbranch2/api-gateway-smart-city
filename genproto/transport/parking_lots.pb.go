// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.2
// 	protoc        v5.27.1
// source: submodule-for-smart-city/transportation-service/parking_lots.proto

package transport

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type ParkingLot struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	LotId       string `protobuf:"bytes,1,opt,name=lot_id,json=lotId,proto3" json:"lot_id,omitempty"`
	LotName     string `protobuf:"bytes,2,opt,name=lot_name,json=lotName,proto3" json:"lot_name,omitempty"`
	TotalSpaces int32  `protobuf:"varint,3,opt,name=total_spaces,json=totalSpaces,proto3" json:"total_spaces,omitempty"`
	Address     string `protobuf:"bytes,4,opt,name=address,proto3" json:"address,omitempty"`
}

func (x *ParkingLot) Reset() {
	*x = ParkingLot{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_transportation_service_parking_lots_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ParkingLot) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ParkingLot) ProtoMessage() {}

func (x *ParkingLot) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_transportation_service_parking_lots_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ParkingLot.ProtoReflect.Descriptor instead.
func (*ParkingLot) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_transportation_service_parking_lots_proto_rawDescGZIP(), []int{0}
}

func (x *ParkingLot) GetLotId() string {
	if x != nil {
		return x.LotId
	}
	return ""
}

func (x *ParkingLot) GetLotName() string {
	if x != nil {
		return x.LotName
	}
	return ""
}

func (x *ParkingLot) GetTotalSpaces() int32 {
	if x != nil {
		return x.TotalSpaces
	}
	return 0
}

func (x *ParkingLot) GetAddress() string {
	if x != nil {
		return x.Address
	}
	return ""
}

type AllParkingLots struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ParkingLots []*ParkingLot `protobuf:"bytes,1,rep,name=ParkingLots,proto3" json:"ParkingLots,omitempty"`
}

func (x *AllParkingLots) Reset() {
	*x = AllParkingLots{}
	if protoimpl.UnsafeEnabled {
		mi := &file_submodule_for_smart_city_transportation_service_parking_lots_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AllParkingLots) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AllParkingLots) ProtoMessage() {}

func (x *AllParkingLots) ProtoReflect() protoreflect.Message {
	mi := &file_submodule_for_smart_city_transportation_service_parking_lots_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AllParkingLots.ProtoReflect.Descriptor instead.
func (*AllParkingLots) Descriptor() ([]byte, []int) {
	return file_submodule_for_smart_city_transportation_service_parking_lots_proto_rawDescGZIP(), []int{1}
}

func (x *AllParkingLots) GetParkingLots() []*ParkingLot {
	if x != nil {
		return x.ParkingLots
	}
	return nil
}

var File_submodule_for_smart_city_transportation_service_parking_lots_proto protoreflect.FileDescriptor

var file_submodule_for_smart_city_transportation_service_parking_lots_proto_rawDesc = []byte{
	0x0a, 0x42, 0x73, 0x75, 0x62, 0x6d, 0x6f, 0x64, 0x75, 0x6c, 0x65, 0x2d, 0x66, 0x6f, 0x72, 0x2d,
	0x73, 0x6d, 0x61, 0x72, 0x74, 0x2d, 0x63, 0x69, 0x74, 0x79, 0x2f, 0x74, 0x72, 0x61, 0x6e, 0x73,
	0x70, 0x6f, 0x72, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2d, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x2f, 0x70, 0x61, 0x72, 0x6b, 0x69, 0x6e, 0x67, 0x5f, 0x6c, 0x6f, 0x74, 0x73, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x12, 0x09, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x1a,
	0x3b, 0x73, 0x75, 0x62, 0x6d, 0x6f, 0x64, 0x75, 0x6c, 0x65, 0x2d, 0x66, 0x6f, 0x72, 0x2d, 0x73,
	0x6d, 0x61, 0x72, 0x74, 0x2d, 0x63, 0x69, 0x74, 0x79, 0x2f, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70,
	0x6f, 0x72, 0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2d, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x2f, 0x72, 0x6f, 0x61, 0x64, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x7b, 0x0a, 0x0a,
	0x50, 0x61, 0x72, 0x6b, 0x69, 0x6e, 0x67, 0x4c, 0x6f, 0x74, 0x12, 0x15, 0x0a, 0x06, 0x6c, 0x6f,
	0x74, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x6c, 0x6f, 0x74, 0x49,
	0x64, 0x12, 0x19, 0x0a, 0x08, 0x6c, 0x6f, 0x74, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x07, 0x6c, 0x6f, 0x74, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x21, 0x0a, 0x0c,
	0x74, 0x6f, 0x74, 0x61, 0x6c, 0x5f, 0x73, 0x70, 0x61, 0x63, 0x65, 0x73, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x05, 0x52, 0x0b, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x53, 0x70, 0x61, 0x63, 0x65, 0x73, 0x12,
	0x18, 0x0a, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22, 0x49, 0x0a, 0x0e, 0x41, 0x6c, 0x6c,
	0x50, 0x61, 0x72, 0x6b, 0x69, 0x6e, 0x67, 0x4c, 0x6f, 0x74, 0x73, 0x12, 0x37, 0x0a, 0x0b, 0x50,
	0x61, 0x72, 0x6b, 0x69, 0x6e, 0x67, 0x4c, 0x6f, 0x74, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b,
	0x32, 0x15, 0x2e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x50, 0x61, 0x72,
	0x6b, 0x69, 0x6e, 0x67, 0x4c, 0x6f, 0x74, 0x52, 0x0b, 0x50, 0x61, 0x72, 0x6b, 0x69, 0x6e, 0x67,
	0x4c, 0x6f, 0x74, 0x73, 0x32, 0x9c, 0x02, 0x0a, 0x11, 0x50, 0x61, 0x72, 0x6b, 0x69, 0x6e, 0x67,
	0x4c, 0x6f, 0x74, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x32, 0x0a, 0x06, 0x43, 0x72,
	0x65, 0x61, 0x74, 0x65, 0x12, 0x15, 0x2e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74,
	0x2e, 0x50, 0x61, 0x72, 0x6b, 0x69, 0x6e, 0x67, 0x4c, 0x6f, 0x74, 0x1a, 0x0f, 0x2e, 0x74, 0x72,
	0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x56, 0x6f, 0x69, 0x64, 0x22, 0x00, 0x12, 0x2c,
	0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x12, 0x0f, 0x2e, 0x74, 0x72, 0x61, 0x6e, 0x73,
	0x70, 0x6f, 0x72, 0x74, 0x2e, 0x42, 0x79, 0x49, 0x64, 0x1a, 0x0f, 0x2e, 0x74, 0x72, 0x61, 0x6e,
	0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x56, 0x6f, 0x69, 0x64, 0x22, 0x00, 0x12, 0x32, 0x0a, 0x06,
	0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x12, 0x15, 0x2e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f,
	0x72, 0x74, 0x2e, 0x50, 0x61, 0x72, 0x6b, 0x69, 0x6e, 0x67, 0x4c, 0x6f, 0x74, 0x1a, 0x0f, 0x2e,
	0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x56, 0x6f, 0x69, 0x64, 0x22, 0x00,
	0x12, 0x33, 0x0a, 0x07, 0x47, 0x65, 0x74, 0x42, 0x79, 0x49, 0x64, 0x12, 0x0f, 0x2e, 0x74, 0x72,
	0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x42, 0x79, 0x49, 0x64, 0x1a, 0x15, 0x2e, 0x74,
	0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x50, 0x61, 0x72, 0x6b, 0x69, 0x6e, 0x67,
	0x4c, 0x6f, 0x74, 0x22, 0x00, 0x12, 0x3c, 0x0a, 0x06, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x12,
	0x15, 0x2e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2e, 0x50, 0x61, 0x72, 0x6b,
	0x69, 0x6e, 0x67, 0x4c, 0x6f, 0x74, 0x1a, 0x19, 0x2e, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f,
	0x72, 0x74, 0x2e, 0x41, 0x6c, 0x6c, 0x50, 0x61, 0x72, 0x6b, 0x69, 0x6e, 0x67, 0x4c, 0x6f, 0x74,
	0x73, 0x22, 0x00, 0x42, 0x15, 0x5a, 0x13, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f,
	0x74, 0x72, 0x61, 0x6e, 0x73, 0x70, 0x6f, 0x72, 0x74, 0x2f, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x33,
}

var (
	file_submodule_for_smart_city_transportation_service_parking_lots_proto_rawDescOnce sync.Once
	file_submodule_for_smart_city_transportation_service_parking_lots_proto_rawDescData = file_submodule_for_smart_city_transportation_service_parking_lots_proto_rawDesc
)

func file_submodule_for_smart_city_transportation_service_parking_lots_proto_rawDescGZIP() []byte {
	file_submodule_for_smart_city_transportation_service_parking_lots_proto_rawDescOnce.Do(func() {
		file_submodule_for_smart_city_transportation_service_parking_lots_proto_rawDescData = protoimpl.X.CompressGZIP(file_submodule_for_smart_city_transportation_service_parking_lots_proto_rawDescData)
	})
	return file_submodule_for_smart_city_transportation_service_parking_lots_proto_rawDescData
}

var file_submodule_for_smart_city_transportation_service_parking_lots_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_submodule_for_smart_city_transportation_service_parking_lots_proto_goTypes = []any{
	(*ParkingLot)(nil),     // 0: transport.ParkingLot
	(*AllParkingLots)(nil), // 1: transport.AllParkingLots
	(*ById)(nil),           // 2: transport.ById
	(*Void)(nil),           // 3: transport.Void
}
var file_submodule_for_smart_city_transportation_service_parking_lots_proto_depIdxs = []int32{
	0, // 0: transport.AllParkingLots.ParkingLots:type_name -> transport.ParkingLot
	0, // 1: transport.ParkingLotService.Create:input_type -> transport.ParkingLot
	2, // 2: transport.ParkingLotService.Delete:input_type -> transport.ById
	0, // 3: transport.ParkingLotService.Update:input_type -> transport.ParkingLot
	2, // 4: transport.ParkingLotService.GetById:input_type -> transport.ById
	0, // 5: transport.ParkingLotService.GetAll:input_type -> transport.ParkingLot
	3, // 6: transport.ParkingLotService.Create:output_type -> transport.Void
	3, // 7: transport.ParkingLotService.Delete:output_type -> transport.Void
	3, // 8: transport.ParkingLotService.Update:output_type -> transport.Void
	0, // 9: transport.ParkingLotService.GetById:output_type -> transport.ParkingLot
	1, // 10: transport.ParkingLotService.GetAll:output_type -> transport.AllParkingLots
	6, // [6:11] is the sub-list for method output_type
	1, // [1:6] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_submodule_for_smart_city_transportation_service_parking_lots_proto_init() }
func file_submodule_for_smart_city_transportation_service_parking_lots_proto_init() {
	if File_submodule_for_smart_city_transportation_service_parking_lots_proto != nil {
		return
	}
	file_submodule_for_smart_city_transportation_service_roads_proto_init()
	if !protoimpl.UnsafeEnabled {
		file_submodule_for_smart_city_transportation_service_parking_lots_proto_msgTypes[0].Exporter = func(v any, i int) any {
			switch v := v.(*ParkingLot); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_submodule_for_smart_city_transportation_service_parking_lots_proto_msgTypes[1].Exporter = func(v any, i int) any {
			switch v := v.(*AllParkingLots); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_submodule_for_smart_city_transportation_service_parking_lots_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_submodule_for_smart_city_transportation_service_parking_lots_proto_goTypes,
		DependencyIndexes: file_submodule_for_smart_city_transportation_service_parking_lots_proto_depIdxs,
		MessageInfos:      file_submodule_for_smart_city_transportation_service_parking_lots_proto_msgTypes,
	}.Build()
	File_submodule_for_smart_city_transportation_service_parking_lots_proto = out.File
	file_submodule_for_smart_city_transportation_service_parking_lots_proto_rawDesc = nil
	file_submodule_for_smart_city_transportation_service_parking_lots_proto_goTypes = nil
	file_submodule_for_smart_city_transportation_service_parking_lots_proto_depIdxs = nil
}
