// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.4.0
// - protoc             v5.27.1
// source: submodule-for-smart-city/enviromental-service/air_quality_readings.proto

package enviromental

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.62.0 or later.
const _ = grpc.SupportPackageIsVersion8

const (
	AirQualityReadingService_Create_FullMethodName  = "/enviromental.AirQualityReadingService/Create"
	AirQualityReadingService_Delete_FullMethodName  = "/enviromental.AirQualityReadingService/Delete"
	AirQualityReadingService_Update_FullMethodName  = "/enviromental.AirQualityReadingService/Update"
	AirQualityReadingService_GetById_FullMethodName = "/enviromental.AirQualityReadingService/GetById"
	AirQualityReadingService_GetAll_FullMethodName  = "/enviromental.AirQualityReadingService/GetAll"
)

// AirQualityReadingServiceClient is the client API for AirQualityReadingService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type AirQualityReadingServiceClient interface {
	Create(ctx context.Context, in *AirQualityReading, opts ...grpc.CallOption) (*Void, error)
	Delete(ctx context.Context, in *ById, opts ...grpc.CallOption) (*Void, error)
	Update(ctx context.Context, in *AirQualityReading, opts ...grpc.CallOption) (*Void, error)
	GetById(ctx context.Context, in *ById, opts ...grpc.CallOption) (*AirQualityReading, error)
	GetAll(ctx context.Context, in *AirQualityReading, opts ...grpc.CallOption) (*AllAirQualityReadings, error)
}

type airQualityReadingServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewAirQualityReadingServiceClient(cc grpc.ClientConnInterface) AirQualityReadingServiceClient {
	return &airQualityReadingServiceClient{cc}
}

func (c *airQualityReadingServiceClient) Create(ctx context.Context, in *AirQualityReading, opts ...grpc.CallOption) (*Void, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(Void)
	err := c.cc.Invoke(ctx, AirQualityReadingService_Create_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *airQualityReadingServiceClient) Delete(ctx context.Context, in *ById, opts ...grpc.CallOption) (*Void, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(Void)
	err := c.cc.Invoke(ctx, AirQualityReadingService_Delete_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *airQualityReadingServiceClient) Update(ctx context.Context, in *AirQualityReading, opts ...grpc.CallOption) (*Void, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(Void)
	err := c.cc.Invoke(ctx, AirQualityReadingService_Update_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *airQualityReadingServiceClient) GetById(ctx context.Context, in *ById, opts ...grpc.CallOption) (*AirQualityReading, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(AirQualityReading)
	err := c.cc.Invoke(ctx, AirQualityReadingService_GetById_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *airQualityReadingServiceClient) GetAll(ctx context.Context, in *AirQualityReading, opts ...grpc.CallOption) (*AllAirQualityReadings, error) {
	cOpts := append([]grpc.CallOption{grpc.StaticMethod()}, opts...)
	out := new(AllAirQualityReadings)
	err := c.cc.Invoke(ctx, AirQualityReadingService_GetAll_FullMethodName, in, out, cOpts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// AirQualityReadingServiceServer is the server API for AirQualityReadingService service.
// All implementations must embed UnimplementedAirQualityReadingServiceServer
// for forward compatibility
type AirQualityReadingServiceServer interface {
	Create(context.Context, *AirQualityReading) (*Void, error)
	Delete(context.Context, *ById) (*Void, error)
	Update(context.Context, *AirQualityReading) (*Void, error)
	GetById(context.Context, *ById) (*AirQualityReading, error)
	GetAll(context.Context, *AirQualityReading) (*AllAirQualityReadings, error)
	mustEmbedUnimplementedAirQualityReadingServiceServer()
}

// UnimplementedAirQualityReadingServiceServer must be embedded to have forward compatible implementations.
type UnimplementedAirQualityReadingServiceServer struct {
}

func (UnimplementedAirQualityReadingServiceServer) Create(context.Context, *AirQualityReading) (*Void, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedAirQualityReadingServiceServer) Delete(context.Context, *ById) (*Void, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedAirQualityReadingServiceServer) Update(context.Context, *AirQualityReading) (*Void, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedAirQualityReadingServiceServer) GetById(context.Context, *ById) (*AirQualityReading, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetById not implemented")
}
func (UnimplementedAirQualityReadingServiceServer) GetAll(context.Context, *AirQualityReading) (*AllAirQualityReadings, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAll not implemented")
}
func (UnimplementedAirQualityReadingServiceServer) mustEmbedUnimplementedAirQualityReadingServiceServer() {
}

// UnsafeAirQualityReadingServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to AirQualityReadingServiceServer will
// result in compilation errors.
type UnsafeAirQualityReadingServiceServer interface {
	mustEmbedUnimplementedAirQualityReadingServiceServer()
}

func RegisterAirQualityReadingServiceServer(s grpc.ServiceRegistrar, srv AirQualityReadingServiceServer) {
	s.RegisterService(&AirQualityReadingService_ServiceDesc, srv)
}

func _AirQualityReadingService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AirQualityReading)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AirQualityReadingServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: AirQualityReadingService_Create_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AirQualityReadingServiceServer).Create(ctx, req.(*AirQualityReading))
	}
	return interceptor(ctx, in, info, handler)
}

func _AirQualityReadingService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ById)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AirQualityReadingServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: AirQualityReadingService_Delete_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AirQualityReadingServiceServer).Delete(ctx, req.(*ById))
	}
	return interceptor(ctx, in, info, handler)
}

func _AirQualityReadingService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AirQualityReading)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AirQualityReadingServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: AirQualityReadingService_Update_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AirQualityReadingServiceServer).Update(ctx, req.(*AirQualityReading))
	}
	return interceptor(ctx, in, info, handler)
}

func _AirQualityReadingService_GetById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ById)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AirQualityReadingServiceServer).GetById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: AirQualityReadingService_GetById_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AirQualityReadingServiceServer).GetById(ctx, req.(*ById))
	}
	return interceptor(ctx, in, info, handler)
}

func _AirQualityReadingService_GetAll_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AirQualityReading)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AirQualityReadingServiceServer).GetAll(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: AirQualityReadingService_GetAll_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AirQualityReadingServiceServer).GetAll(ctx, req.(*AirQualityReading))
	}
	return interceptor(ctx, in, info, handler)
}

// AirQualityReadingService_ServiceDesc is the grpc.ServiceDesc for AirQualityReadingService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var AirQualityReadingService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "enviromental.AirQualityReadingService",
	HandlerType: (*AirQualityReadingServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _AirQualityReadingService_Create_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _AirQualityReadingService_Delete_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _AirQualityReadingService_Update_Handler,
		},
		{
			MethodName: "GetById",
			Handler:    _AirQualityReadingService_GetById_Handler,
		},
		{
			MethodName: "GetAll",
			Handler:    _AirQualityReadingService_GetAll_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "submodule-for-smart-city/enviromental-service/air_quality_readings.proto",
}
