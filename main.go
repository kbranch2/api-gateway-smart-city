package main

import (
	"fmt"
	"path/filepath"

	"github.com/casbin/casbin/v2"
	"github.com/smart-city/api-gateway-smart-city/api"
	cf "github.com/smart-city/api-gateway-smart-city/config"
	"github.com/smart-city/api-gateway-smart-city/config/logger"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"runtime"
)

var (
	_, b, _, _ = runtime.Caller(0)
	basepath   = filepath.Dir(b)
)

func main() {
	config := cf.Load()
	logger := logger.NewLogger(basepath, config.LOG_PATH) // Don't forget to change your log path
	em := cf.NewErrorManager(logger)
	enforcer, err := casbin.NewEnforcer("config/casbin/casbin.conf", "config/casbin/casbin.csv")
	em.CheckErr(err)

	CitizenConn, err := grpc.NewClient(fmt.Sprintf("localhost%s", config.CITIZEN_SERVICE_PORT), grpc.WithTransportCredentials(insecure.NewCredentials()))
	em.CheckErr(err)
	defer CitizenConn.Close()

	CityConn, err := grpc.NewClient(fmt.Sprintf("localhost%s", config.CITY_SERVICE_PORT), grpc.WithTransportCredentials(insecure.NewCredentials()))
	em.CheckErr(err)
	defer CityConn.Close()

	EmergencyConn, err := grpc.NewClient(fmt.Sprintf("localhost%s", config.EMERGENCY_SERVICE_PORT), grpc.WithTransportCredentials(insecure.NewCredentials()))
	em.CheckErr(err)
	defer EmergencyConn.Close()

	EnergyConn, err := grpc.NewClient(fmt.Sprintf("localhost%s", config.EMERGENCY_SERVICE_PORT), grpc.WithTransportCredentials(insecure.NewCredentials()))
	em.CheckErr(err)
	defer EnergyConn.Close()

	EnvironmentalConn, err := grpc.NewClient(fmt.Sprintf("localhost%s", config.ENVIRONMENTAL_SERVICE_PORT), grpc.WithTransportCredentials(insecure.NewCredentials()))
	em.CheckErr(err)
	defer EnvironmentalConn.Close()

	TransportationConn, err := grpc.NewClient(fmt.Sprintf("localhost%s", config.TRANSPORTATION_SERVICE_PORT), grpc.WithTransportCredentials(insecure.NewCredentials()))
	em.CheckErr(err)
	defer TransportationConn.Close()

	r := api.NewRouter(CitizenConn, CityConn, EmergencyConn, EnergyConn, EnvironmentalConn, TransportationConn, *logger, enforcer)

	fmt.Printf("Server started on port %s\n", config.HTTPPort)
	logger.INFO.Println("Server started on port: " + config.HTTPPort)
	if r.Run(config.HTTPPort); err != nil {
		logger.ERROR.Panicln("Handling stopped due to error " + err.Error())
	}
}
